export interface PostFilter {
    authorId?: number;
    likedByUserId?: number;
}
