export interface SharePost {
    postId: number;
    receiverEmail: string;
}
