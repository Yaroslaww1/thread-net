export interface EditPost {
    body: string;
    previewImageUrl: string;
}
