import { Post } from '../post/post';
import { Reaction } from './reaction';

export interface ReactionWithPost {
    reaction: Reaction;
    post: Post;
}
