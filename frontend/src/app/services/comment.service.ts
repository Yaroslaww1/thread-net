import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) {}

    public createComment(comment: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, comment);
    }

    public deleteComment(comment: Comment) {
        return this.httpService.deleteFullRequest<Comment>(`${this.routePrefix}/${comment.id}`);
    }

    public saveComment(comment: Comment) {
        return this.httpService.putFullRequest<Comment>(`${this.routePrefix}/${comment.id}`, comment);
    }
}
