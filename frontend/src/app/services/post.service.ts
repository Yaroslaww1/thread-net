import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { PostFilter } from '../models/filters/post-filter';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) {}

    public getPosts(filter: PostFilter) {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`, filter);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public deletePost(post: Post) {
        return this.httpService.deleteFullRequest<Post>(`${this.routePrefix}/${post.id}`);
    }

    public savePost(post: Post) {
        return this.httpService.putFullRequest<Post>(`${this.routePrefix}/${post.id}`, post);
    }
}
