import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { SharePostDialogComponent } from '../components/main-thread/share-post-dialog/share-post-dialog.component';
import { SharePost } from '../models/post/share-post';
import { HttpInternalService } from './http-internal.service';

@Injectable({ providedIn: 'root' })
export class SharePostDialogService implements OnDestroy {
    public routePrefix = '/api/posts';

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialog: MatDialog,
        private httpService: HttpInternalService
    ) { }

    public openSharePostDialog(data: any) {
        const dialog = this.dialog.open(SharePostDialogComponent, {
            data,
            minWidth: 300,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });

        dialog
            .afterClosed()
            .pipe(takeUntil(this.unsubscribe$));
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public sendSharePostEmail(sharePost: SharePost) {
        return this.httpService.postFullRequest(`${this.routePrefix}/sendSharePostEmail`, sharePost);
    }
}
