import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { Post } from '../../models/post/post';
import { NewReaction } from '../../models/reactions/newReaction';
import { User } from '../../models/user';
import { HttpInternalService } from './../http-internal.service';
import { Reaction } from '../../models/reactions/reaction';
import { ReactionsService } from './reactions.service';

@Injectable({ providedIn: 'root' })
export class PostReactionsService {
    public routePrefix = '/api/posts';

    constructor(
        private httpService: HttpInternalService,
        private reactionsService: ReactionsService,
    ) {}

    public createPostReactionAPI(reaction: NewReaction) {
        return this.httpService.postFullRequest<Reaction[]>(`${this.routePrefix}/reactions`, reaction);
    }

    public createPostReaction(post: Post, currentUser: User, isLike: boolean) {
        const reactionsBeforeChange = post.reactions;

        const {
            reaction,
            updatedEntity: updatedPost
        } = this.reactionsService.createReaction(post, currentUser, isLike);

        return this.createPostReactionAPI(reaction)
            .pipe(
                map(() => ({ ...post, ...updatedPost })),
                catchError(() => {
                    return of({ ...post, ...updatedPost, reactions: reactionsBeforeChange });
                })
            );
    }

    public likePost(post: Post, currentUser: User) {
        return this.createPostReaction(post, currentUser, true);
    }

    public dislikePost(post: Post, currentUser: User) {
        return this.createPostReaction(post, currentUser, false);
    }
}
