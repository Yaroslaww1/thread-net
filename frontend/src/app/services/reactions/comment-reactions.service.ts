import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { NewReaction } from '../../models/reactions/newReaction';
import { User } from '../../models/user';
import { HttpInternalService } from '../http-internal.service';
import { Reaction } from '../../models/reactions/reaction';
import { ReactionsService } from './reactions.service';
import { Comment } from 'src/app/models/comment/comment';

@Injectable({ providedIn: 'root' })
export class CommentReactionsService {
    public routePrefix = '/api/comments';

    constructor(
        private httpService: HttpInternalService,
        private reactionsService: ReactionsService,
    ) {}

    public createCommentReactionAPI(reaction: NewReaction) {
        return this.httpService.postFullRequest<Reaction[]>(`${this.routePrefix}/reactions`, reaction);
    }

    public createCommentReaction(comment: Comment, currentUser: User, isLike: boolean) {
        const reactionsBeforeChange = comment.reactions;

        const {
            reaction,
            updatedEntity: updatedComment
        } = this.reactionsService.createReaction(comment, currentUser, isLike);

        return this.createCommentReactionAPI(reaction)
            .pipe(
                map(() => ({ ...comment, ...updatedComment })),
                catchError(() => {
                    return of({ ...comment, ...updatedComment, reactions: reactionsBeforeChange });
                })
            );
    }

    public likeComment(comment: Comment, currentUser: User) {
        return this.createCommentReaction(comment, currentUser, true);
    }

    public dislikeComment(comment: Comment, currentUser: User) {
        return this.createCommentReaction(comment, currentUser, false);
    }
}
