import { Injectable } from '@angular/core';

import { NewReaction } from '../../models/reactions/newReaction';
import { User } from '../../models/user';
import { Reaction } from '../../models/reactions/reaction';

export interface EntityToReactOn {
    id: number;
    reactions: Reaction[];
}

@Injectable({ providedIn: 'root' })
export class ReactionsService {
    public createReaction(entity: EntityToReactOn, currentUser: User, isLike: boolean) {
        const innerEntity = entity;

        const reaction: NewReaction = {
            entityId: entity.id,
            isLike,
            userId: currentUser.id
        };

        const existingReaction = this.getExistingReaction(innerEntity.reactions, currentUser.id);

        if (existingReaction) {
            if (existingReaction.isLike === isLike) {
                innerEntity.reactions = this.removeUserReactionsFromReactionsArray(innerEntity.reactions, currentUser.id);
            } else {
                innerEntity.reactions = this.removeUserReactionsFromReactionsArray(innerEntity.reactions, currentUser.id);
                innerEntity.reactions = this.addReactionToReactionsArray(innerEntity.reactions, ({ isLike, user: currentUser }));
            }
        } else {
            innerEntity.reactions = this.addReactionToReactionsArray(innerEntity.reactions, ({ isLike, user: currentUser }));
        }

        return {
            updatedEntity: innerEntity,
            reaction,
        };
    }

    private getExistingReaction(reactions: Reaction[], userId: number) {
        return reactions.find((x) => x.user.id === userId);
    }

    private removeUserReactionsFromReactionsArray(reactions: Reaction[], userId: number) {
        return reactions.filter((x) => x.user.id !== userId);
    }

    private addReactionToReactionsArray(reactions: Reaction[], newReaction: Reaction) {
        return reactions.concat(newReaction);
    }
}
