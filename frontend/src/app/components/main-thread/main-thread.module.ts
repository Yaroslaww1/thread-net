import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { MaterialComponentsModule } from '../common/material-components.module';
import { PostCommentComponent } from './post/post-comment/post-comment.component';
import { PostComponent } from './post/post.component';
import { MainThreadComponent } from './main-thread.component';
import { PostEditFormComponent } from './post/post-edit-form/post-edit-form.component';
import { ReactionButtonComponent } from './reaction-button/reaction-button.component';
import { ReactionsTooltipComponent } from './reaction-button/reactions-tooltip/reactions-tooltip.component';
import { ReactionsTooltipDirective } from './reaction-button/reactions-tooltip/reactions-tooltip.directive';
import { UserPreviewComponent } from './reaction-button/user-preview/user-preview.component';
import { CommentEditFormComponent } from './post/comment-edit-form/comment-edit-form.component';
import { MainThreadFiltersComponent } from './main-thread-filters/main-thread-filters.component';
import { SharePostDialogComponent } from './share-post-dialog/share-post-dialog.component';

@NgModule({
    imports: [
        BrowserModule,
        MaterialComponentsModule,
        FormsModule
    ],
    declarations: [
        MainThreadFiltersComponent,
        CommentEditFormComponent,
        ReactionsTooltipComponent,
        ReactionsTooltipDirective,
        UserPreviewComponent,
        ReactionButtonComponent,
        PostCommentComponent,
        PostComponent,
        PostEditFormComponent,
        MainThreadComponent,
        SharePostDialogComponent,
    ],
    bootstrap: [PostComponent]
})
export class MainThreadModule { }
