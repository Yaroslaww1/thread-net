import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';

import { Post } from '../../models/post/post';
import { User } from '../../models/user';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { NewPost } from '../../models/post/new-post';
import { SnackBarService } from '../../services/snack-bar.service';
import { environment } from 'src/environments/environment';
import { EditPost } from 'src/app/models/post/edit-post';
import { PostFilter } from 'src/app/models/filters/post-filter';
import { ReactionWithPost } from 'src/app/models/reactions/reaction-with-post';
import { Reaction } from 'src/app/models/reactions/reaction';
import { getParamFromQuery } from 'src/app/helpers/get-param-from-query.helper';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public isOnlyMine = false;

    public currentUser: User;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;

    public postHub: HubConnection;

    private unsubscribe$ = new Subject<void>();

    private _postFilter: PostFilter;
    get postFilter() {
        return this._postFilter;
    }
    set postFilter(postFilter: PostFilter) {
        this._postFilter = postFilter;
        this.getPosts();
    }

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHub.stop();
    }

    public ngOnInit() {
        this.registerHub();
        this.getPosts();
        this.getUser();

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
        });
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts(this.postFilter)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.cachedPosts = resp.body;

                    // wee need to wait until post will be rendered
                    setTimeout(() => {
                        this.scrollToPostFromQueryParams();
                    }, 100);
                },
                (error) => (this.loadingPosts = false)
            );
    }

    public scrollToPostFromQueryParams() {
        const postId = getParamFromQuery('postId');
        if (!postId) {
            return;
        }

        const postElement = document.getElementById(`post-${postId}`);
        postElement?.scrollIntoView();
    }

    public addPostAPI(post: EditPost) {
        const newPost: NewPost = {
            authorId: this.currentUser?.id ?? undefined,
            body: post.body,
            previewImage: post.previewImageUrl
        };

        this.loading = true;

        this.postService.createPost(newPost)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(
                (respPost) => {
                    this.addPost(respPost.body);
                    this.showPostContainer = false;
                    this.loading = false;
                    this.snackBarService.showUsualMessage('Post was successfully created!');
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public addPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                this.posts = this.sortPostArray(this.posts.concat(newPost));
            }
        }
    }

    public savePostAPI(post: Post) {
        this.loading = true;

        this.postService.savePost(post)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(
                (respPost) => {
                    this.updatePost(respPost.body);
                    this.loading = false;
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public updatePost(savedPost: Post) {
        let index = this.cachedPosts.findIndex(post => post.id === savedPost.id);

        if (index === -1) {
            return;
        }

        this.cachedPosts[index] = savedPost;
        this.cachedPosts = this.sortPostArray(this.cachedPosts);
        if (!this.isOnlyMine || (this.isOnlyMine && savedPost.author.id === this.currentUser.id)) {
            index = this.posts.findIndex(post => post.id === savedPost.id);

            if (index === -1) {
                return;
            }

            this.posts[index] = savedPost;
            this.posts = this.sortPostArray(this.posts);
        }
    }

    public deletePostAPI(post: Post) {
        this.loading = true;

        this.postService
            .deletePost(post)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.deletePost(resp.body);
                    this.loading = false;
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public deletePost(deletedPost: Post) {
        if (this.cachedPosts.some((x) => x.id === deletedPost.id)) {
            this.cachedPosts = this.sortPostArray(this.cachedPosts.filter(post => post.id !== deletedPost.id));
            if (!this.isOnlyMine || (this.isOnlyMine && deletedPost.author.id === this.currentUser.id)) {
                this.posts = this.sortPostArray(this.posts.filter(post => post.id !== deletedPost.id));
            }
        }
    }

    public reactOnPost(reaction: ReactionWithPost) {
        if (this.postFilter?.likedByUserId === this.currentUser?.id) {
            this.getPosts();
        }
    }

    public changeFilter(filter: PostFilter) {
        this.postFilter = filter;
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private sortPostArray(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder()
            .withUrl(`${environment.apiUrl}/notifications/post`)
            .build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('NotifyAboutNewPost', (newPost: Post) => {
            if (newPost) {
                this.addPost(newPost);
            }
        });

        this.postHub.on('NotifyAboutLikePost', (likedPost: Post, reaction: Reaction) => {
            this.notifyAboutLikePost(likedPost, reaction);
        });
    }

    private notifyAboutLikePost(post: Post, reaction: Reaction) {
        if (post.author.id === this.currentUser?.id && reaction.user.id !== this.currentUser?.id) {
            this.snackBarService.showUsualMessage('Your post was liked');
        }
    }
}
