import { Component, Input } from '@angular/core';

import { Reaction } from 'src/app/models/reactions/reaction';

@Component({
    selector: 'reactions-tooltip',
    templateUrl: './reactions-tooltip.component.html',
    styleUrls: ['./reactions-tooltip.component.sass']
})
export class ReactionsTooltipComponent {
    @Input() reactions: Reaction[];
}
