import { ComponentRef, Directive, ElementRef, HostListener, Input, OnInit, TemplateRef } from '@angular/core';
import { Overlay, OverlayPositionBuilder, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';

import { ReactionsTooltipComponent } from './reactions-tooltip.component';
import { Reaction } from 'src/app/models/reactions/reaction';

@Directive({ selector: '[reactionsTooltip]' })
export class ReactionsTooltipDirective implements OnInit {
  @Input() reactions: Reaction[];

  private overlayRef: OverlayRef;

  constructor(private overlay: Overlay,
    private overlayPositionBuilder: OverlayPositionBuilder,
    private elementRef: ElementRef) {
  }

  ngOnInit(): void {
    const positionStrategy = this.overlayPositionBuilder
      .flexibleConnectedTo(this.elementRef)
      .withPositions([{
        originX: 'center',
        originY: 'top',
        overlayX: 'center',
        overlayY: 'bottom',
        offsetY: -8,
      }]);

    this.overlayRef = this.overlay.create({ positionStrategy });
  }

  @HostListener('mouseenter')
  show() {
    const tooltipRef: ComponentRef<ReactionsTooltipComponent>
      = this.overlayRef.attach(new ComponentPortal(ReactionsTooltipComponent));

    tooltipRef.instance.reactions = this.reactions;
  }

  @HostListener('mouseout')
  hide() {
    this.overlayRef.detach();
  }
}
