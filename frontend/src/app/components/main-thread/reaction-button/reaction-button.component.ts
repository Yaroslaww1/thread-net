import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Reaction } from 'src/app/models/reactions/reaction';

@Component({
    selector: 'reaction-button',
    templateUrl: './reaction-button.component.html',
    styleUrls: ['./reaction-button.component.sass']
})
export class ReactionButtonComponent {
    @Input() public reactions: Reaction[];
    @Input() public isLike: boolean;

    @Output() public onReact: EventEmitter<void> = new EventEmitter();

    public get reactionsCount() {
        return this.reactions.length;
    }
    public get iconName() {
        return this.isLike ? 'favorite' : 'thumb_down';
    }

    public react() {
        this.onReact.emit();
    }
}
