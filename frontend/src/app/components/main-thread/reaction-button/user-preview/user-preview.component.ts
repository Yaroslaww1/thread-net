import { Component, Input, OnInit } from '@angular/core';

import { User } from 'src/app/models/user';

@Component({
    selector: 'user-preview',
    templateUrl: './user-preview.component.html',
    styleUrls: ['./user-preview.component.sass']
})
export class UserPreviewComponent implements OnInit {
    @Input() user: User;

    ngOnInit(): void {
        console.log(this.user);
    }
}
