import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { of, Subject } from 'rxjs';

import { SnackBarService } from 'src/app/services/snack-bar.service';
import { EditPost } from 'src/app/models/post/edit-post';
import { GyazoService } from 'src/app/services/gyazo.service';
import { switchMap, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'post-edit-form',
    templateUrl: './post-edit-form.component.html',
    styleUrls: ['./post-edit-form.component.sass']
})
export class PostEditFormComponent implements OnInit, OnDestroy {
    @Input() public initialBody = '';
    @Input() public initialImageUrl: string;
    @Input() public initialImageFile: Blob;

    @Output() public onSave: EventEmitter<EditPost> = new EventEmitter();

    public isLoading = false;
    public body: string;
    public imageUrl: string;
    public imageFile: Blob;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private snackBarService: SnackBarService,
        private gyazoService: GyazoService,
    ) {}

    public ngOnInit() {
        this.body = this.initialBody;
        this.imageUrl = this.initialImageUrl;
        this.imageFile = this.initialImageFile;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public savePost() {
        const editPost: EditPost = {
            previewImageUrl: this.imageUrl,
            body: this.body,
        };

        const postSubscription = !this.imageFile
            ? of({})
            : this.gyazoService.uploadImage(this.imageFile).pipe(
                switchMap((imageData) => {
                    editPost.previewImageUrl = imageData.url;
                    return of({});
                })
            );

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            () => {
                this.onSave.emit(editPost);
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }
}
