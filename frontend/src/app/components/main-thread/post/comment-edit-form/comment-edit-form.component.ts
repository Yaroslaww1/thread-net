import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { EditComment } from 'src/app/models/comment/edit-comment';

@Component({
    selector: 'comment-edit-form',
    templateUrl: './comment-edit-form.component.html',
    styleUrls: ['./comment-edit-form.component.sass']
})
export class CommentEditFormComponent implements OnInit {
    @Input() public initialBody = '';

    @Output() public onSave: EventEmitter<EditComment> = new EventEmitter();

    public isLoading = false;
    public body: string;

    public ngOnInit() {
        this.body = this.initialBody;
    }

    public saveComment() {
        const editComment: EditComment = {
            body: this.body,
        };

        this.onSave.emit(editComment);
        this.body = '';
    }
}
