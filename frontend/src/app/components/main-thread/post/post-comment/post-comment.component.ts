import { Component, EventEmitter, Input, Output } from '@angular/core';
import { empty, Observable, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { EditComment } from 'src/app/models/comment/edit-comment';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { User } from 'src/app/models/user';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { AuthenticationService } from 'src/app/services/auth.service';
import { CommentReactionsService } from 'src/app/services/reactions/comment-reactions.service';

import { Comment } from '../../../../models/comment/comment';

@Component({
    selector: 'post-comment',
    templateUrl: './post-comment.component.html',
    styleUrls: ['./post-comment.component.sass']
})
export class PostCommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    @Output() public onSave: EventEmitter<Comment> = new EventEmitter();
    @Output() public onDelete: EventEmitter<Comment> = new EventEmitter();

    public isEditing = false;

    get canModify() {
        return this.comment.author?.id === this.currentUser?.id;
    }
    get likesReactions() {
        return this.comment.reactions.filter(r => r.isLike);
    }
    get dislikesReactions() {
        return this.comment.reactions.filter(r => !r.isLike);
    }

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private commentReactionsService: CommentReactionsService,
    ) {}

    public startEditingComment() {
        this.isEditing = true;
    }

    public saveComment(editComment: EditComment) {
        this.onSave.emit({
            ...this.comment,
            body: editComment.body,
        });
        this.isEditing = false;
    }

    public deleteComment() {
        this.onDelete.emit(this.comment);
    }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.commentReactionsService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.commentReactionsService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.commentReactionsService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.commentReactionsService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.authDialogService.openAuthDialog(DialogType.SignIn);
                return empty();
            })
        );
    }
}
