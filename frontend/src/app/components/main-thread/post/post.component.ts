import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { empty, Observable, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil, tap } from 'rxjs/operators';

import { Post } from '../../../models/post/post';
import { AuthenticationService } from '../../../services/auth.service';
import { AuthDialogService } from '../../../services/auth-dialog.service';
import { DialogType } from '../../../models/common/auth-dialog-type';
import { NewComment } from '../../../models/comment/new-comment';
import { CommentService } from '../../../services/comment.service';
import { User } from '../../../models/user';
import { Comment } from '../../../models/comment/comment';
import { SnackBarService } from '../../../services/snack-bar.service';
import { EditPost } from 'src/app/models/post/edit-post';
import { EditComment } from 'src/app/models/comment/edit-comment';
import { PostReactionsService } from 'src/app/services/reactions/post-reactions.service';
import { Reaction } from 'src/app/models/reactions/reaction';
import { ReactionWithPost } from 'src/app/models/reactions/reaction-with-post';
import { SharePostDialogService } from 'src/app/services/share-post-dialog.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass'],
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    @Output() public onDelete: EventEmitter<Post> = new EventEmitter();
    @Output() public onSave: EventEmitter<Post> = new EventEmitter();
    @Output() public onReact: EventEmitter<ReactionWithPost> = new EventEmitter();

    public showComments = false;
    public isEditing = false;
    get canModify() {
        return this.post.author?.id === this.currentUser?.id;
    }
    get likesReactions() {
        return this.post.reactions.filter(r => r.isLike);
    }
    get likesCount() {
        return this.likesReactions.length;
    }
    get dislikesReactions() {
        return this.post.reactions.filter(r => !r.isLike);
    }
    get dislikesCount() {
        return this.dislikesReactions.length;
    }
    get showShareButton() {
        return this.currentUser;
    }

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private postReactionsService: PostReactionsService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private sharePostDialogService: SharePostDialogService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public reactOnPost(isLike: boolean) {
        const newReaction: Reaction = {
            isLike,
            user: this.currentUser
        };
        const reactionWithPost: ReactionWithPost = {
            reaction: newReaction,
            post: this.post
        };

        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.postReactionsService.createPostReaction(this.post, userResp, isLike)),
                    tap(() => this.onReact.emit(reactionWithPost)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.postReactionsService
            .createPostReaction(this.post, this.currentUser, isLike)
            .pipe(
                takeUntil(this.unsubscribe$),
                tap(() => this.onReact.emit(reactionWithPost)),
            )
            .subscribe((post) => (this.post = post));
    }

    public savePost(editPost: EditPost) {
        this.onSave.emit({
            ...this.post,
            previewImage: editPost.previewImageUrl,
            body: editPost.body,
        });
        this.isEditing = false;
    }

    public deletePost() {
        this.onDelete.emit(this.post);
    }

    public startEditingPost() {
        this.isEditing = true;
    }

    public sendComment(editComment: EditComment) {
        const newComment: NewComment = {
            body: editComment.body,
            authorId: this.currentUser.id,
            postId: this.post.id
        };

        this.commentService
            .createComment(newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public saveComment(comment: Comment) {
        this.commentService
            .saveComment(comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        const index = this.post.comments.findIndex(c => c.id === resp.body.id);
                        this.post.comments[index] = resp.body;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public deleteComment(comment: Comment) {
        this.commentService
            .deleteComment(comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.filter(c => c.id !== comment.id));
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openSharePostDialog() {
        this.sharePostDialogService.openSharePostDialog({ postId: this.post.id });
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.authDialogService.openAuthDialog(DialogType.SignIn);
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
