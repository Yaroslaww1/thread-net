import { Component, Output, EventEmitter, Input } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

import { PostFilter } from 'src/app/models/filters/post-filter';
import { User } from 'src/app/models/user';

@Component({
    selector: 'main-thread-filters',
    templateUrl: './main-thread-filters.component.html',
    styleUrls: ['./main-thread-filters.component.sass']
})
export class MainThreadFiltersComponent {
    @Input() public currentUser: User;

    @Output() public onFilterChange: EventEmitter<PostFilter> = new EventEmitter();

    public filter: PostFilter = {};

    public toggleOnlyMine(event: MatSlideToggleChange) {
        if (event.checked) {
            this.filter.authorId = this.currentUser.id;
        } else {
            this.filter.authorId = undefined;
        }

        this.onFilterChange.emit(this.filter);
    }

    public toggleLikedByMe(event: MatSlideToggleChange) {
        if (event.checked) {
            this.filter.likedByUserId = this.currentUser.id;
        } else {
            this.filter.likedByUserId = undefined;
        }

        this.onFilterChange.emit(this.filter);
    }
}
