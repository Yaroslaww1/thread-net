import { Component, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

import { DialogType } from '../../../models/common/auth-dialog-type';
import { SnackBarService } from '../../../services/snack-bar.service';
import { SharePostDialogService } from 'src/app/services/share-post-dialog.service';
import { SharePost } from 'src/app/models/post/share-post';
import { takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './share-post-dialog.component.html',
    styleUrls: ['./share-post-dialog.component.sass']
})
export class SharePostDialogComponent implements OnDestroy {
    public dialogType = DialogType;
    public email: string;

    public hidePass = true;
    public title: string;
    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<SharePostDialogComponent>,
        private snackBarService: SnackBarService,
        private sharePostDialogService: SharePostDialogService,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public share() {
        const sharePost: SharePost = {
            postId: this.data.postId,
            receiverEmail: this.email,
        };

        this.sharePostDialogService.sendSharePostEmail(sharePost)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => {
                    this.dialogRef.close(false);
                    this.snackBarService.showUsualMessage('Post was successfully shared!');
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}
