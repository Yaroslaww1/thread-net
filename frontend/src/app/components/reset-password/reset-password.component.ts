import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

import { SnackBarService } from '../../services/snack-bar.service';
import { AppRoute } from 'src/app/app.routes';
import { UserService } from 'src/app/services/user.service';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.sass']
})
export class ResetPasswordComponent implements OnDestroy {
    public loading = false;
    public email = '';

    private unsubscribe$ = new Subject<void>();

    constructor(
        private router: Router,
        private userService: UserService,
        private snackBarService: SnackBarService,
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public sendResetPasswordEmail() {
        this.userService
            .sendResetPasswordEmail(this.email)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.snackBarService.showUsualMessage('Email was send. Please check your email.');
                        this.goBackToMainPage();
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public goBackToMainPage() {
        this.router.navigate([AppRoute.Thread]);
    }
}
