import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

import { SnackBarService } from '../../../services/snack-bar.service';
import { AppRoute } from 'src/app/app.routes';
import { UserService } from 'src/app/services/user.service';
import { takeUntil } from 'rxjs/operators';
import { ResetPassword } from 'src/app/models/user/reset-password';
import { getParamFromQuery } from 'src/app/helpers/get-param-from-query.helper';

@Component({
    selector: 'app-reset-password-callback',
    templateUrl: './reset-password-callback.component.html',
    styleUrls: ['./reset-password-callback.component.sass']
})
export class ResetPasswordCallbackComponent implements OnDestroy {
    public loading = false;
    public password = '';

    private unsubscribe$ = new Subject<void>();

    constructor(
        private router: Router,
        private userService: UserService,
        private snackBarService: SnackBarService,
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public resetPassword() {
        const resetPassword: ResetPassword = {
            token: getParamFromQuery('token'),
            password: this.password,
        };

        this.userService
            .resetPassword(resetPassword)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.snackBarService.showUsualMessage('Password was successfully reset.');
                        this.goBackToMainPage();
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public goBackToMainPage() {
        this.router.navigate([AppRoute.Thread]);
    }
}
