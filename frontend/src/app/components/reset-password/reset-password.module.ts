import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { MaterialComponentsModule } from '../common/material-components.module';
import { ResetPasswordComponent } from './reset-password.component';
import { ResetPasswordCallbackComponent } from './reset-password-callback/reset-password-callback.component';
import { ValidatorsModule } from 'src/app/validators/validators.module';

@NgModule({
    imports: [
        BrowserModule,
        MaterialComponentsModule,
        FormsModule,
        ValidatorsModule
    ],
    declarations: [
        ResetPasswordComponent,
        ResetPasswordCallbackComponent,
    ],
})
export class ResetPasswordModule { }
