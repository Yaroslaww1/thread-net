import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { AppRoutes } from './app.routes';
import { HomeComponent } from './components/home/home.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { AuthDialogComponent } from './components/auth-dialog/auth-dialog.component';
import { MaterialComponentsModule } from './components/common/material-components.module';
import { MainThreadModule } from './components/main-thread/main-thread.module';
import { ResetPasswordModule } from './components/reset-password/reset-password.module';
import { EmailValidatorDirective } from './validators/email-validator.directive';
import { ValidatorsModule } from './validators/validators.module';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        UserProfileComponent,
        AuthDialogComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MaterialComponentsModule,
        RouterModule.forRoot(AppRoutes),
        FormsModule,
        MainThreadModule,
        ResetPasswordModule,
        ValidatorsModule
    ],
    exports: [MaterialComponentsModule],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
