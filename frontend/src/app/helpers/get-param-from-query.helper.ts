import { HttpParams } from '@angular/common/http';

export const getParamFromQuery = (paramName: string): string | undefined => {
  const url = window.location.href;
  if (url.includes('?')) {
    const httpParams = new HttpParams({ fromString: url.split('?')[1] });
    return httpParams.get(paramName);
  }

  return undefined;
};