import { Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { MainThreadComponent } from './components/main-thread/main-thread.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ResetPasswordCallbackComponent } from './components/reset-password/reset-password-callback/reset-password-callback.component';

export enum AppRoute {
    Base = '',
    Thread = 'thread',
    Profile = 'profile',
    ResetPassword = 'reset-password',
    ResetPasswordCallback = 'reset-password/callback',
    NotFound = '**'
}

export const AppRoutes: Routes = [
    { path: AppRoute.Base, component: MainThreadComponent, pathMatch: 'full' },
    { path: AppRoute.Thread, component: MainThreadComponent, pathMatch: 'full' },
    { path: AppRoute.Profile, component: UserProfileComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: AppRoute.ResetPassword, component: ResetPasswordComponent, pathMatch: 'full' },
    { path: AppRoute.ResetPasswordCallback, component: ResetPasswordCallbackComponent, pathMatch: 'full' },
    { path: AppRoute.NotFound, redirectTo: '' }
];
