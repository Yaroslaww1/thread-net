using System;

namespace Thread_.NET.BLL.Exceptions
{
    public class NotEnoughPermissionsException : Exception
    {
        public NotEnoughPermissionsException(string action) : base($"Not enough permissions to {action}") { }
    }
}