﻿using System;

namespace Thread_.NET.BLL.Exceptions.Users
{
    public sealed class EmailMustBeUniqueException : Exception
    {
        public EmailMustBeUniqueException() : base("User email must be unique.") { }
    }
}
