﻿using System;

namespace Thread_.NET.BLL.Exceptions.Users
{
    public sealed class ResetPasswordTokenIsNotValidException : Exception
    {
        public ResetPasswordTokenIsNotValidException() : base("Reset password token is not valid.") { }
    }
}
