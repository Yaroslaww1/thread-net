namespace Thread_.NET.BLL.Exceptions.Posts
{
    public sealed class NotEnoughPermissionsToDeletePostException : NotEnoughPermissionsException
    {
        public NotEnoughPermissionsToDeletePostException() : base("delete post") { }
    }
}