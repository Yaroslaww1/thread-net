namespace Thread_.NET.BLL.Exceptions.Posts
{
    public sealed class NotEnoughPermissionsToUpdatePostException : NotEnoughPermissionsException
    {
        public NotEnoughPermissionsToUpdatePostException() : base("update post") { }
    }
}