namespace Thread_.NET.BLL.Exceptions.Comments
{
    public sealed class NotEnoughPermissionsToUpdateCommentException : NotEnoughPermissionsException
    {
        public NotEnoughPermissionsToUpdateCommentException() : base("update comment") { }
    }
}