namespace Thread_.NET.BLL.Exceptions.Comments
{
    public sealed class NotEnoughPermissionsToDeleteCommentException : NotEnoughPermissionsException
    {
        public NotEnoughPermissionsToDeleteCommentException() : base("delete comment") { }
    }
}