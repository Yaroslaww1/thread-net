﻿using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO;
using Thread_.NET.Common.Options;

namespace Thread_.NET.BLL.Providers
{
    public sealed class EmailService : IEmailService
    {
        private readonly IEmailBuilder _builder;

        private readonly string _senderName;
        private readonly string _apiKey;

        public string SenderEmail { get; private set; }

        public EmailService(IEmailBuilder builder, IOptions<EmailOptions> options)
        {
            _builder = builder;

            _apiKey = options.Value.SendgridApiKey;
            SenderEmail = options.Value.SendgridEmail;
            _senderName = options.Value.SendgridName;
        }

        public async Task SendEmailAsync(string email, EmailModel emailModel)
        {

            await SendEmailsAsync(new List<string>() { email }, emailModel);
        }

        public async Task SendEmailsAsync(List<string> emails, EmailModel emailModel)
        {

            var msg = new SendGridMessage()
            {
                From = new EmailAddress(SenderEmail, _senderName),
                Subject = emailModel.Subject,
                HtmlContent = _builder.CreateTemplate(emailModel.Title, emailModel.Body),
            };

            msg.AddTos(emails.Select(email => new EmailAddress(email)).ToList());

            var client = new SendGridClient(_apiKey);
            var response = await client.SendEmailAsync(msg);
        }
    }
}
