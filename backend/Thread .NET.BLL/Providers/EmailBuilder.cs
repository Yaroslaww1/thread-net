﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Thread_.NET.Common.DTO;
using Thread_.NET.Common.Options;

namespace Thread_.NET.BLL.Providers
{
    public sealed class EmailBuilder : IEmailBuilder
    {
        private readonly IOptions<ClientOptions> _clientOptions;

        public EmailBuilder(IOptions<ClientOptions> clientOptions)
        {
            _clientOptions = clientOptions;
        }

        public string CreateTemplate(string title, string body)
        {
            string emailText = @$"<tr><td style=""padding: 20px 30px 30px 30px;""><table width=""100%""><tr><td align=""center"">
                                            {title} </td></tr><tr><td></td></tr><tr><td>
                                            {body}</td></tr><tr><td></td></tr></table></td></tr>";
            return emailText;
        }

        public EmailModel GetPostLikedLetter(int postId)
        {
            string baseUrl = _clientOptions.Value.Url;
            string subject = "Your post was liked";
            string title = @"<b style=""font-size: 20px"">Your post was liked.</b>";
            string body = @$"Check <a href=""{baseUrl}?postId={postId}"">post</a>";
            return new EmailModel()
            {
                Subject = subject,
                Title = title,
                Body = body
            };
        }

        public EmailModel GetResetPasswordLetter(string resetPasswordToken)
        {
            string baseUrl = _clientOptions.Value.Url;
            string subject = "Password reset";
            string title = @"<b style=""font-size: 20px"">Your have requested a password reset.</b>";
            string body = @$"Please visit this <a href=""{baseUrl}/reset-password/callback?token={resetPasswordToken}"">link</a> to reset a password";
            return new EmailModel()
            {
                Subject = subject,
                Title = title,
                Body = body
            };
        }

        public EmailModel GetSharePostLetter(int postId)
        {
            string baseUrl = _clientOptions.Value.Url;
            string subject = "Your post was liked";
            string title = @"<b style=""font-size: 20px"">Post was shared with you.</b>";
            string body = @$"Check <a href=""{baseUrl}?postId={postId}"">post</a>";
            return new EmailModel()
            {
                Subject = subject,
                Title = title,
                Body = body
            };
        }
    }
}
