﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Entities.Abstract;

namespace Thread_.NET.BLL.Hubs
{
    public interface IPostHubClient
    {
        Task NotifyAboutNewPost(PostDTO post);
        Task NotifyAboutLikePost(PostDTO post, ReactionDTO reaction);
    }
    public sealed class PostHub : Hub<IPostHubClient> {}
}
