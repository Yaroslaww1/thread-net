﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Exceptions.Posts;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.Filter.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService, IPostService
    {
        private readonly IHubContext<PostHub, IPostHubClient> _postHub;

        public PostService(
            ThreadContext context,
            IMapper mapper,
            IHubContext<PostHub, IPostHubClient> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<PostDTO> GetPostById(int postId)
        {
            var post = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                    .ThenInclude(user => user.Avatar)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(post => post.Id == postId)
                .FirstOrDefaultAsync();

            if (post == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }

            return _mapper.Map<PostDTO>(post);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(PostFilter postFilter)
        {
            var postsQuery = _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                    .ThenInclude(user => user.Avatar)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .AsQueryable();

            if (postFilter.AuthorId != null)
            {
                postsQuery = postsQuery.Where(post => post.Author.Id == postFilter.AuthorId);
            }

            if (postFilter.LikedByUserId != null)
            {
                postsQuery = postsQuery.Where(
                    post => post.Reactions.Any(
                        reaction => reaction.User.Id == postFilter.LikedByUserId && reaction.IsLike));
            }

            var posts = await postsQuery.ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPostDto = await GetPostById(postEntity.Id);
            await _postHub.Clients.All.NotifyAboutNewPost(createdPostDto);

            return createdPostDto;
        }

        public async Task<PostDTO> UpdatePostById(int postId, int currentUserId, PostUpdateDTO updatePost)
        {
            var post = await _context.Posts
                .Include(post => post.Author)
                .Include(post => post.Preview)
                .FirstOrDefaultAsync(post => post.Id == postId);

            if (post == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }

            if (post.Author.Id != currentUserId)
            {
                throw new NotEnoughPermissionsToUpdatePostException();
            }

            post.Body = updatePost.Body;

            if (post.Preview != null)
            {
                post.Preview.URL = updatePost.PreviewImage;
            }
            else
            {
                if (updatePost.PreviewImage != null)
                {
                    post.Preview = new Image() { URL = updatePost.PreviewImage };
                }
                else
                {
                    post.Preview = null;
                }
            }

            _context.Posts.Update(post);
            await _context.SaveChangesAsync();

            return await GetPostById(postId);
        }

        public async Task<PostDTO> DeletePostById(int postId, int currentUserId)
        {
            var post = await _context.Posts
                .Include(post => post.Author)
                .FirstOrDefaultAsync(post => post.Id == postId);

            if (post == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }

            if (post.Author.Id != currentUserId)
            {
                throw new NotEnoughPermissionsToDeletePostException();
            }

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();

            // TODO: probably need to delete preview image from Gyazo too
            // await _postHub.Clients.All.SendAsync("PostDeleted", deletedPostDto);
            return _mapper.Map<PostDTO>(post);
        }
    }
}
