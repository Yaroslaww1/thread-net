﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Exceptions.Posts;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Providers;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.Filter.Post;
using Thread_.NET.DAL.Context;
namespace Thread_.NET.BLL.Services
{
    public sealed class PostShareService : BaseService, IPostShareService
    {
        private readonly IEmailBuilder _emailBuilder;
        private readonly IEmailService _emailService;
        public PostShareService(
            ThreadContext context,
            IMapper mapper,
            IEmailBuilder emailBuilder,
            IEmailService emailService) : base(context, mapper)
        {
            _emailBuilder = emailBuilder;
            _emailService = emailService;
        }

        public async Task SendSharePostEmail(SendSharePostEmailDTO dto)
        {
            var email = _emailBuilder.GetSharePostLetter(dto.PostId);
            await _emailService.SendEmailAsync(dto.ReceiverEmail, email);
        }
    }
}
