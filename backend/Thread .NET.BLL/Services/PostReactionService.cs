﻿using System;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.BLL.Providers;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostReactionService : BaseService, IPostReactionService
    {
        private readonly IHubContext<PostHub, IPostHubClient> _postHub;
        private readonly IPostService _postService;
        private readonly IEmailService _emailService;
        private readonly IEmailBuilder _emailBuilder;

        public PostReactionService(
            ThreadContext context,
            IMapper mapper,
            IHubContext<PostHub, IPostHubClient> postHub,
            IPostService postService,
            IEmailService emailService,
            IEmailBuilder emailBuilder) : base(context, mapper)
        {
            _postHub = postHub;
            _postService = postService;
            _emailService = emailService;
            _emailBuilder = emailBuilder;
        }

        public async Task CreatePostReaction(NewReactionDTO reactionDto)
        {
            var postReactions = _context.PostReactions
                .Where(r => r.UserId == reactionDto.UserId && r.PostId == reactionDto.EntityId);

            var postReactionsSameAsNewReaction = postReactions
                .Where(r => r.IsLike == reactionDto.IsLike);
            var postReactionsDifferentFromNewReaction = postReactions
                .Where(r => r.IsLike == !reactionDto.IsLike);

            // If same reactions exists remove them
            if (postReactionsSameAsNewReaction.Any())
            {
                _context.PostReactions.RemoveRange(postReactions);
                await _context.SaveChangesAsync();

                return;
            }

            // If opposite reaction exists remove them
            if (postReactionsDifferentFromNewReaction.Any())
            {
                _context.PostReactions.RemoveRange(postReactions);
            }

            var reaction = new PostReaction
            {
                PostId = reactionDto.EntityId,
                IsLike = reactionDto.IsLike,
                UserId = reactionDto.UserId
            };
            _context.PostReactions.Add(reaction);

            await _context.SaveChangesAsync();

            if (reaction.IsLike)
            {
                var likedPost = await _postService.GetPostById(reaction.PostId);
                await _postHub.Clients.All.NotifyAboutLikePost(likedPost, _mapper.Map<ReactionDTO>(reaction));

                var email = _emailBuilder.GetPostLikedLetter(likedPost.Id);
                await _emailService.SendEmailAsync(likedPost.Author.Email, email);
            }
        }
    }
}
