﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Exceptions.Users;
using Thread_.NET.BLL.Providers;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.Security;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class ResetPasswordService : BaseService, IResetPasswordService
    {
        private readonly IEmailService _emailService;
        private readonly IEmailBuilder _emailBuilder;
        private readonly IUserService _userService;

        public ResetPasswordService(
            ThreadContext context,
            IMapper mapper,
            IEmailService emailService,
            IEmailBuilder emailBuilder,
            IUserService userService
        ) : base(context, mapper)
        {
            _emailService = emailService;
            _emailBuilder = emailBuilder;
            _userService = userService;
        }

        public async Task SendResetPasswordEmail(SendResetPasswordEmailDTO dto)
        {
            var userEmail = dto.Email;
            var userEntity = await _context.Users.FirstOrDefaultAsync(u => u.Email == userEmail);

            if (userEntity == null)
            {
                throw new NotFoundException(nameof(User), "email", userEmail);
            }

            var resetPasswordToken = Base64UrlEncoder.Encode(SecurityHelper.GetRandomBytes());

            userEntity.ResetPasswordToken = resetPasswordToken;
            await _context.SaveChangesAsync();

            var email = _emailBuilder.GetResetPasswordLetter(resetPasswordToken);
            await _emailService.SendEmailAsync(userEntity.Email, email);
        }

        public async Task ResetPassword(ResetPasswordDTO dto)
        {
            var userEntity = await _context.Users.FirstOrDefaultAsync(u => u.ResetPasswordToken == dto.Token);

            if (userEntity == null)
            {
                throw new ResetPasswordTokenIsNotValidException();
            }

            userEntity = _userService.UpdateUserPassword(userEntity, dto.Password);
            userEntity.ResetPasswordToken = null;

            await _context.SaveChangesAsync();
        }
    }
}
