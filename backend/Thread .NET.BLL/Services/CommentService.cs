﻿using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Exceptions.Comments;
using Thread_.NET.BLL.Exceptions.Posts;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService, ICommentService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> GetCommentById(int commentId)
        {
            var comment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(comment => comment.Reactions)
                    .ThenInclude(reaction => reaction.User)
                    .ThenInclude(user => user.Avatar)
                .Where(comment => comment.Id == commentId)
                .FirstOrDefaultAsync();

            if (comment == null)
            {
                throw new NotFoundException(nameof(Comment), commentId);
            }

            return _mapper.Map<CommentDTO>(comment);
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task<CommentDTO> UpdateCommentById(int commentId, int currentUserId, CommentUpdateDTO updateComment)
        {
            var comment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(author => author.Avatar)
                .FirstOrDefaultAsync(comment => comment.Id == commentId);

            if (comment == null)
            {
                throw new NotFoundException(nameof(Comment), commentId);
            }

            if (comment.Author.Id != currentUserId)
            {
                throw new NotEnoughPermissionsToUpdateCommentException();
            }

            comment.Body = updateComment.Body;

            _context.Comments.Update(comment);
            await _context.SaveChangesAsync();

            return await GetCommentById(commentId);
        }

        public async Task<CommentDTO> DeleteCommentById(int commentId, int currentUserId)
        {
            var comment = await _context.Comments
                .Include(comment => comment.Author)
                .FirstOrDefaultAsync(comment => comment.Id == commentId);

            if (comment == null)
            {
                throw new NotFoundException(nameof(Comment), commentId);
            }

            if (comment.Author.Id != currentUserId)
            {
                throw new NotEnoughPermissionsToDeleteCommentException();
            }

            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();

            return _mapper.Map<CommentDTO>(comment);
        }
    }
}
