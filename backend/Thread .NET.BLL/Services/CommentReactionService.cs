﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentReactionService : BaseService, ICommentReactionService
    {
        public CommentReactionService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task CreateCommentReaction(NewReactionDTO reaction)
        {
            var commentReactions = _context.CommentReactions
                .Where(r => r.UserId == reaction.UserId && r.CommentId == reaction.EntityId);

            var commentReactionsSameAsNewReaction = commentReactions
                .Where(r => r.IsLike == reaction.IsLike);
            var commentReactionsDifferentFromNewReaction = commentReactions
                .Where(r => r.IsLike == !reaction.IsLike);

            // If same reactions exists remove them
            if (commentReactionsSameAsNewReaction.Any())
            {
                _context.CommentReactions.RemoveRange(commentReactions);
                await _context.SaveChangesAsync();

                return;
            }

            // If opposite reaction exists remove them
            if (commentReactionsDifferentFromNewReaction.Any())
            {
                _context.CommentReactions.RemoveRange(commentReactions);
            }

            _context.CommentReactions.Add(new CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }
    }
}
