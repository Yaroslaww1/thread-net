﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO;

namespace Thread_.NET.BLL.Providers
{
    public interface IEmailService
    {
        string SenderEmail { get; }

        Task SendEmailAsync(string email, EmailModel emailModel);
        Task SendEmailsAsync(List<string> emails, EmailModel emailModel);
    }
}