﻿using Thread_.NET.Common.DTO;

namespace Thread_.NET.BLL.Providers
{
    public interface IEmailBuilder
    {
        string CreateTemplate(string title, string body);
        EmailModel GetPostLikedLetter(int postId);
        EmailModel GetResetPasswordLetter(string resetPasswordToken);
        EmailModel GetSharePostLetter(int postId);
    }
}