﻿using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Comment;

namespace Thread_.NET.BLL.Services
{
    public interface ICommentService
    {
        Task<CommentDTO> CreateComment(NewCommentDTO newComment);
        Task<CommentDTO> DeleteCommentById(int commentId, int currentUserId);
        Task<CommentDTO> GetCommentById(int commentId);
        Task<CommentDTO> UpdateCommentById(int commentId, int currentUserId, CommentUpdateDTO updateComment);
    }
}