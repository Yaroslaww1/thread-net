﻿using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Auth;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.BLL.Services
{
    public interface IAuthService
    {
        Task<AuthUserDTO> Authorize(UserLoginDTO userDto);
        Task<AccessTokenDTO> GenerateAccessToken(int userId, string userName, string email);
        Task<AccessTokenDTO> RefreshToken(RefreshTokenDTO dto);
        Task RevokeRefreshToken(string refreshToken, int userId);
    }
}