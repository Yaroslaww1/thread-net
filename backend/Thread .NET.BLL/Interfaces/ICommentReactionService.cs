﻿using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Reaction;

namespace Thread_.NET.BLL.Services
{
    public interface ICommentReactionService
    {
        Task CreateCommentReaction(NewReactionDTO reaction);
    }
}