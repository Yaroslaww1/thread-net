﻿using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.BLL.Services
{
    public interface IPostShareService
    {
        Task SendSharePostEmail(SendSharePostEmailDTO dto);
    }
}