﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public interface IUserService
    {
        Task<UserDTO> CreateUser(UserRegisterDTO userDto);
        Task DeleteUser(int userId);
        Task<UserDTO> GetUserById(int id);
        Task<ICollection<UserDTO>> GetUsers();
        Task UpdateUser(UserDTO userDto);
        User UpdateUserPassword(User user, string newPassword);
    }
}