﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.Filter.Post;

namespace Thread_.NET.BLL.Services
{
    public interface IPostService
    {
        Task<PostDTO> CreatePost(PostCreateDTO postDto);
        Task<PostDTO> DeletePostById(int postId, int currentUserId);
        Task<ICollection<PostDTO>> GetAllPosts(PostFilter postFilter);
        Task<PostDTO> GetPostById(int postId);
        Task<PostDTO> UpdatePostById(int postId, int currentUserId, PostUpdateDTO updatePost);
    }
}