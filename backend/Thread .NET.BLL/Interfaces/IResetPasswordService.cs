﻿using System.Threading.Tasks;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.BLL.Services
{
    public interface IResetPasswordService
    {
        Task ResetPassword(ResetPasswordDTO dto);
        Task SendResetPasswordEmail(SendResetPasswordEmailDTO dto);
    }
}