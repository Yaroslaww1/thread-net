using System;
using System.IO;

namespace Thread_.NET.Common
{
    public class DotEnvLoader
    {
        public static void Load(string filePath)
        {
            if (!File.Exists(filePath)) {
                Console.WriteLine(".env not found");
                return;
            }

            foreach (var line in File.ReadAllLines(filePath))
            {
                var parts = line.Split(new[] { '=' }, 2);
                
                if (parts.Length != 2)
                    continue;

                Environment.SetEnvironmentVariable(parts[0], parts[1]);
            }
        }
    }
}
