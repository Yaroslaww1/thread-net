﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.Filter.Post;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostService _postService;
        private readonly IPostReactionService _postReactionService;
        private readonly IPostShareService _postShareService;

        public PostsController(IPostService postService, IPostReactionService postReactionService, IPostShareService postShareService)
        {
            _postService = postService;
            _postReactionService = postReactionService;
            _postShareService = postShareService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get([FromQuery] PostFilter filter)
        {
            return Ok(await _postService.GetAllPosts(filter));
        }

        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPost("reactions")]
        public async Task<IActionResult> CreatePostReaction(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _postReactionService.CreatePostReaction(reaction);
            return Ok();
        }
        
        [HttpPut("{id:int}")]
        public async Task<IActionResult> UpdatePost(int id, [FromBody] PostUpdateDTO updatePost)
        {
            var post = await _postService.UpdatePostById(id, this.GetUserIdFromToken(), updatePost);
            return Ok(post);
        }
        
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            var post = await _postService.DeletePostById(id, this.GetUserIdFromToken());
            return Ok(post);
        }

        [HttpPost("sendSharePostEmail")]
        public async Task<IActionResult> SendSharePostEmail([FromBody] SendSharePostEmailDTO dto)
        {
            await _postShareService.SendSharePostEmail(dto);
            return Ok();
        }
    }
}