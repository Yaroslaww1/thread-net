﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly ICommentReactionService _commentReactionService;

        public CommentsController(ICommentService commentService, ICommentReactionService commentReactionService)
        {
            _commentService = commentService;
            _commentReactionService = commentReactionService;
        }

        [HttpPost]
        public async Task<ActionResult<CommentDTO>> CreatePost([FromBody] NewCommentDTO comment)
        {
            comment.AuthorId = this.GetUserIdFromToken();
            return Ok(await _commentService.CreateComment(comment));
        }
        
        [HttpPut("{id:int}")]
        public async Task<IActionResult> UpdateComment(int id, [FromBody] CommentUpdateDTO updateComment)
        {
            var comment = await _commentService.UpdateCommentById(id, this.GetUserIdFromToken(), updateComment);
            return Ok(comment);
        }
        
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteComment(int id)
        {
            var comment = await _commentService.DeleteCommentById(id, this.GetUserIdFromToken());
            return Ok(comment);
        }
        
        [HttpPost("reactions")]
        public async Task<IActionResult> CreateCommentReaction(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _commentReactionService.CreateCommentReaction(reaction);
            return Ok();
        }
    }
}