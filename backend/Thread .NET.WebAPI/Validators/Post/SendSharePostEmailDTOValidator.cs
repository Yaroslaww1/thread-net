﻿using FluentValidation;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.Validators
{
    public sealed class SendSharePostEmailDTOValidator : AbstractValidator<SendSharePostEmailDTO>
    {
        public SendSharePostEmailDTOValidator()
        {
            RuleFor(d => d.ReceiverEmail).Email();
        }
    }
}
