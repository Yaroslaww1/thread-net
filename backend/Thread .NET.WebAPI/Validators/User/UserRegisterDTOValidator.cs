﻿using FluentValidation;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class UserRegisterDTOValidator : AbstractValidator<UserRegisterDTO>
    {
        public UserRegisterDTOValidator()
        {
            RuleFor(u => u.UserName).UserName();

            RuleFor(u => u.Email).Email();

            RuleFor(u => u.Password).Password();

        }
    }
}
