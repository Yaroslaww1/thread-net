using FluentValidation;

namespace Thread_.NET.Validators
{
    public static class UserRules
    {
        public static IRuleBuilderOptions<T, string> Email<T>(this IRuleBuilder<T, string> rule)
        {
            return rule
                .NotNull()
                .EmailAddress()
                .WithMessage("Email is not valid");
        }
        
        public static IRuleBuilderOptions<T, string> Password<T>(this IRuleBuilder<T, string> rule)
        {
            return rule
                .Length(4, 16)
                .WithMessage("Password must be from 4 to 16 characters.");
        }
        
        public static IRuleBuilderOptions<T, string> UserName<T>(this IRuleBuilder<T, string> rule)
        {
            return rule
                .NotEmpty()
                    .WithMessage("Username is mandatory.")
                .MinimumLength(3)
                    .WithMessage("Username should be minimum 3 character.");
        }
    }
}