﻿using FluentValidation;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class SendResetPasswordEmailDTOValidator : AbstractValidator<SendResetPasswordEmailDTO>
    {
        public SendResetPasswordEmailDTOValidator()
        {
            RuleFor(d => d.Email).Email();
        }
    }
}
