﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddResetPasswordTokenTouser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<string>(
                name: "ResetPasswordToken",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 8, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4133), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4550), 14 },
                    { 18, 16, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5101), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5103), 3 },
                    { 17, 6, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5088), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5090), 7 },
                    { 15, 6, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5059), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5062), 10 },
                    { 14, 12, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5045), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5048), 5 },
                    { 13, 15, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5028), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5031), 18 },
                    { 12, 18, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4970), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4972), 10 },
                    { 11, 16, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4956), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4959), 4 },
                    { 19, 15, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5115), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5117), 10 },
                    { 10, 18, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4942), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4944), 6 },
                    { 8, 9, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4910), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4912), 19 },
                    { 7, 7, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4895), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4897), 11 },
                    { 6, 6, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4879), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4882), 16 },
                    { 5, 14, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4864), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4867), 21 },
                    { 4, 20, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4850), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4852), 15 },
                    { 3, 16, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4835), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4837), 20 },
                    { 2, 17, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4812), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4816), 21 },
                    { 9, 6, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4923), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4926), 11 },
                    { 20, 10, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5128), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5131), 10 },
                    { 16, 15, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5074), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5076), 7 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Voluptatem fugit consequatur quasi.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1016), 11, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1341) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Rerum officiis explicabo deserunt qui.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1692), 2, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1698) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Voluptas ut earum vel magni rerum nam iure nemo.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1747), 18, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1751) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Amet quod vitae est id dolorum modi illo est.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1791), 3, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1794) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Qui commodi hic aut praesentium sint exercitationem quos.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1836), 9, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1839) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Velit repellendus facilis.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1863), 4, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1866) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Est inventore aut harum placeat ipsum sed.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1901), 9, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1904) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Eum voluptatum repellat et quia incidunt non molestiae assumenda repellat.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1978), 19, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1982) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Saepe vel quia est cum tempore.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2015), 20, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2018) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Doloremque qui rerum praesentium.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2045), 2, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2048) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Consectetur maxime sit aut recusandae laudantium sit mollitia.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2085), 15, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2087) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "In porro eligendi adipisci.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2114), 16, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2117) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Maiores magnam sunt in non sed qui voluptatem.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2154), 9, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2156) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Ex eos doloribus aut modi in magnam.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2220), 19, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2223) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Et commodi culpa id recusandae quos.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2254), 5, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2257) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Quo impedit sunt autem aliquam nulla.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2289), 16, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2292) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Ut nihil voluptates autem.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2320), 2, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2323) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Aut consectetur laudantium quos sed earum ut officiis non.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2358), 18, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2361) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Autem deserunt fuga ullam omnis velit est fugiat ut quasi.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2400), 6, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2403) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Dolor aperiam earum.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2457), 11, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2460) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 412, DateTimeKind.Local).AddTicks(8497), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/649.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(904) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1206), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1247.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1211) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1226), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/625.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1229) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1240), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/202.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1243) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1253), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/420.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1256) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1266), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/565.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1268) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1278), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/851.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1281) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1335), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/334.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1337) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1349), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/67.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1352) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1363), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1212.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1365) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1375), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/920.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1378) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1388), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/905.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1391) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1401), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/295.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1403) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1413), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/385.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1416) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1426), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1028.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1429) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1439), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/761.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1441) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1451), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/500.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1454) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1463), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1085.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1466) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1476), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/180.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1478) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1489), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/813.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1491) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(1708), "https://picsum.photos/640/480/?image=518", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2046) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2164), "https://picsum.photos/640/480/?image=533", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2168) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2181), "https://picsum.photos/640/480/?image=197", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2183) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2261), "https://picsum.photos/640/480/?image=281", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2264) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2277), "https://picsum.photos/640/480/?image=447", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2279) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2290), "https://picsum.photos/640/480/?image=632", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2293) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2304), "https://picsum.photos/640/480/?image=65", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2306) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2317), "https://picsum.photos/640/480/?image=163", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2319) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2330), "https://picsum.photos/640/480/?image=1063", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2332) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2342), "https://picsum.photos/640/480/?image=203", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2345) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2355), "https://picsum.photos/640/480/?image=482", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2358) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2368), "https://picsum.photos/640/480/?image=240", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2370) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2381), "https://picsum.photos/640/480/?image=463", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2383) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2393), "https://picsum.photos/640/480/?image=327", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2396) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2407), "https://picsum.photos/640/480/?image=811", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2409) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2419), "https://picsum.photos/640/480/?image=226", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2422) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2474), "https://picsum.photos/640/480/?image=517", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2477) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2488), "https://picsum.photos/640/480/?image=407", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2490) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2500), "https://picsum.photos/640/480/?image=810", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2503) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2513), "https://picsum.photos/640/480/?image=41", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2516) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1341), true, 5, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1343), 6 },
                    { 1, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(400), false, 11, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(740), 17 },
                    { 19, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1326), true, 17, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1329), 1 },
                    { 18, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1312), true, 8, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1314), 14 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1298), true, 16, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1300), 21 },
                    { 16, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1284), true, 1, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1286), 11 },
                    { 14, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1253), true, 1, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1256), 10 },
                    { 13, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1227), true, 11, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1230), 1 },
                    { 12, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1213), false, 3, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1216), 20 },
                    { 11, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1198), false, 8, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1201), 21 },
                    { 15, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1268), false, 1, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1271), 16 },
                    { 9, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1170), false, 10, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1173), 18 },
                    { 8, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1155), false, 5, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1158), 21 },
                    { 7, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1141), true, 3, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1143), 3 },
                    { 6, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1126), true, 13, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1129), 1 },
                    { 10, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1185), true, 7, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1187), 9 },
                    { 5, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1109), false, 20, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1111), 15 },
                    { 4, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1093), false, 5, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1096), 17 },
                    { 3, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1076), false, 8, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1079), 16 },
                    { 2, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1010), false, 7, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1015), 2 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Vitae consequatur harum.\nIn tempora cum.\nOmnis ut qui.\nDebitis earum eius aliquid repellendus fugiat nisi et.\nUt fugit necessitatibus officiis.\nRatione iure sed minus voluptate aliquid molestiae sit error.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(2863), 27, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(3215) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "eos", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(3720), 33, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(3729) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Ut placeat quis est sed qui quia libero et neque.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(4187), 27, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(4196) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Velit minus vitae nostrum tenetur illum magni aspernatur. Perspiciatis vel accusamus voluptatem quidem eligendi cumque quam. Enim in eos officia adipisci accusantium et repellat et. Consectetur officia aliquid quo et officia rerum voluptate natus. Nisi qui voluptate et sunt aut quod enim aut doloremque.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(5802), 39, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(5814) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Sed id laborum ut inventore quae a. Ut et consequuntur adipisci libero numquam iusto est quis. Quas et consequuntur dolorem.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(5982), 30, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(5986) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "veniam", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6010), 40, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6014) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "pariatur", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6036), 35, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6039) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "dolores", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6058), 31, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6061) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Et accusamus eum reprehenderit inventore maxime dignissimos voluptates architecto.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6106), 22, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6109) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Voluptatem velit quidem qui tenetur sed deserunt ducimus.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6201), 36, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6204) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "porro", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6224), 31, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6227) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Provident soluta qui aliquid esse rerum harum sed id odit.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6270), 35, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6272) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "non", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6290), 39, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6293) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Quaerat culpa nobis culpa et maxime.\nQui autem voluptatem cum natus sunt.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6377), 40, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6380) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Maiores incidunt soluta dolor.\nQuo pariatur autem sit ut natus dolorem.\nVoluptatem corporis omnis velit in pariatur aspernatur officia est.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6453), 40, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6456) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Unde ipsum qui. Tempore sed aut impedit delectus. Deleniti veritatis ut minus quos est voluptatem dolorem. Deleniti expedita quaerat consequatur temporibus non est ad. Autem ut assumenda itaque quisquam expedita corporis sed. Sit necessitatibus et dolorem enim suscipit.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6623), 21, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6627) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Consequatur aspernatur rerum nulla maxime numquam ipsam ea nostrum commodi.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6674), 36, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6677) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Sit qui delectus ut laboriosam ipsam. Nostrum nostrum dolores ad repellat et. Et repellendus et amet aut. Provident quos reprehenderit voluptatem est voluptate magnam perferendis voluptas quidem.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6801), 26, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6804) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Voluptatibus id inventore sit alias similique.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6838), 39, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6841) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Eius distinctio veritatis consequatur in qui.\nDolor aliquid at voluptas laboriosam rerum vel.\nVeritatis esse ducimus et voluptas quis fuga non commodi ipsam.\nRerum amet labore aut quisquam saepe cum iste.\nMaxime consectetur facilis voluptatem voluptates possimus est.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(7013), 21, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(7017) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 17, 17, 45, 7, 439, DateTimeKind.Local).AddTicks(7117), "Tony46@hotmail.com", "EzvDUZE0gsaLB5yCtvsx+4jPs2BJ6clZHOPudN4S2h0=", "m3oZNpc+fjUyqttSnP7tKyMb/xdlDSdXkC/teMgu89M=", new DateTime(2021, 6, 17, 17, 45, 7, 439, DateTimeKind.Local).AddTicks(7514), "Jakob_Shanahan3" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 17, 17, 45, 7, 444, DateTimeKind.Local).AddTicks(9618), "Albin18@gmail.com", "vBdBUV+laGkAj4wqaCjj3Dkb8W9YupQS41fAq8D8axs=", "4oO/QcUT2XsLJCvx6RYCCGLnFq6Je2JzoSVtjsPSyug=", new DateTime(2021, 6, 17, 17, 45, 7, 444, DateTimeKind.Local).AddTicks(9630), "Jordane_Gulgowski61" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 17, 17, 45, 7, 450, DateTimeKind.Local).AddTicks(1737), "Alexander_Fritsch@yahoo.com", "weLnlZ9rDqHORhjvgj3rTdIlBwxsV+qZf6s3FuhFUOY=", "hRctuLbFR538GLgCocURpR4EyAd9bgWcRXdCiFah9E4=", new DateTime(2021, 6, 17, 17, 45, 7, 450, DateTimeKind.Local).AddTicks(1743), "Daryl_Muller93" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 17, 17, 45, 7, 455, DateTimeKind.Local).AddTicks(2565), "Green.Champlin@hotmail.com", "9lja6zxNPqnN0fFP+hv9F1snIC4uR3AqNM1vkGZubCM=", "GZmIHzUQssBucPo5g1Hk5+SIWDk1/LBgDzXp9MR89yU=", new DateTime(2021, 6, 17, 17, 45, 7, 455, DateTimeKind.Local).AddTicks(2572), "Assunta_Rice22" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 17, 17, 45, 7, 460, DateTimeKind.Local).AddTicks(3921), "Rogelio.Armstrong@yahoo.com", "fXrWgunspkhCa3y6ZzN0mI3lNkkaDGA/cMhiHha34Y0=", "pGFRz5cC+Y2OijzhLVQiClotCddl4Zq6QnVqFuv2t7U=", new DateTime(2021, 6, 17, 17, 45, 7, 460, DateTimeKind.Local).AddTicks(3927), "Florida.Runolfsdottir" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 17, 17, 45, 7, 465, DateTimeKind.Local).AddTicks(5716), "Katrina_Fisher23@hotmail.com", "StgPdItc6dWZJGGc9WLXoXZq3IlviSb9voyd95u9l9o=", "5VIlMiV8D1Hzb6zY5GSHG0VDUbxn0FoA9WFZRl0uOHE=", new DateTime(2021, 6, 17, 17, 45, 7, 465, DateTimeKind.Local).AddTicks(5722), "Abbie_Murazik" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 6, 17, 17, 45, 7, 470, DateTimeKind.Local).AddTicks(6390), "Rolando.Blick93@yahoo.com", "1kHsL507FIj56J0t7T0QaN3faVMZufJKwv9AmjbX7ac=", "X2f0F0SQIO1ngAXEHJnHAbdxvKALJWvg+9UqSV2x0wQ=", new DateTime(2021, 6, 17, 17, 45, 7, 470, DateTimeKind.Local).AddTicks(6396), "Idell17" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 6, 17, 17, 45, 7, 475, DateTimeKind.Local).AddTicks(7870), "Ollie_Schowalter73@yahoo.com", "wS18Mb8oNx3YS5w6Bra/GeqZNhm8Al4nToNSVt79Vnc=", "VnnMKjdggo4ZmG7JE+4hhg6Wv1AGoErTnODmeB5W3hM=", new DateTime(2021, 6, 17, 17, 45, 7, 475, DateTimeKind.Local).AddTicks(7886), "Royal13" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 17, 17, 45, 7, 481, DateTimeKind.Local).AddTicks(1489), "Xander.Murphy@yahoo.com", "U4d5L8wmjttBayeTgaID4FVcgjoC5ZGii4TM6+1ysb0=", "Gkxz+vLYWanJsbz4rhgxopeUQJPWZ2K898REYYZmFiY=", new DateTime(2021, 6, 17, 17, 45, 7, 481, DateTimeKind.Local).AddTicks(1494), "Dan80" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 17, 17, 45, 7, 486, DateTimeKind.Local).AddTicks(2341), "Ned_Stehr96@hotmail.com", "l06MYDOtIelXBgrR57dpRRz9Z6rBC7NbEqNnsgiA7jE=", "1kOolGEFysCrF2I/Wzq8O2oR/myl8w7c4yQ339hHj/U=", new DateTime(2021, 6, 17, 17, 45, 7, 486, DateTimeKind.Local).AddTicks(2348), "Josie_Heller17" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 17, 17, 45, 7, 491, DateTimeKind.Local).AddTicks(4356), "Hilma_Hilll86@hotmail.com", "YSks3HmbpDr766nefhDQi8sfQu/3G9LaoZdJrsSF/wk=", "xpBHLLODBfbcRn0V6fqAjhj+F3C+2BJRTJJchwA9lyQ=", new DateTime(2021, 6, 17, 17, 45, 7, 491, DateTimeKind.Local).AddTicks(4376), "Aliya78" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 17, 17, 45, 7, 496, DateTimeKind.Local).AddTicks(6154), "Bertrand66@hotmail.com", "TEsFmFKwF8eZyUwaXkfWYLh/Mrqldh59jCgQIeye4fg=", "vkoY5n9aGwEgc6Olx5zc9AqkZtH+49ooS82WpkpGbBE=", new DateTime(2021, 6, 17, 17, 45, 7, 496, DateTimeKind.Local).AddTicks(6161), "Amaya82" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 6, 17, 17, 45, 7, 501, DateTimeKind.Local).AddTicks(8633), "Teresa54@yahoo.com", "33q5CsaLEkYoX8Pwg7yrTVmDRbL0TA5cXQXnjFJRAwc=", "ueX7np2NEORasABrb/PvLo3dhP3f7QOQm5C6VuddPb4=", new DateTime(2021, 6, 17, 17, 45, 7, 501, DateTimeKind.Local).AddTicks(8639), "Eryn90" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 507, DateTimeKind.Local).AddTicks(289), "Estevan_Lehner@gmail.com", "S9i7wCeiyJ8uuLJ5HQrPvxyLNgOby4eUhVBb32TJ920=", "WMgiNoJhsghwzNXjszNT9PptfKATxGwAYIIWuXXXPsA=", new DateTime(2021, 6, 17, 17, 45, 7, 507, DateTimeKind.Local).AddTicks(306), "Larry38" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 17, 17, 45, 7, 512, DateTimeKind.Local).AddTicks(1351), "Stevie.Gislason@hotmail.com", "0LwiMgPVe/hCrmwLNvhDvkX18o5q1l9MvfrR091d6aU=", "KvnFyo+lsWt9LVJsXiiS8UO5dwbiBa1zHBPLsAHaqLQ=", new DateTime(2021, 6, 17, 17, 45, 7, 512, DateTimeKind.Local).AddTicks(1361), "Billie.White64" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 17, 17, 45, 7, 517, DateTimeKind.Local).AddTicks(5561), "Libby_Koss49@yahoo.com", "y6aFaZEqN1vbsxTrgqyzgjt8VtJlnYR2NO2kE+NF1ZE=", "YeIx0KlC1+XbKNlW1MzTH7QFTtf7phKlku6nAIQ/J1Y=", new DateTime(2021, 6, 17, 17, 45, 7, 517, DateTimeKind.Local).AddTicks(5569), "Eulalia3" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 17, 17, 45, 7, 522, DateTimeKind.Local).AddTicks(7140), "Damian18@yahoo.com", "d1YNpY5RlQ6zvlJDwJsWGroy6N4e9vROVfVjH82DQYs=", "oGEgKF6YgTYV7Q4+NNKrSsEVkn9AavC8ws/rIh0Kjz8=", new DateTime(2021, 6, 17, 17, 45, 7, 522, DateTimeKind.Local).AddTicks(7147), "Freddie.Gleichner79" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 17, 17, 45, 7, 527, DateTimeKind.Local).AddTicks(8581), "Jaycee_Howell@hotmail.com", "naxfTey9hN8qd7qzWzLKPWjnrKqqEVBsJrSlVCyNgag=", "RY+2kQah0jwpnYqRdirGG104BWGpVWGMkeM6J8sD/qk=", new DateTime(2021, 6, 17, 17, 45, 7, 527, DateTimeKind.Local).AddTicks(8588), "Marlene22" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 6, 17, 17, 45, 7, 532, DateTimeKind.Local).AddTicks(9363), "Mina.Terry42@gmail.com", "JYm0NaTg4a/BBTQBguMrO8FMz6VU1EpDBVDViI69Gbw=", "6VkFXEMLXU1JxQRHpkOGkZYRCATzvVaGqRG34/Jb7z8=", new DateTime(2021, 6, 17, 17, 45, 7, 532, DateTimeKind.Local).AddTicks(9369), "Missouri_Kshlerin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 17, 17, 45, 7, 539, DateTimeKind.Local).AddTicks(3740), "Christophe_Strosin45@yahoo.com", "UE/GndHEwvqqfebRNFoT++LMmB/00T8DLinrgqCGF2Q=", "Io9lviHms5HjmbruFPMAIN+gLtzrcwiXt9w8CcJV0tE=", new DateTime(2021, 6, 17, 17, 45, 7, 539, DateTimeKind.Local).AddTicks(3756), "Preston74" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 544, DateTimeKind.Local).AddTicks(5129), "lthbXAnZjHHAjO4dF+tVsUlgMON6Ecgszra/qFEYY3Q=", "+5xbHHwhZVICAppVTzJWLMiJFQnrNQRMrc4XZVbuav0=", new DateTime(2021, 6, 17, 17, 45, 7, 544, DateTimeKind.Local).AddTicks(5129) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "ResetPasswordToken",
                table: "Users");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 8, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(6484), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(6826), 5 },
                    { 18, 16, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7351), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7353), 9 },
                    { 17, 7, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7336), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7339), 4 },
                    { 15, 18, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7307), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7310), 5 },
                    { 14, 15, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7293), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7296), 9 },
                    { 13, 20, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7279), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7282), 15 },
                    { 12, 15, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7264), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7267), 2 },
                    { 11, 4, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7250), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7252), 17 },
                    { 19, 14, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7365), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7368), 16 },
                    { 10, 17, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7236), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7238), 19 },
                    { 8, 15, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7196), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7199), 12 },
                    { 7, 6, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7181), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7184), 14 },
                    { 6, 10, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7167), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7169), 18 },
                    { 5, 9, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7151), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7154), 7 },
                    { 4, 20, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7135), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7138), 9 },
                    { 3, 18, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7119), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7122), 17 },
                    { 2, 4, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7096), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7101), 3 },
                    { 9, 1, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7220), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7223), 19 },
                    { 20, 5, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7380), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7383), 6 },
                    { 16, 7, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7322), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7325), 10 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Neque ut velit molestiae.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(4479), 19, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(4806) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Sint delectus incidunt eveniet ut nihil quas a accusamus rerum.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5138), 9, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5145) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Accusantium corrupti amet iusto distinctio.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5187), 6, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5190) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Tenetur eos ut eveniet a qui qui et non.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5279), 10, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5283) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Sed voluptatum voluptas praesentium consectetur dolorem occaecati.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5320), 10, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5323) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Aspernatur corporis veritatis qui numquam.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5353), 8, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5356) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Voluptatem aut quidem quia natus aut eligendi repellat ad.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5400), 2, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5403) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Itaque et eligendi debitis dolorem unde.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5434), 15, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5437) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Dolores sint assumenda voluptas.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5464), 16, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5466) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Quo incidunt excepturi omnis.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5528), 4, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5531) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Et sint velit repudiandae distinctio voluptas adipisci necessitatibus perspiciatis.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5572), 20, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5575) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Ut laborum sit ut libero.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5604), 13, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5607) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Assumenda praesentium accusamus quibusdam voluptates nisi blanditiis.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5650), 1, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5653) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Ad enim est odit adipisci sapiente.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5684), 13, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5687) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Sapiente ab id.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5714), 6, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5717) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Veniam non fuga et voluptas.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5781), 17, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5784) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Et maiores eaque ab et alias voluptatem.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5821), 1, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5823) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Beatae velit est deserunt consequuntur esse modi corrupti aperiam.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5861), 14, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5864) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Neque corrupti in totam et illum.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5893), 11, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5896) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Dolorum dignissimos repellendus quia sed sequi voluptates et.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5930), 15, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 980, DateTimeKind.Local).AddTicks(9573), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/388.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(3633) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4223), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/924.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4228) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4244), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/514.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4247) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4257), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/118.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4260) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4271), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/48.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4273) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4283), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/412.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4286) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4296), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1123.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4299) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4459), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/966.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4461) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4471), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/17.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4474) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4559), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/556.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4562) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4575), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1179.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4577) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4588), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/116.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4590) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4600), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/726.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4603) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4613), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1056.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4615) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4625), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/930.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4628) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4638), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1248.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4640) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4651), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1053.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4653) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4663), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/635.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4801) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4811), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/297.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4813) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4823), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/391.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4825) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7180), "https://picsum.photos/640/480/?image=383", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7524) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7699), "https://picsum.photos/640/480/?image=429", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7703) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7717), "https://picsum.photos/640/480/?image=925", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7719) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7729), "https://picsum.photos/640/480/?image=599", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7732) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7742), "https://picsum.photos/640/480/?image=840", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7745) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7755), "https://picsum.photos/640/480/?image=148", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7757) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7767), "https://picsum.photos/640/480/?image=1019", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7770) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7780), "https://picsum.photos/640/480/?image=730", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7783) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7793), "https://picsum.photos/640/480/?image=1045", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7795) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7805), "https://picsum.photos/640/480/?image=705", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7808) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7818), "https://picsum.photos/640/480/?image=795", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7820) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7831), "https://picsum.photos/640/480/?image=341", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7833) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7843), "https://picsum.photos/640/480/?image=494", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7845) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7889), "https://picsum.photos/640/480/?image=296", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7891) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7903), "https://picsum.photos/640/480/?image=682", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7905) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7916), "https://picsum.photos/640/480/?image=121", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7918) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7928), "https://picsum.photos/640/480/?image=146", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7930) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7940), "https://picsum.photos/640/480/?image=822", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7942) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7952), "https://picsum.photos/640/480/?image=286", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7955) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7965), "https://picsum.photos/640/480/?image=575", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7967) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5336), false, 7, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5338), 10 },
                    { 1, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(4296), false, 16, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(4733), 1 },
                    { 19, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5321), true, 10, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5323), 4 },
                    { 18, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5306), true, 6, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5309), 14 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5293), false, 13, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5295), 7 },
                    { 16, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5278), false, 19, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5281), 11 },
                    { 14, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5247), false, 9, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5250), 20 },
                    { 13, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5188), true, 10, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5190), 13 },
                    { 12, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5171), true, 10, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5173), 1 },
                    { 11, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5157), true, 9, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5159), 15 },
                    { 15, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5263), false, 18, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5266), 2 },
                    { 9, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5127), true, 19, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5129), 16 },
                    { 8, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5112), true, 3, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5114), 12 },
                    { 7, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5098), true, 11, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5100), 8 },
                    { 6, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5083), false, 10, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5085), 12 },
                    { 10, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5142), true, 6, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5145), 1 },
                    { 5, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5067), false, 4, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5070), 19 },
                    { 4, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5052), true, 17, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5055), 18 },
                    { 3, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5034), false, 5, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5037), 9 },
                    { 2, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5012), false, 13, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5016), 2 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Provident accusamus fuga facilis nihil odio odio quam dolorem. Reprehenderit sed aspernatur. Rerum soluta commodi non tenetur assumenda dolor.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(2567), 25, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(2916) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Excepturi repudiandae rerum explicabo et velit non et.\nDoloribus earum recusandae fugiat iusto sed ut quia ea eum.\nDolor praesentium explicabo.\nLaboriosam ut aut rem est voluptatem non a nobis placeat.\nNon rem minus iusto voluptatibus et exercitationem est eveniet quod.\nExplicabo sint et non necessitatibus culpa.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(4598), 26, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(4611) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Aut possimus veniam iusto id consectetur deserunt inventore voluptatibus. Dicta molestias officia dolores. Molestiae in nesciunt atque pariatur velit incidunt.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(4773), 39, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(4777) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "In molestiae aliquid libero temporibus at officiis impedit et.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5542), 40, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5552) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Sit quo possimus.\nAut consequatur veritatis quo.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5617), 26, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5621) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Sed dolorum quod pariatur doloremque reiciendis. Autem dolorem impedit ipsam praesentium incidunt quidem debitis asperiores et. Vero doloribus eum fugiat ea voluptatum quisquam et qui. Nobis magni eligendi repellendus ab ducimus tempore dicta possimus. Consequuntur error magnam enim.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5809), 38, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5813) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Omnis quis sit quisquam soluta rerum doloremque maxime.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5910), 21, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5914) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Consequuntur temporibus non sequi odit.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5956), 21, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5959) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Delectus temporibus ut corrupti facilis nostrum id quasi voluptatibus.\nAperiam asperiores totam vitae.\nDelectus ducimus consequatur facere corporis quam ullam.\nQuos alias suscipit eaque explicabo quia illum aut voluptates.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6062), 27, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6066) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Atque adipisci et id aut tenetur.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6134), 28, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6137) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "magni", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6357), 27, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6366) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Non omnis aut consequatur nam sapiente quod.\nNemo aliquam rerum et dolorem consequuntur.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6443), 23, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6447) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Sit atque magnam error sit exercitationem laudantium repudiandae.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6482), 37, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6485) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Sint et tempora ut dolores eius fuga. Et quod quo recusandae deleniti consequatur sunt modi. Odit blanditiis nulla assumenda in expedita laboriosam facilis possimus aut.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6614), 39, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6618) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "deleniti", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6639), 33, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6642) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Itaque tempora doloribus blanditiis pariatur itaque placeat quod ut.\nUt non autem est ut sequi esse dolorem.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6737), 37, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6741) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Aliquam autem esse est doloribus enim quaerat aspernatur rerum vel.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6783), 25, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6786) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Sed alias asperiores fugiat est repudiandae et perferendis.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6822), 25, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6825) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Voluptas adipisci consequuntur nulla aut.\nAb ut laborum ipsa consequuntur rerum.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6874), 30, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6877) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Qui est dicta ea tempora aperiam esse ipsum maiores rem.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6973), 30, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6977) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 15, 22, 12, 40, 9, DateTimeKind.Local).AddTicks(204), "Domenick38@hotmail.com", "aLbBpwfOh1oA5I13T9H5eMvdopmpq2iLK1iq7He7EwY=", "jSfaPr7S1iDBhWtcinjyWXG50uAUw5GOGSiEo1nQ6Fk=", new DateTime(2021, 6, 15, 22, 12, 40, 9, DateTimeKind.Local).AddTicks(572), "Justyn.Pagac" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 22, 12, 40, 14, DateTimeKind.Local).AddTicks(5358), "Loraine24@gmail.com", "J2N+sAZ2gboIe8fH/iPVUdu3Ru4yL95IJ4LDRUilET0=", "6sSOEgHwhLT8vfzShP2ZYVvDVLXGZd9L9K2uZ9/yjkw=", new DateTime(2021, 6, 15, 22, 12, 40, 14, DateTimeKind.Local).AddTicks(5381), "Merritt.Murphy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 15, 22, 12, 40, 19, DateTimeKind.Local).AddTicks(6875), "Barton63@hotmail.com", "IWFChfqF+D8JSgCgcstLTct1oohqPhS16ZygAZ15IhU=", "Ja4CRdZdjJjddZPtPvxhGmmf2MEn0ojGYa/fNO3ST0M=", new DateTime(2021, 6, 15, 22, 12, 40, 19, DateTimeKind.Local).AddTicks(6884), "Hyman30" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 15, 22, 12, 40, 24, DateTimeKind.Local).AddTicks(8105), "Jarred.Schoen74@yahoo.com", "T0gtX9NeazJaMOnC3zqAK4Cqfd278N/zDjdD5qjZgdg=", "OLkLEK0JNQezTReZGgFppgt7sEVfp+QYEQ6GB4AMPLw=", new DateTime(2021, 6, 15, 22, 12, 40, 24, DateTimeKind.Local).AddTicks(8112), "Gwendolyn_McGlynn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 6, 15, 22, 12, 40, 30, DateTimeKind.Local).AddTicks(2439), "Isadore_Boyer@yahoo.com", "qIRPBDjWrFHIU8k1tOcz+h0L0CVGNYK0h/ydsXKwRUI=", "Erv1x7D92vrWnHyYpRyRP3u810ghpkbHXIr386WOceI=", new DateTime(2021, 6, 15, 22, 12, 40, 30, DateTimeKind.Local).AddTicks(2446), "Tianna57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 15, 22, 12, 40, 35, DateTimeKind.Local).AddTicks(3600), "Winona_Bechtelar18@yahoo.com", "G6c8zXHemXSTuRIrwOeXGUEV4AfEoYYQfxsrYHE/66M=", "8Sm1w0zoHlHOgWi5z6iWcPyglXb9/kSXdwEBTrxe89Y=", new DateTime(2021, 6, 15, 22, 12, 40, 35, DateTimeKind.Local).AddTicks(3606), "Raymond.Nader18" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 6, 15, 22, 12, 40, 40, DateTimeKind.Local).AddTicks(4408), "Carli47@yahoo.com", "vpp+uIvmtapKHbymdneOOBqjagSX4xVsL1ieN3HBiHs=", "1c/dTkvTfyZ55/xnWAD2CjsL5va6VuvsXSRDVVHq3cw=", new DateTime(2021, 6, 15, 22, 12, 40, 40, DateTimeKind.Local).AddTicks(4415), "Sofia.Veum" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 15, 22, 12, 40, 45, DateTimeKind.Local).AddTicks(7330), "Missouri_Gorczany@gmail.com", "Vkbp887bu6FqJL9hBK1kJPNCyYYNX8ET+UjeIogDaGg=", "5TyfkrInrE+yvIoM+tWC80n8i2gPg0XhWmLmsbXxSSg=", new DateTime(2021, 6, 15, 22, 12, 40, 45, DateTimeKind.Local).AddTicks(7337), "Sigmund_Kuphal" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 15, 22, 12, 40, 50, DateTimeKind.Local).AddTicks(9379), "Jules_Romaguera73@yahoo.com", "NZ4HyB/HwmDHWA7DxJv2FKufMyyMOpktXBEviMaY0KQ=", "3HcOV+I2dbmV/5MtJriOqjUx2RJdPZr47ctMoBmObes=", new DateTime(2021, 6, 15, 22, 12, 40, 50, DateTimeKind.Local).AddTicks(9385), "Maggie_MacGyver63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 15, 22, 12, 40, 56, DateTimeKind.Local).AddTicks(237), "Lew.Bergstrom36@hotmail.com", "Dq70kKbLkCgRgR/PndDWdRTwUs1wYSq9CM3mTDaskcg=", "EMeuq3DOcd+pe9sJQ45/O0FJTosbA3RfvrcnbYyUy2A=", new DateTime(2021, 6, 15, 22, 12, 40, 56, DateTimeKind.Local).AddTicks(244), "Norene13" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 15, 22, 12, 40, 61, DateTimeKind.Local).AddTicks(2684), "Ernesto_Brekke71@yahoo.com", "0SlafbM9ZTfUlpq9RkFqeZU2nNqubDwo76l7vaRyfiM=", "zmwPIm1FAfykL8Miavmqy3Ey11dHGiXLmQymn9jtfSU=", new DateTime(2021, 6, 15, 22, 12, 40, 61, DateTimeKind.Local).AddTicks(2691), "Jackson24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 15, 22, 12, 40, 66, DateTimeKind.Local).AddTicks(4471), "Joannie_Schimmel67@yahoo.com", "wHRPQjQCRZ5Y/S/NdIVyWeq+Nq9/2AFhLHmXAKqxo00=", "WRfS1q8+oxdafYG9Z+RZ5X/31fRwnIqIDWzaHk6Q1Mk=", new DateTime(2021, 6, 15, 22, 12, 40, 66, DateTimeKind.Local).AddTicks(4478), "Wilmer_Robel" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 15, 22, 12, 40, 71, DateTimeKind.Local).AddTicks(5387), "Luis84@hotmail.com", "3Szv2S6Q9Q2Z2L4gLlMKexZZOIIt9dmcrPiXyO5k2hQ=", "20B4TlxfDdns3x3wgL23dyvSVwsK4yJ3nIUNXwxu7G4=", new DateTime(2021, 6, 15, 22, 12, 40, 71, DateTimeKind.Local).AddTicks(5394), "Pinkie0" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 40, 77, DateTimeKind.Local).AddTicks(1498), "Laron_Prosacco64@hotmail.com", "2U06ikbMSAPhDJ5k8FqvEc/3YffamH79aHfIs+Pnr1s=", "RlOJkNn4j3pMoKmnGHS7efSjczY5u7w8DdC4KK2SdR8=", new DateTime(2021, 6, 15, 22, 12, 40, 77, DateTimeKind.Local).AddTicks(1505), "Rachelle.Treutel63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 15, 22, 12, 40, 82, DateTimeKind.Local).AddTicks(2297), "Andres.Carroll59@yahoo.com", "NCf4lFghe5inWkor4Is0R2nnIIYzPkX7axPJh/9eo8o=", "iO8qTz/LvdpjIemzI/n33K9RfNz8rS+CgZpWthsn3MA=", new DateTime(2021, 6, 15, 22, 12, 40, 82, DateTimeKind.Local).AddTicks(2303), "Alexys93" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 15, 22, 12, 40, 87, DateTimeKind.Local).AddTicks(3468), "Destiny65@hotmail.com", "fiio+oosEc6GztLZd7Gh199td5LmYNpSVRq87sTA/to=", "gelED9dLRvUYn2Xq0s/4jdFXgfzmrn80uZLPQCT22mg=", new DateTime(2021, 6, 15, 22, 12, 40, 87, DateTimeKind.Local).AddTicks(3475), "Donna47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 15, 22, 12, 40, 92, DateTimeKind.Local).AddTicks(7918), "Gabriel.Welch@gmail.com", "cWfGPSavA8lT2JjaYTcNHcKRBqrVtAR/g/AasyVyocQ=", "aBSyFK28YQSlPqIO1W3JsFXNgqcz7JcoH4GkX31cKX8=", new DateTime(2021, 6, 15, 22, 12, 40, 92, DateTimeKind.Local).AddTicks(7926), "Emile.Baumbach68" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 15, 22, 12, 40, 97, DateTimeKind.Local).AddTicks(9150), "Austyn.Nienow@yahoo.com", "CPN3X+Mya0Yx08W5ql+Ud5jsEK24QosoSGqT5wMFpr4=", "6W20/Yzt50npfS6BYlVGAxEXya7MyIiv8L0TCddoCY8=", new DateTime(2021, 6, 15, 22, 12, 40, 97, DateTimeKind.Local).AddTicks(9156), "Frederique94" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 15, 22, 12, 40, 103, DateTimeKind.Local).AddTicks(192), "Wiley1@hotmail.com", "U+ld2O34IaYn7gU/Sekwxh3D1SAYZgpHjPS4tMdjRbk=", "3zpqdjaLnk03fFh1LY8n/fdXsaGpAqQ+J3pEmq8M0zM=", new DateTime(2021, 6, 15, 22, 12, 40, 103, DateTimeKind.Local).AddTicks(198), "Keshawn.Herman7" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 22, 12, 40, 108, DateTimeKind.Local).AddTicks(956), "Ettie.Kling@hotmail.com", "TfWoJwJ5RrwYFUhz47ZJZ+nFp0QYg+0CYQulqcsnL0E=", "gIwvNJj+FYlmB4JT4mg1/S1ksTsa2RlqNGjous7j1jI=", new DateTime(2021, 6, 15, 22, 12, 40, 108, DateTimeKind.Local).AddTicks(962), "Kayden_Kunde79" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 40, 113, DateTimeKind.Local).AddTicks(5303), "RRVk7tjt0srr1+O3t4pILWt66XlQiQm1cyJxYIu5QdI=", "eKlX0cIXyvBt3UvMyacnI+nNhLi+p2PfqHhKYAVK4TY=", new DateTime(2021, 6, 15, 22, 12, 40, 113, DateTimeKind.Local).AddTicks(5303) });
        }
    }
}
