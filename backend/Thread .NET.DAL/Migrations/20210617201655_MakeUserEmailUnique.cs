﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class MakeUserEmailUnique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 5, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(5572), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(5916), 2 },
                    { 18, 12, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6544), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6547), 17 },
                    { 17, 1, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6530), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6532), 21 },
                    { 15, 13, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6489), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6492), 3 },
                    { 14, 2, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6409), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6411), 8 },
                    { 13, 13, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6394), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6397), 4 },
                    { 12, 14, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6381), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6383), 6 },
                    { 11, 9, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6364), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6368), 7 },
                    { 19, 11, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6557), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6560), 2 },
                    { 10, 3, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6316), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6319), 19 },
                    { 8, 15, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6289), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6291), 11 },
                    { 7, 9, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6274), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6277), 16 },
                    { 6, 12, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6260), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6262), 7 },
                    { 5, 6, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6246), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6249), 17 },
                    { 4, 8, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6232), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6235), 12 },
                    { 3, 5, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6217), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6220), 13 },
                    { 2, 16, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6195), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6200), 12 },
                    { 9, 10, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6302), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6304), 7 },
                    { 20, 5, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6571), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6574), 1 },
                    { 16, 16, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6505), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6507), 9 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Amet maiores corrupti perspiciatis aut cupiditate eveniet ut.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(3667), 20, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4005) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Doloremque unde et consectetur modi rerum.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4391), 11, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4396) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Sint rerum nihil tempora beatae nihil voluptates totam nostrum ut.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4449), 19, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4452) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Est at ut velit.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4493), 7, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4496) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Quos doloribus rerum quis blanditiis voluptates occaecati ipsa maxime sit.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4543), 6, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4546) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Omnis omnis unde minima quaerat laudantium dolor.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4577), 9, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4580) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Aliquam aut sunt necessitatibus eveniet et modi.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4666), 13, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4670) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Laborum praesentium non facere suscipit.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4706), 4, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4709) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Et est odit eum.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4738), 9, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4740) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Officiis corporis eligendi voluptatem error.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4769), 7, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4771) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Vel et commodi.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4801), 1, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4804) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 18, "Labore cupiditate quis et hic ut voluptatem.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4841), new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4844) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Quos ullam asperiores placeat nihil et officiis laboriosam dolorem cupiditate.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4884), 10, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4886) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Distinctio quae eius nemo qui unde quia.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4951), 18, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4954) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Natus et velit id.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4982), 15, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4985) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Quae accusantium molestias placeat distinctio ut magni.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5017), 8, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5019) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Possimus accusantium porro dolores animi quod asperiores impedit eos minus.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5059), 19, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5062) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Tempore aut est sequi iusto.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5090), 11, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5093) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Qui et libero deleniti repellat doloribus dolor alias dolor.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5157), 16, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5160) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Libero consequatur rerum.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5186), 13, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5188) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(4857), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/875.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(8271) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(8807), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/559.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(8811) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(8993), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/327.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(8996) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9007), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/984.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9009) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9065), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1220.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9068) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9080), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1109.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9083) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9093), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/629.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9096) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9106), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/733.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9109) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9119), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/129.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9121) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9131), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/128.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9134) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9144), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/951.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9147) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9157), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/913.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9159) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9170), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1222.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9172) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9182), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/278.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9185) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9325), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/647.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9328) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9338), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1032.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9340) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9350), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/882.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9352) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9363), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/726.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9365) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9375), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1237.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9378) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9388), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/259.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9390) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5332), "https://picsum.photos/640/480/?image=622", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5701) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5875), "https://picsum.photos/640/480/?image=247", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5880) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5895), "https://picsum.photos/640/480/?image=723", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5897) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5908), "https://picsum.photos/640/480/?image=920", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5911) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5921), "https://picsum.photos/640/480/?image=937", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5924) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5934), "https://picsum.photos/640/480/?image=327", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5936) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5946), "https://picsum.photos/640/480/?image=514", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5949) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5959), "https://picsum.photos/640/480/?image=460", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5962) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5972), "https://picsum.photos/640/480/?image=72", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5975) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5985), "https://picsum.photos/640/480/?image=342", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5988) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5997), "https://picsum.photos/640/480/?image=514", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6000) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6010), "https://picsum.photos/640/480/?image=490", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6013) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6023), "https://picsum.photos/640/480/?image=123", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6026) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6036), "https://picsum.photos/640/480/?image=479", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6038) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6102), "https://picsum.photos/640/480/?image=627", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6105) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6117), "https://picsum.photos/640/480/?image=449", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6119) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6129), "https://picsum.photos/640/480/?image=268", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6132) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6141), "https://picsum.photos/640/480/?image=513", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6144) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6154), "https://picsum.photos/640/480/?image=34", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6157) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6167), "https://picsum.photos/640/480/?image=457", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6169) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4422), false, 11, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4425), 7 },
                    { 1, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(3473), false, 5, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(3814), 19 },
                    { 19, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4409), false, 3, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4411), 8 },
                    { 18, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4395), true, 3, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4397), 12 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4381), true, 12, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4383), 4 },
                    { 16, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4367), false, 8, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4370), 14 },
                    { 14, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4340), false, 8, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4342), 16 },
                    { 13, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4326), true, 16, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4329), 16 },
                    { 12, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4313), false, 5, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4315), 11 },
                    { 11, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4299), true, 9, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4301), 1 },
                    { 15, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4353), true, 1, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4356), 4 },
                    { 9, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4270), false, 13, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4272), 2 },
                    { 8, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4256), false, 5, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4258), 16 },
                    { 7, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4242), true, 1, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4244), 2 },
                    { 6, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4226), false, 8, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4228), 3 },
                    { 10, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4283), false, 13, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4286), 19 },
                    { 5, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4211), true, 9, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4214), 15 },
                    { 4, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4197), false, 2, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4199), 17 },
                    { 3, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4182), false, 15, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4185), 3 },
                    { 2, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4160), true, 20, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4164), 13 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Rerum ea omnis qui. Ullam ipsam corrupti eveniet vitae et non ut. Quod consectetur aut. Laudantium aut debitis mollitia et qui saepe eos.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(2267), 29, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(2633) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Sunt est sed deleniti mollitia voluptatibus. Officia aspernatur nihil beatae dicta laboriosam aut. Aut et sit dolor sit.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(3215), 37, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(3223) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "sit", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(3516), 21, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(3526) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Dolorem necessitatibus amet expedita distinctio tempore suscipit odio fugit.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(4110), 32, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(4120) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "natus", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(4156), 23, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(4159) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Et nihil dolorum at quos aut quae.\nIn eaque et temporibus iusto deleniti animi quo omnis non.\nQuam molestiae libero rerum.\nSaepe ea officiis sunt est eligendi dolorem voluptatum non.\nNon sapiente non aut qui architecto est.\nAspernatur exercitationem qui voluptas consequuntur libero est nulla vel.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5556), 24, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5568) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Labore dolor temporibus.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5664), 40, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5668) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "voluptatum", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5689), 23, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5692) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Eveniet ut quidem quas temporibus. Neque consequatur tempore similique ut perferendis error incidunt sed. Molestiae ipsam aut aperiam ratione qui aut dolor nihil aut.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5791), 26, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5794) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "dolorum", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5813), 25, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5816) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Ea ut nisi deserunt.\nMinima sunt velit libero iusto.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5898), 36, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5902) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Sed similique eos aut fugit dignissimos nostrum. Beatae omnis unde sit cum ab. Molestiae suscipit et alias et.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5974), 40, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5978) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "In ipsa nemo quod est et consequatur quibusdam fuga.\nFacilis natus similique tenetur repellendus aut.\nVoluptatem vel quibusdam hic saepe ea enim.\nDignissimos saepe eveniet ut sint dolorum nulla.\nEx sed ea maiores.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6159), 35, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6163) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "dolor", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6185), 31, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6187) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Voluptas unde eius eligendi atque.\nVoluptates officiis et soluta soluta.\nIncidunt sed quisquam totam natus odit quibusdam et consequatur non.\nVel quia autem eum et et.\nNulla est id ratione cum ut.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6326), 34, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6330) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "ullam", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6349), 39, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6352) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Asperiores corrupti tempora iusto quo aut rerum quo.\nRepellat iste magni voluptas sunt eligendi quos aliquam quaerat eum.\nTenetur reprehenderit vitae non autem temporibus perferendis architecto et.\nQuae expedita natus et consequuntur corporis animi numquam ut.\nEveniet rerum optio ratione quia numquam.\nAut nisi aut cum veritatis ipsum sit est id.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6543), 29, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6548) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "et", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6567), 40, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6570) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Tenetur eveniet sunt.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6595), 24, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6598) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 16, "Eaque ex earum itaque et optio itaque voluptate neque hic.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6671), new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6674) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 17, 23, 16, 54, 775, DateTimeKind.Local).AddTicks(8776), "Rusty_McClure44@yahoo.com", "n10moKj3hYIfd4Db+Jmpj9mLKXxQBW68Upwnybn9pw4=", "vSLPY7B6VrHA3CqfNyWqQ97gEtVZql+RnBanoXHhkrM=", new DateTime(2021, 6, 17, 23, 16, 54, 775, DateTimeKind.Local).AddTicks(9221), "Mariela52" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 17, 23, 16, 54, 781, DateTimeKind.Local).AddTicks(5957), "June.Harris24@yahoo.com", "hn2zPFyh2mGotkfw5zWhIADolgElEYaYKBa3XL+/omo=", "n4WYX6Jd6/etbU4eYjHr+mfh3wASZe6XECpFqZxc/bM=", new DateTime(2021, 6, 17, 23, 16, 54, 781, DateTimeKind.Local).AddTicks(5974), "Beatrice54" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 17, 23, 16, 54, 786, DateTimeKind.Local).AddTicks(6899), "Dimitri.Lueilwitz87@gmail.com", "DJ+W3CoNJTCa/yu9Z83ocm88v2fnCobgDB2DqWwH828=", "C81dUSt8Tf5BScxQG1vuTtDbKGk6+ssQy1JnlFnTTd4=", new DateTime(2021, 6, 17, 23, 16, 54, 786, DateTimeKind.Local).AddTicks(6907), "Iva24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 6, 17, 23, 16, 54, 791, DateTimeKind.Local).AddTicks(8017), "Lenore94@hotmail.com", "J/xPkzzhbHgllcxpdMzIaCxMajR+rXmKjbdPUXPzjAI=", "5jGqR7YOhU+dEGTqUv+XRbx4K2TWVrPLRLQmWJjI53I=", new DateTime(2021, 6, 17, 23, 16, 54, 791, DateTimeKind.Local).AddTicks(8024), "Nathanael56" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 17, 23, 16, 54, 797, DateTimeKind.Local).AddTicks(1701), "Daniella54@yahoo.com", "S7aEE05FBTMRCr0drXkEsTLvxxsQNlrLkK+IvdQcda0=", "hui2SluzHppwLv8yyQjT3+Tp7yvh3OjCPLnc2CZsOz8=", new DateTime(2021, 6, 17, 23, 16, 54, 797, DateTimeKind.Local).AddTicks(1709), "Stan94" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 17, 23, 16, 54, 802, DateTimeKind.Local).AddTicks(2449), "Jazmyn52@yahoo.com", "YhGxdXlD8TU1ZTF1kyHu8e2dq/5gNMwQM2DoyNdMxQk=", "GAXTlRLfK3+JdFRBbuY0bE2SQGNpmLykZnrcZ/h0uQk=", new DateTime(2021, 6, 17, 23, 16, 54, 802, DateTimeKind.Local).AddTicks(2456), "Pearline62" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 17, 23, 16, 54, 807, DateTimeKind.Local).AddTicks(3166), "Jayce_Wolff8@yahoo.com", "56gERSIqsC5dodxax94YC4BLtUQlmn3eEiFAaaw0/Ks=", "BHbQK5Ix9IUdCRcKZmgRyPTosRIPSrAPkVdNIuCOM+Q=", new DateTime(2021, 6, 17, 23, 16, 54, 807, DateTimeKind.Local).AddTicks(3173), "Lawson.Hauck69" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 6, 17, 23, 16, 54, 812, DateTimeKind.Local).AddTicks(5543), "Sigrid.Conn65@gmail.com", "v+N71Apqz5TOkEQMa9LYuLZZXLGWkNJUc8O+MOvClDw=", "HmDAeTdpIchBi6xuPLMglVi/c54hhnl0VbQgZlnxlqA=", new DateTime(2021, 6, 17, 23, 16, 54, 812, DateTimeKind.Local).AddTicks(5551), "Adrianna.McKenzie" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 17, 23, 16, 54, 817, DateTimeKind.Local).AddTicks(6312), "Juana_Schowalter@gmail.com", "Ffa8a/4ipETzG15mTW19e2acVX0kIeLbCiqoZfTegRs=", "sWWScfJEYXcGGYYlTUPssCk8MWmsEQ2787qIMiyyk6A=", new DateTime(2021, 6, 17, 23, 16, 54, 817, DateTimeKind.Local).AddTicks(6319), "Cristal.Kerluke33" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 17, 23, 16, 54, 822, DateTimeKind.Local).AddTicks(7757), "Hazel49@yahoo.com", "ERg1P2fhiDpm8I5d6YVbAL/4n/MTJRkwlzx/hOppguk=", "SQ9O6bH9AXNHunpaptYdkrAxMRtSfBzgAL2lFx73yVU=", new DateTime(2021, 6, 17, 23, 16, 54, 822, DateTimeKind.Local).AddTicks(7762), "Lois27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 17, 23, 16, 54, 828, DateTimeKind.Local).AddTicks(3397), "Haskell_Robel@gmail.com", "+ZbL0WOrt7pG65y8f5tWm92zRMys8xVtC/fK0lvZSdY=", "nWsUa3cG2xtO0q0T0z/eiMnASM2NjJwmBEQNL4f9R48=", new DateTime(2021, 6, 17, 23, 16, 54, 828, DateTimeKind.Local).AddTicks(3405), "Rosalinda82" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 833, DateTimeKind.Local).AddTicks(6848), "Monroe_Kulas34@yahoo.com", "ou5XPQhdZVJErDTQc6azUfKD+G6L6FWMDI4aEcBCcT8=", "h285kvwToulClj30ZvEU6+0TNVDtW4GzefTPuePJUso=", new DateTime(2021, 6, 17, 23, 16, 54, 833, DateTimeKind.Local).AddTicks(6855), "Holly_Hettinger" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 17, 23, 16, 54, 838, DateTimeKind.Local).AddTicks(7562), "Berta47@yahoo.com", "9Kzw02mzyrealbeWni/FWEgVYu1mQkiTqAoEEdL04mk=", "tb9iHciezZKfMqP2DrhBZnT9TAdmWyysVVt7bCYSctw=", new DateTime(2021, 6, 17, 23, 16, 54, 838, DateTimeKind.Local).AddTicks(7568), "Adrianna.Goyette23" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 17, 23, 16, 54, 843, DateTimeKind.Local).AddTicks(9449), "Angus.Wiza@yahoo.com", "nTGnJt7nIHwvtEsuuSsIeFiVodhRlrro5pT4Zv/lHAQ=", "F9dASjZDfhFgkLyMOplzI89UNVWcuALVrUJyb4s6aps=", new DateTime(2021, 6, 17, 23, 16, 54, 843, DateTimeKind.Local).AddTicks(9457), "Gilbert25" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 17, 23, 16, 54, 849, DateTimeKind.Local).AddTicks(527), "Yesenia12@yahoo.com", "+cNZqtDO5DKz/LNKtDaPopx4ZYhmTsfn3KO3pWtoqiY=", "+fY4ddOcZeY2RFLRzSIuEh5MSbnEj/kC9iqtbx3Z71Y=", new DateTime(2021, 6, 17, 23, 16, 54, 849, DateTimeKind.Local).AddTicks(532), "Raheem_Sipes97" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 17, 23, 16, 54, 854, DateTimeKind.Local).AddTicks(1287), "Shaina14@gmail.com", "kqOBP3UxOEjzuymiN+Y+0205roO5gd+4uQczR7/LE4U=", "DTpDzR7ff2TVfZAI5P5inT8jvO0Pdu1YQ7ltnescrDk=", new DateTime(2021, 6, 17, 23, 16, 54, 854, DateTimeKind.Local).AddTicks(1294), "Clement77" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 17, 23, 16, 54, 859, DateTimeKind.Local).AddTicks(3922), "Brooke_Parisian@yahoo.com", "q9kPcdrpTcuUYum0MgCR4wZxx2VCAsI6SZUNLNOGPTs=", "lGZ98Jr42mJjHjNC2bOVLn6IhuOeDmmTClq+xFiSLzU=", new DateTime(2021, 6, 17, 23, 16, 54, 859, DateTimeKind.Local).AddTicks(3930), "Arlene34" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 17, 23, 16, 54, 864, DateTimeKind.Local).AddTicks(5311), "Eino62@hotmail.com", "YeTBzpSzYSS4l2atojMMCuz5YYIY9lwrwLEDw9G1l8o=", "3oYWn7i7ol6A5noZDEpq1Ci9RpAozx8BCjCRhLEdpLo=", new DateTime(2021, 6, 17, 23, 16, 54, 864, DateTimeKind.Local).AddTicks(5318), "Marilie.Ruecker93" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 17, 23, 16, 54, 869, DateTimeKind.Local).AddTicks(6020), "Rhianna.Marquardt65@hotmail.com", "O4OAEvOdUwfqp7S5MTI7ZmM5RYKeJAX/+JPgEL08uV4=", "9Lpt32LHIyOpNqOJWyKcVzSlb2iGjzO+mxOO6ozXxjY=", new DateTime(2021, 6, 17, 23, 16, 54, 869, DateTimeKind.Local).AddTicks(6026), "Felipe.Reilly46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 17, 23, 16, 54, 875, DateTimeKind.Local).AddTicks(984), "Garry_Trantow97@gmail.com", "m0CYgwFtC4SohPIgAiqNvZvbu75Ocg8DGvgDgoiMd80=", "AjSeDTZ+pZueOWpTQdQ0kcJFhrUaO6mL6bfPSn/s2sI=", new DateTime(2021, 6, 17, 23, 16, 54, 875, DateTimeKind.Local).AddTicks(991), "Kira38" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 880, DateTimeKind.Local).AddTicks(1923), "DTJWj+MiJodeSN2K9dV8DH0/miL7xYR/RETnlqznzaQ=", "7JfqcPG422zBYcuDpzEgtFkyVi7ZGmPORr9FMW4ORC8=", new DateTime(2021, 6, 17, 23, 16, 54, 880, DateTimeKind.Local).AddTicks(1923) });

            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                table: "Users",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Users_Email",
                table: "Users");

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 8, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4133), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4550), 14 },
                    { 18, 16, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5101), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5103), 3 },
                    { 17, 6, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5088), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5090), 7 },
                    { 15, 6, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5059), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5062), 10 },
                    { 14, 12, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5045), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5048), 5 },
                    { 13, 15, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5028), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5031), 18 },
                    { 12, 18, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4970), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4972), 10 },
                    { 11, 16, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4956), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4959), 4 },
                    { 19, 15, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5115), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5117), 10 },
                    { 10, 18, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4942), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4944), 6 },
                    { 8, 9, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4910), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4912), 19 },
                    { 7, 7, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4895), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4897), 11 },
                    { 6, 6, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4879), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4882), 16 },
                    { 5, 14, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4864), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4867), 21 },
                    { 4, 20, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4850), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4852), 15 },
                    { 3, 16, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4835), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4837), 20 },
                    { 2, 17, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4812), true, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4816), 21 },
                    { 9, 6, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4923), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(4926), 11 },
                    { 20, 10, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5128), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5131), 10 },
                    { 16, 15, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5074), false, new DateTime(2021, 6, 17, 17, 45, 7, 562, DateTimeKind.Local).AddTicks(5076), 7 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Voluptatem fugit consequatur quasi.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1016), 11, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1341) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Rerum officiis explicabo deserunt qui.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1692), 2, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1698) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Voluptas ut earum vel magni rerum nam iure nemo.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1747), 18, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1751) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Amet quod vitae est id dolorum modi illo est.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1791), 3, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1794) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Qui commodi hic aut praesentium sint exercitationem quos.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1836), 9, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1839) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Velit repellendus facilis.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1863), 4, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1866) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Est inventore aut harum placeat ipsum sed.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1901), 9, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1904) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Eum voluptatum repellat et quia incidunt non molestiae assumenda repellat.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1978), 19, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(1982) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Saepe vel quia est cum tempore.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2015), 20, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2018) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Doloremque qui rerum praesentium.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2045), 2, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2048) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Consectetur maxime sit aut recusandae laudantium sit mollitia.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2085), 15, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2087) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 16, "In porro eligendi adipisci.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2114), new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2117) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Maiores magnam sunt in non sed qui voluptatem.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2154), 9, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2156) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Ex eos doloribus aut modi in magnam.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2220), 19, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2223) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Et commodi culpa id recusandae quos.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2254), 5, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2257) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Quo impedit sunt autem aliquam nulla.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2289), 16, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2292) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Ut nihil voluptates autem.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2320), 2, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2323) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Aut consectetur laudantium quos sed earum ut officiis non.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2358), 18, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2361) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Autem deserunt fuga ullam omnis velit est fugiat ut quasi.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2400), 6, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2403) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Dolor aperiam earum.", new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2457), 11, new DateTime(2021, 6, 17, 17, 45, 7, 555, DateTimeKind.Local).AddTicks(2460) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 412, DateTimeKind.Local).AddTicks(8497), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/649.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(904) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1206), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1247.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1211) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1226), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/625.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1229) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1240), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/202.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1243) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1253), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/420.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1256) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1266), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/565.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1268) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1278), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/851.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1281) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1335), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/334.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1337) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1349), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/67.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1352) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1363), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1212.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1365) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1375), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/920.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1378) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1388), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/905.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1391) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1401), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/295.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1403) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1413), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/385.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1416) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1426), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1028.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1429) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1439), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/761.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1441) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1451), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/500.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1454) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1463), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1085.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1466) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1476), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/180.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1478) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1489), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/813.jpg", new DateTime(2021, 6, 17, 17, 45, 7, 413, DateTimeKind.Local).AddTicks(1491) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(1708), "https://picsum.photos/640/480/?image=518", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2046) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2164), "https://picsum.photos/640/480/?image=533", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2168) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2181), "https://picsum.photos/640/480/?image=197", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2183) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2261), "https://picsum.photos/640/480/?image=281", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2264) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2277), "https://picsum.photos/640/480/?image=447", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2279) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2290), "https://picsum.photos/640/480/?image=632", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2293) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2304), "https://picsum.photos/640/480/?image=65", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2306) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2317), "https://picsum.photos/640/480/?image=163", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2319) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2330), "https://picsum.photos/640/480/?image=1063", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2332) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2342), "https://picsum.photos/640/480/?image=203", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2345) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2355), "https://picsum.photos/640/480/?image=482", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2358) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2368), "https://picsum.photos/640/480/?image=240", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2370) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2381), "https://picsum.photos/640/480/?image=463", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2383) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2393), "https://picsum.photos/640/480/?image=327", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2396) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2407), "https://picsum.photos/640/480/?image=811", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2409) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2419), "https://picsum.photos/640/480/?image=226", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2422) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2474), "https://picsum.photos/640/480/?image=517", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2477) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2488), "https://picsum.photos/640/480/?image=407", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2490) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2500), "https://picsum.photos/640/480/?image=810", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2503) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2513), "https://picsum.photos/640/480/?image=41", new DateTime(2021, 6, 17, 17, 45, 7, 416, DateTimeKind.Local).AddTicks(2516) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1341), true, 5, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1343), 6 },
                    { 1, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(400), false, 11, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(740), 17 },
                    { 19, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1326), true, 17, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1329), 1 },
                    { 18, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1312), true, 8, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1314), 14 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1298), true, 16, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1300), 21 },
                    { 16, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1284), true, 1, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1286), 11 },
                    { 14, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1253), true, 1, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1256), 10 },
                    { 13, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1227), true, 11, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1230), 1 },
                    { 12, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1213), false, 3, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1216), 20 },
                    { 11, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1198), false, 8, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1201), 21 },
                    { 15, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1268), false, 1, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1271), 16 },
                    { 9, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1170), false, 10, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1173), 18 },
                    { 8, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1155), false, 5, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1158), 21 },
                    { 7, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1141), true, 3, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1143), 3 },
                    { 6, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1126), true, 13, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1129), 1 },
                    { 10, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1185), true, 7, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1187), 9 },
                    { 5, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1109), false, 20, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1111), 15 },
                    { 4, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1093), false, 5, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1096), 17 },
                    { 3, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1076), false, 8, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1079), 16 },
                    { 2, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1010), false, 7, new DateTime(2021, 6, 17, 17, 45, 7, 559, DateTimeKind.Local).AddTicks(1015), 2 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Vitae consequatur harum.\nIn tempora cum.\nOmnis ut qui.\nDebitis earum eius aliquid repellendus fugiat nisi et.\nUt fugit necessitatibus officiis.\nRatione iure sed minus voluptate aliquid molestiae sit error.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(2863), 27, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(3215) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "eos", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(3720), 33, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(3729) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Ut placeat quis est sed qui quia libero et neque.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(4187), 27, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(4196) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Velit minus vitae nostrum tenetur illum magni aspernatur. Perspiciatis vel accusamus voluptatem quidem eligendi cumque quam. Enim in eos officia adipisci accusantium et repellat et. Consectetur officia aliquid quo et officia rerum voluptate natus. Nisi qui voluptate et sunt aut quod enim aut doloremque.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(5802), 39, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(5814) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Sed id laborum ut inventore quae a. Ut et consequuntur adipisci libero numquam iusto est quis. Quas et consequuntur dolorem.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(5982), 30, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(5986) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "veniam", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6010), 40, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6014) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "pariatur", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6036), 35, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6039) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "dolores", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6058), 31, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6061) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Et accusamus eum reprehenderit inventore maxime dignissimos voluptates architecto.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6106), 22, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6109) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Voluptatem velit quidem qui tenetur sed deserunt ducimus.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6201), 36, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6204) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "porro", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6224), 31, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6227) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Provident soluta qui aliquid esse rerum harum sed id odit.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6270), 35, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6272) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "non", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6290), 39, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6293) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Quaerat culpa nobis culpa et maxime.\nQui autem voluptatem cum natus sunt.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6377), 40, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6380) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Maiores incidunt soluta dolor.\nQuo pariatur autem sit ut natus dolorem.\nVoluptatem corporis omnis velit in pariatur aspernatur officia est.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6453), 40, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6456) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Unde ipsum qui. Tempore sed aut impedit delectus. Deleniti veritatis ut minus quos est voluptatem dolorem. Deleniti expedita quaerat consequatur temporibus non est ad. Autem ut assumenda itaque quisquam expedita corporis sed. Sit necessitatibus et dolorem enim suscipit.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6623), 21, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6627) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Consequatur aspernatur rerum nulla maxime numquam ipsam ea nostrum commodi.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6674), 36, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6677) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Sit qui delectus ut laboriosam ipsam. Nostrum nostrum dolores ad repellat et. Et repellendus et amet aut. Provident quos reprehenderit voluptatem est voluptate magnam perferendis voluptas quidem.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6801), 26, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6804) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Voluptatibus id inventore sit alias similique.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6838), 39, new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(6841) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 13, "Eius distinctio veritatis consequatur in qui.\nDolor aliquid at voluptas laboriosam rerum vel.\nVeritatis esse ducimus et voluptas quis fuga non commodi ipsam.\nRerum amet labore aut quisquam saepe cum iste.\nMaxime consectetur facilis voluptatem voluptates possimus est.", new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(7013), new DateTime(2021, 6, 17, 17, 45, 7, 551, DateTimeKind.Local).AddTicks(7017) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 17, 17, 45, 7, 439, DateTimeKind.Local).AddTicks(7117), "Tony46@hotmail.com", "EzvDUZE0gsaLB5yCtvsx+4jPs2BJ6clZHOPudN4S2h0=", "m3oZNpc+fjUyqttSnP7tKyMb/xdlDSdXkC/teMgu89M=", new DateTime(2021, 6, 17, 17, 45, 7, 439, DateTimeKind.Local).AddTicks(7514), "Jakob_Shanahan3" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 17, 17, 45, 7, 444, DateTimeKind.Local).AddTicks(9618), "Albin18@gmail.com", "vBdBUV+laGkAj4wqaCjj3Dkb8W9YupQS41fAq8D8axs=", "4oO/QcUT2XsLJCvx6RYCCGLnFq6Je2JzoSVtjsPSyug=", new DateTime(2021, 6, 17, 17, 45, 7, 444, DateTimeKind.Local).AddTicks(9630), "Jordane_Gulgowski61" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 17, 17, 45, 7, 450, DateTimeKind.Local).AddTicks(1737), "Alexander_Fritsch@yahoo.com", "weLnlZ9rDqHORhjvgj3rTdIlBwxsV+qZf6s3FuhFUOY=", "hRctuLbFR538GLgCocURpR4EyAd9bgWcRXdCiFah9E4=", new DateTime(2021, 6, 17, 17, 45, 7, 450, DateTimeKind.Local).AddTicks(1743), "Daryl_Muller93" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 17, 17, 45, 7, 455, DateTimeKind.Local).AddTicks(2565), "Green.Champlin@hotmail.com", "9lja6zxNPqnN0fFP+hv9F1snIC4uR3AqNM1vkGZubCM=", "GZmIHzUQssBucPo5g1Hk5+SIWDk1/LBgDzXp9MR89yU=", new DateTime(2021, 6, 17, 17, 45, 7, 455, DateTimeKind.Local).AddTicks(2572), "Assunta_Rice22" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 17, 17, 45, 7, 460, DateTimeKind.Local).AddTicks(3921), "Rogelio.Armstrong@yahoo.com", "fXrWgunspkhCa3y6ZzN0mI3lNkkaDGA/cMhiHha34Y0=", "pGFRz5cC+Y2OijzhLVQiClotCddl4Zq6QnVqFuv2t7U=", new DateTime(2021, 6, 17, 17, 45, 7, 460, DateTimeKind.Local).AddTicks(3927), "Florida.Runolfsdottir" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 17, 17, 45, 7, 465, DateTimeKind.Local).AddTicks(5716), "Katrina_Fisher23@hotmail.com", "StgPdItc6dWZJGGc9WLXoXZq3IlviSb9voyd95u9l9o=", "5VIlMiV8D1Hzb6zY5GSHG0VDUbxn0FoA9WFZRl0uOHE=", new DateTime(2021, 6, 17, 17, 45, 7, 465, DateTimeKind.Local).AddTicks(5722), "Abbie_Murazik" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 6, 17, 17, 45, 7, 470, DateTimeKind.Local).AddTicks(6390), "Rolando.Blick93@yahoo.com", "1kHsL507FIj56J0t7T0QaN3faVMZufJKwv9AmjbX7ac=", "X2f0F0SQIO1ngAXEHJnHAbdxvKALJWvg+9UqSV2x0wQ=", new DateTime(2021, 6, 17, 17, 45, 7, 470, DateTimeKind.Local).AddTicks(6396), "Idell17" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 6, 17, 17, 45, 7, 475, DateTimeKind.Local).AddTicks(7870), "Ollie_Schowalter73@yahoo.com", "wS18Mb8oNx3YS5w6Bra/GeqZNhm8Al4nToNSVt79Vnc=", "VnnMKjdggo4ZmG7JE+4hhg6Wv1AGoErTnODmeB5W3hM=", new DateTime(2021, 6, 17, 17, 45, 7, 475, DateTimeKind.Local).AddTicks(7886), "Royal13" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 17, 17, 45, 7, 481, DateTimeKind.Local).AddTicks(1489), "Xander.Murphy@yahoo.com", "U4d5L8wmjttBayeTgaID4FVcgjoC5ZGii4TM6+1ysb0=", "Gkxz+vLYWanJsbz4rhgxopeUQJPWZ2K898REYYZmFiY=", new DateTime(2021, 6, 17, 17, 45, 7, 481, DateTimeKind.Local).AddTicks(1494), "Dan80" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 17, 17, 45, 7, 486, DateTimeKind.Local).AddTicks(2341), "Ned_Stehr96@hotmail.com", "l06MYDOtIelXBgrR57dpRRz9Z6rBC7NbEqNnsgiA7jE=", "1kOolGEFysCrF2I/Wzq8O2oR/myl8w7c4yQ339hHj/U=", new DateTime(2021, 6, 17, 17, 45, 7, 486, DateTimeKind.Local).AddTicks(2348), "Josie_Heller17" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 17, 17, 45, 7, 491, DateTimeKind.Local).AddTicks(4356), "Hilma_Hilll86@hotmail.com", "YSks3HmbpDr766nefhDQi8sfQu/3G9LaoZdJrsSF/wk=", "xpBHLLODBfbcRn0V6fqAjhj+F3C+2BJRTJJchwA9lyQ=", new DateTime(2021, 6, 17, 17, 45, 7, 491, DateTimeKind.Local).AddTicks(4376), "Aliya78" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 496, DateTimeKind.Local).AddTicks(6154), "Bertrand66@hotmail.com", "TEsFmFKwF8eZyUwaXkfWYLh/Mrqldh59jCgQIeye4fg=", "vkoY5n9aGwEgc6Olx5zc9AqkZtH+49ooS82WpkpGbBE=", new DateTime(2021, 6, 17, 17, 45, 7, 496, DateTimeKind.Local).AddTicks(6161), "Amaya82" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 6, 17, 17, 45, 7, 501, DateTimeKind.Local).AddTicks(8633), "Teresa54@yahoo.com", "33q5CsaLEkYoX8Pwg7yrTVmDRbL0TA5cXQXnjFJRAwc=", "ueX7np2NEORasABrb/PvLo3dhP3f7QOQm5C6VuddPb4=", new DateTime(2021, 6, 17, 17, 45, 7, 501, DateTimeKind.Local).AddTicks(8639), "Eryn90" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 17, 17, 45, 7, 507, DateTimeKind.Local).AddTicks(289), "Estevan_Lehner@gmail.com", "S9i7wCeiyJ8uuLJ5HQrPvxyLNgOby4eUhVBb32TJ920=", "WMgiNoJhsghwzNXjszNT9PptfKATxGwAYIIWuXXXPsA=", new DateTime(2021, 6, 17, 17, 45, 7, 507, DateTimeKind.Local).AddTicks(306), "Larry38" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 17, 17, 45, 7, 512, DateTimeKind.Local).AddTicks(1351), "Stevie.Gislason@hotmail.com", "0LwiMgPVe/hCrmwLNvhDvkX18o5q1l9MvfrR091d6aU=", "KvnFyo+lsWt9LVJsXiiS8UO5dwbiBa1zHBPLsAHaqLQ=", new DateTime(2021, 6, 17, 17, 45, 7, 512, DateTimeKind.Local).AddTicks(1361), "Billie.White64" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 17, 17, 45, 7, 517, DateTimeKind.Local).AddTicks(5561), "Libby_Koss49@yahoo.com", "y6aFaZEqN1vbsxTrgqyzgjt8VtJlnYR2NO2kE+NF1ZE=", "YeIx0KlC1+XbKNlW1MzTH7QFTtf7phKlku6nAIQ/J1Y=", new DateTime(2021, 6, 17, 17, 45, 7, 517, DateTimeKind.Local).AddTicks(5569), "Eulalia3" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 17, 17, 45, 7, 522, DateTimeKind.Local).AddTicks(7140), "Damian18@yahoo.com", "d1YNpY5RlQ6zvlJDwJsWGroy6N4e9vROVfVjH82DQYs=", "oGEgKF6YgTYV7Q4+NNKrSsEVkn9AavC8ws/rIh0Kjz8=", new DateTime(2021, 6, 17, 17, 45, 7, 522, DateTimeKind.Local).AddTicks(7147), "Freddie.Gleichner79" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 17, 17, 45, 7, 527, DateTimeKind.Local).AddTicks(8581), "Jaycee_Howell@hotmail.com", "naxfTey9hN8qd7qzWzLKPWjnrKqqEVBsJrSlVCyNgag=", "RY+2kQah0jwpnYqRdirGG104BWGpVWGMkeM6J8sD/qk=", new DateTime(2021, 6, 17, 17, 45, 7, 527, DateTimeKind.Local).AddTicks(8588), "Marlene22" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 6, 17, 17, 45, 7, 532, DateTimeKind.Local).AddTicks(9363), "Mina.Terry42@gmail.com", "JYm0NaTg4a/BBTQBguMrO8FMz6VU1EpDBVDViI69Gbw=", "6VkFXEMLXU1JxQRHpkOGkZYRCATzvVaGqRG34/Jb7z8=", new DateTime(2021, 6, 17, 17, 45, 7, 532, DateTimeKind.Local).AddTicks(9369), "Missouri_Kshlerin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 17, 17, 45, 7, 539, DateTimeKind.Local).AddTicks(3740), "Christophe_Strosin45@yahoo.com", "UE/GndHEwvqqfebRNFoT++LMmB/00T8DLinrgqCGF2Q=", "Io9lviHms5HjmbruFPMAIN+gLtzrcwiXt9w8CcJV0tE=", new DateTime(2021, 6, 17, 17, 45, 7, 539, DateTimeKind.Local).AddTicks(3756), "Preston74" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 17, 45, 7, 544, DateTimeKind.Local).AddTicks(5129), "lthbXAnZjHHAjO4dF+tVsUlgMON6Ecgszra/qFEYY3Q=", "+5xbHHwhZVICAppVTzJWLMiJFQnrNQRMrc4XZVbuav0=", new DateTime(2021, 6, 17, 17, 45, 7, 544, DateTimeKind.Local).AddTicks(5129) });
        }
    }
}
