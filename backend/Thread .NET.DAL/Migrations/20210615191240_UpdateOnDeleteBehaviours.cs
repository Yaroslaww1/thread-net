﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class UpdateOnDeleteBehaviours : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Users_AuthorId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_PostReactions_PostId_UserId",
                table: "PostReactions");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_CommentReactions_CommentId_UserId",
                table: "CommentReactions");

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_PostReactions_PostId_UserId_IsLike",
                table: "PostReactions",
                columns: new[] { "PostId", "UserId", "IsLike" });

            migrationBuilder.AddUniqueConstraint(
                name: "AK_CommentReactions_CommentId_UserId_IsLike",
                table: "CommentReactions",
                columns: new[] { "CommentId", "UserId", "IsLike" });

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 8, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(6484), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(6826), 5 },
                    { 18, 16, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7351), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7353), 9 },
                    { 17, 7, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7336), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7339), 4 },
                    { 15, 18, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7307), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7310), 5 },
                    { 14, 15, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7293), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7296), 9 },
                    { 13, 20, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7279), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7282), 15 },
                    { 12, 15, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7264), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7267), 2 },
                    { 11, 4, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7250), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7252), 17 },
                    { 19, 14, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7365), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7368), 16 },
                    { 10, 17, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7236), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7238), 19 },
                    { 8, 15, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7196), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7199), 12 },
                    { 7, 6, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7181), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7184), 14 },
                    { 6, 10, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7167), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7169), 18 },
                    { 5, 9, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7151), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7154), 7 },
                    { 4, 20, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7135), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7138), 9 },
                    { 3, 18, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7119), true, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7122), 17 },
                    { 2, 4, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7096), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7101), 3 },
                    { 9, 1, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7220), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7223), 19 },
                    { 20, 5, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7380), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7383), 6 },
                    { 16, 7, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7322), false, new DateTime(2021, 6, 15, 22, 12, 40, 132, DateTimeKind.Local).AddTicks(7325), 10 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Neque ut velit molestiae.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(4479), 19, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(4806) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Sint delectus incidunt eveniet ut nihil quas a accusamus rerum.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5138), 9, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5145) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Accusantium corrupti amet iusto distinctio.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5187), 6, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5190) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Tenetur eos ut eveniet a qui qui et non.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5279), 10, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5283) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Sed voluptatum voluptas praesentium consectetur dolorem occaecati.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5320), 10, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5323) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Aspernatur corporis veritatis qui numquam.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5353), 8, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5356) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Voluptatem aut quidem quia natus aut eligendi repellat ad.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5400), 2, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5403) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Itaque et eligendi debitis dolorem unde.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5434), 15, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5437) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Dolores sint assumenda voluptas.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5464), 16, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5466) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Quo incidunt excepturi omnis.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5528), 4, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5531) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Et sint velit repudiandae distinctio voluptas adipisci necessitatibus perspiciatis.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5572), 20, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5575) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Ut laborum sit ut libero.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5604), 13, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5607) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Assumenda praesentium accusamus quibusdam voluptates nisi blanditiis.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5650), 1, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5653) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Ad enim est odit adipisci sapiente.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5684), 13, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5687) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Sapiente ab id.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5714), 6, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5717) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 18, "Veniam non fuga et voluptas.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5781), new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5784) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Et maiores eaque ab et alias voluptatem.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5821), 1, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5823) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Beatae velit est deserunt consequuntur esse modi corrupti aperiam.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5861), 14, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5864) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Neque corrupti in totam et illum.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5893), 11, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5896) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Dolorum dignissimos repellendus quia sed sequi voluptates et.", new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5930), 15, new DateTime(2021, 6, 15, 22, 12, 40, 125, DateTimeKind.Local).AddTicks(5933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 980, DateTimeKind.Local).AddTicks(9573), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/388.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(3633) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4223), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/924.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4228) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4244), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/514.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4247) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4257), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/118.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4260) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4271), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/48.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4273) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4283), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/412.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4286) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4296), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1123.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4299) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4459), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/966.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4461) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4471), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/17.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4474) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4559), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/556.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4562) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4575), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1179.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4577) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4588), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/116.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4590) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4600), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/726.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4603) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4613), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1056.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4615) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4625), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/930.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4628) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4638), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1248.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4640) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4651), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1053.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4653) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4663), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/635.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4801) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4811), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/297.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4813) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4823), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/391.jpg", new DateTime(2021, 6, 15, 22, 12, 39, 981, DateTimeKind.Local).AddTicks(4825) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7180), "https://picsum.photos/640/480/?image=383", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7524) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7699), "https://picsum.photos/640/480/?image=429", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7703) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7717), "https://picsum.photos/640/480/?image=925", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7719) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7729), "https://picsum.photos/640/480/?image=599", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7732) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7742), "https://picsum.photos/640/480/?image=840", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7745) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7755), "https://picsum.photos/640/480/?image=148", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7757) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7767), "https://picsum.photos/640/480/?image=1019", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7770) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7780), "https://picsum.photos/640/480/?image=730", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7783) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7793), "https://picsum.photos/640/480/?image=1045", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7795) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7805), "https://picsum.photos/640/480/?image=705", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7808) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7818), "https://picsum.photos/640/480/?image=795", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7820) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7831), "https://picsum.photos/640/480/?image=341", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7833) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7843), "https://picsum.photos/640/480/?image=494", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7845) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7889), "https://picsum.photos/640/480/?image=296", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7891) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7903), "https://picsum.photos/640/480/?image=682", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7905) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7916), "https://picsum.photos/640/480/?image=121", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7918) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7928), "https://picsum.photos/640/480/?image=146", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7930) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7940), "https://picsum.photos/640/480/?image=822", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7942) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7952), "https://picsum.photos/640/480/?image=286", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7955) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7965), "https://picsum.photos/640/480/?image=575", new DateTime(2021, 6, 15, 22, 12, 39, 984, DateTimeKind.Local).AddTicks(7967) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5336), false, 7, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5338), 10 },
                    { 1, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(4296), false, 16, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(4733), 1 },
                    { 19, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5321), true, 10, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5323), 4 },
                    { 18, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5306), true, 6, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5309), 14 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5293), false, 13, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5295), 7 },
                    { 16, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5278), false, 19, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5281), 11 },
                    { 14, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5247), false, 9, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5250), 20 },
                    { 13, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5188), true, 10, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5190), 13 },
                    { 12, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5171), true, 10, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5173), 1 },
                    { 11, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5157), true, 9, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5159), 15 },
                    { 15, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5263), false, 18, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5266), 2 },
                    { 9, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5127), true, 19, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5129), 16 },
                    { 8, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5112), true, 3, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5114), 12 },
                    { 7, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5098), true, 11, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5100), 8 },
                    { 6, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5083), false, 10, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5085), 12 },
                    { 10, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5142), true, 6, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5145), 1 },
                    { 5, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5067), false, 4, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5070), 19 },
                    { 4, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5052), true, 17, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5055), 18 },
                    { 3, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5034), false, 5, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5037), 9 },
                    { 2, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5012), false, 13, new DateTime(2021, 6, 15, 22, 12, 40, 129, DateTimeKind.Local).AddTicks(5016), 2 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Provident accusamus fuga facilis nihil odio odio quam dolorem. Reprehenderit sed aspernatur. Rerum soluta commodi non tenetur assumenda dolor.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(2567), 25, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(2916) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Excepturi repudiandae rerum explicabo et velit non et.\nDoloribus earum recusandae fugiat iusto sed ut quia ea eum.\nDolor praesentium explicabo.\nLaboriosam ut aut rem est voluptatem non a nobis placeat.\nNon rem minus iusto voluptatibus et exercitationem est eveniet quod.\nExplicabo sint et non necessitatibus culpa.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(4598), 26, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(4611) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Aut possimus veniam iusto id consectetur deserunt inventore voluptatibus. Dicta molestias officia dolores. Molestiae in nesciunt atque pariatur velit incidunt.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(4773), 39, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(4777) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "In molestiae aliquid libero temporibus at officiis impedit et.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5542), 40, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5552) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Sit quo possimus.\nAut consequatur veritatis quo.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5617), 26, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5621) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Sed dolorum quod pariatur doloremque reiciendis. Autem dolorem impedit ipsam praesentium incidunt quidem debitis asperiores et. Vero doloribus eum fugiat ea voluptatum quisquam et qui. Nobis magni eligendi repellendus ab ducimus tempore dicta possimus. Consequuntur error magnam enim.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5809), 38, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5813) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Omnis quis sit quisquam soluta rerum doloremque maxime.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5910), 21, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5914) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Consequuntur temporibus non sequi odit.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5956), 21, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(5959) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Delectus temporibus ut corrupti facilis nostrum id quasi voluptatibus.\nAperiam asperiores totam vitae.\nDelectus ducimus consequatur facere corporis quam ullam.\nQuos alias suscipit eaque explicabo quia illum aut voluptates.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6062), 27, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6066) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Atque adipisci et id aut tenetur.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6134), 28, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6137) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "magni", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6357), 27, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6366) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Non omnis aut consequatur nam sapiente quod.\nNemo aliquam rerum et dolorem consequuntur.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6443), 23, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6447) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Sit atque magnam error sit exercitationem laudantium repudiandae.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6482), 37, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6485) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Sint et tempora ut dolores eius fuga. Et quod quo recusandae deleniti consequatur sunt modi. Odit blanditiis nulla assumenda in expedita laboriosam facilis possimus aut.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6614), 39, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6618) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "deleniti", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6639), 33, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6642) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Itaque tempora doloribus blanditiis pariatur itaque placeat quod ut.\nUt non autem est ut sequi esse dolorem.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6737), 37, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6741) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Aliquam autem esse est doloribus enim quaerat aspernatur rerum vel.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6783), 25, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6786) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Sed alias asperiores fugiat est repudiandae et perferendis.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6822), 25, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6825) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Voluptas adipisci consequuntur nulla aut.\nAb ut laborum ipsa consequuntur rerum.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6874), 30, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6877) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Qui est dicta ea tempora aperiam esse ipsum maiores rem.", new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6973), 30, new DateTime(2021, 6, 15, 22, 12, 40, 121, DateTimeKind.Local).AddTicks(6977) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 15, 22, 12, 40, 9, DateTimeKind.Local).AddTicks(204), "Domenick38@hotmail.com", "aLbBpwfOh1oA5I13T9H5eMvdopmpq2iLK1iq7He7EwY=", "jSfaPr7S1iDBhWtcinjyWXG50uAUw5GOGSiEo1nQ6Fk=", new DateTime(2021, 6, 15, 22, 12, 40, 9, DateTimeKind.Local).AddTicks(572), "Justyn.Pagac" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 22, 12, 40, 14, DateTimeKind.Local).AddTicks(5358), "Loraine24@gmail.com", "J2N+sAZ2gboIe8fH/iPVUdu3Ru4yL95IJ4LDRUilET0=", "6sSOEgHwhLT8vfzShP2ZYVvDVLXGZd9L9K2uZ9/yjkw=", new DateTime(2021, 6, 15, 22, 12, 40, 14, DateTimeKind.Local).AddTicks(5381), "Merritt.Murphy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 15, 22, 12, 40, 19, DateTimeKind.Local).AddTicks(6875), "Barton63@hotmail.com", "IWFChfqF+D8JSgCgcstLTct1oohqPhS16ZygAZ15IhU=", "Ja4CRdZdjJjddZPtPvxhGmmf2MEn0ojGYa/fNO3ST0M=", new DateTime(2021, 6, 15, 22, 12, 40, 19, DateTimeKind.Local).AddTicks(6884), "Hyman30" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 15, 22, 12, 40, 24, DateTimeKind.Local).AddTicks(8105), "Jarred.Schoen74@yahoo.com", "T0gtX9NeazJaMOnC3zqAK4Cqfd278N/zDjdD5qjZgdg=", "OLkLEK0JNQezTReZGgFppgt7sEVfp+QYEQ6GB4AMPLw=", new DateTime(2021, 6, 15, 22, 12, 40, 24, DateTimeKind.Local).AddTicks(8112), "Gwendolyn_McGlynn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 6, 15, 22, 12, 40, 30, DateTimeKind.Local).AddTicks(2439), "Isadore_Boyer@yahoo.com", "qIRPBDjWrFHIU8k1tOcz+h0L0CVGNYK0h/ydsXKwRUI=", "Erv1x7D92vrWnHyYpRyRP3u810ghpkbHXIr386WOceI=", new DateTime(2021, 6, 15, 22, 12, 40, 30, DateTimeKind.Local).AddTicks(2446), "Tianna57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 15, 22, 12, 40, 35, DateTimeKind.Local).AddTicks(3600), "Winona_Bechtelar18@yahoo.com", "G6c8zXHemXSTuRIrwOeXGUEV4AfEoYYQfxsrYHE/66M=", "8Sm1w0zoHlHOgWi5z6iWcPyglXb9/kSXdwEBTrxe89Y=", new DateTime(2021, 6, 15, 22, 12, 40, 35, DateTimeKind.Local).AddTicks(3606), "Raymond.Nader18" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 6, 15, 22, 12, 40, 40, DateTimeKind.Local).AddTicks(4408), "Carli47@yahoo.com", "vpp+uIvmtapKHbymdneOOBqjagSX4xVsL1ieN3HBiHs=", "1c/dTkvTfyZ55/xnWAD2CjsL5va6VuvsXSRDVVHq3cw=", new DateTime(2021, 6, 15, 22, 12, 40, 40, DateTimeKind.Local).AddTicks(4415), "Sofia.Veum" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 15, 22, 12, 40, 45, DateTimeKind.Local).AddTicks(7330), "Missouri_Gorczany@gmail.com", "Vkbp887bu6FqJL9hBK1kJPNCyYYNX8ET+UjeIogDaGg=", "5TyfkrInrE+yvIoM+tWC80n8i2gPg0XhWmLmsbXxSSg=", new DateTime(2021, 6, 15, 22, 12, 40, 45, DateTimeKind.Local).AddTicks(7337), "Sigmund_Kuphal" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 15, 22, 12, 40, 50, DateTimeKind.Local).AddTicks(9379), "Jules_Romaguera73@yahoo.com", "NZ4HyB/HwmDHWA7DxJv2FKufMyyMOpktXBEviMaY0KQ=", "3HcOV+I2dbmV/5MtJriOqjUx2RJdPZr47ctMoBmObes=", new DateTime(2021, 6, 15, 22, 12, 40, 50, DateTimeKind.Local).AddTicks(9385), "Maggie_MacGyver63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 15, 22, 12, 40, 56, DateTimeKind.Local).AddTicks(237), "Lew.Bergstrom36@hotmail.com", "Dq70kKbLkCgRgR/PndDWdRTwUs1wYSq9CM3mTDaskcg=", "EMeuq3DOcd+pe9sJQ45/O0FJTosbA3RfvrcnbYyUy2A=", new DateTime(2021, 6, 15, 22, 12, 40, 56, DateTimeKind.Local).AddTicks(244), "Norene13" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 15, 22, 12, 40, 61, DateTimeKind.Local).AddTicks(2684), "Ernesto_Brekke71@yahoo.com", "0SlafbM9ZTfUlpq9RkFqeZU2nNqubDwo76l7vaRyfiM=", "zmwPIm1FAfykL8Miavmqy3Ey11dHGiXLmQymn9jtfSU=", new DateTime(2021, 6, 15, 22, 12, 40, 61, DateTimeKind.Local).AddTicks(2691), "Jackson24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 15, 22, 12, 40, 66, DateTimeKind.Local).AddTicks(4471), "Joannie_Schimmel67@yahoo.com", "wHRPQjQCRZ5Y/S/NdIVyWeq+Nq9/2AFhLHmXAKqxo00=", "WRfS1q8+oxdafYG9Z+RZ5X/31fRwnIqIDWzaHk6Q1Mk=", new DateTime(2021, 6, 15, 22, 12, 40, 66, DateTimeKind.Local).AddTicks(4478), "Wilmer_Robel" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 15, 22, 12, 40, 71, DateTimeKind.Local).AddTicks(5387), "Luis84@hotmail.com", "3Szv2S6Q9Q2Z2L4gLlMKexZZOIIt9dmcrPiXyO5k2hQ=", "20B4TlxfDdns3x3wgL23dyvSVwsK4yJ3nIUNXwxu7G4=", new DateTime(2021, 6, 15, 22, 12, 40, 71, DateTimeKind.Local).AddTicks(5394), "Pinkie0" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 15, 22, 12, 40, 77, DateTimeKind.Local).AddTicks(1498), "Laron_Prosacco64@hotmail.com", "2U06ikbMSAPhDJ5k8FqvEc/3YffamH79aHfIs+Pnr1s=", "RlOJkNn4j3pMoKmnGHS7efSjczY5u7w8DdC4KK2SdR8=", new DateTime(2021, 6, 15, 22, 12, 40, 77, DateTimeKind.Local).AddTicks(1505), "Rachelle.Treutel63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 15, 22, 12, 40, 82, DateTimeKind.Local).AddTicks(2297), "Andres.Carroll59@yahoo.com", "NCf4lFghe5inWkor4Is0R2nnIIYzPkX7axPJh/9eo8o=", "iO8qTz/LvdpjIemzI/n33K9RfNz8rS+CgZpWthsn3MA=", new DateTime(2021, 6, 15, 22, 12, 40, 82, DateTimeKind.Local).AddTicks(2303), "Alexys93" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 40, 87, DateTimeKind.Local).AddTicks(3468), "Destiny65@hotmail.com", "fiio+oosEc6GztLZd7Gh199td5LmYNpSVRq87sTA/to=", "gelED9dLRvUYn2Xq0s/4jdFXgfzmrn80uZLPQCT22mg=", new DateTime(2021, 6, 15, 22, 12, 40, 87, DateTimeKind.Local).AddTicks(3475), "Donna47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 40, 92, DateTimeKind.Local).AddTicks(7918), "Gabriel.Welch@gmail.com", "cWfGPSavA8lT2JjaYTcNHcKRBqrVtAR/g/AasyVyocQ=", "aBSyFK28YQSlPqIO1W3JsFXNgqcz7JcoH4GkX31cKX8=", new DateTime(2021, 6, 15, 22, 12, 40, 92, DateTimeKind.Local).AddTicks(7926), "Emile.Baumbach68" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 15, 22, 12, 40, 97, DateTimeKind.Local).AddTicks(9150), "Austyn.Nienow@yahoo.com", "CPN3X+Mya0Yx08W5ql+Ud5jsEK24QosoSGqT5wMFpr4=", "6W20/Yzt50npfS6BYlVGAxEXya7MyIiv8L0TCddoCY8=", new DateTime(2021, 6, 15, 22, 12, 40, 97, DateTimeKind.Local).AddTicks(9156), "Frederique94" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 15, 22, 12, 40, 103, DateTimeKind.Local).AddTicks(192), "Wiley1@hotmail.com", "U+ld2O34IaYn7gU/Sekwxh3D1SAYZgpHjPS4tMdjRbk=", "3zpqdjaLnk03fFh1LY8n/fdXsaGpAqQ+J3pEmq8M0zM=", new DateTime(2021, 6, 15, 22, 12, 40, 103, DateTimeKind.Local).AddTicks(198), "Keshawn.Herman7" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 22, 12, 40, 108, DateTimeKind.Local).AddTicks(956), "Ettie.Kling@hotmail.com", "TfWoJwJ5RrwYFUhz47ZJZ+nFp0QYg+0CYQulqcsnL0E=", "gIwvNJj+FYlmB4JT4mg1/S1ksTsa2RlqNGjous7j1jI=", new DateTime(2021, 6, 15, 22, 12, 40, 108, DateTimeKind.Local).AddTicks(962), "Kayden_Kunde79" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 12, 40, 113, DateTimeKind.Local).AddTicks(5303), "RRVk7tjt0srr1+O3t4pILWt66XlQiQm1cyJxYIu5QdI=", "eKlX0cIXyvBt3UvMyacnI+nNhLi+p2PfqHhKYAVK4TY=", new DateTime(2021, 6, 15, 22, 12, 40, 113, DateTimeKind.Local).AddTicks(5303) });

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Users_AuthorId",
                table: "Comments",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Users_AuthorId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_PostReactions_PostId_UserId_IsLike",
                table: "PostReactions");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_CommentReactions_CommentId_UserId_IsLike",
                table: "CommentReactions");

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_PostReactions_PostId_UserId",
                table: "PostReactions",
                columns: new[] { "PostId", "UserId" });

            migrationBuilder.AddUniqueConstraint(
                name: "AK_CommentReactions_CommentId_UserId",
                table: "CommentReactions",
                columns: new[] { "CommentId", "UserId" });

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 4, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(3131), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4182), 8 },
                    { 18, 15, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5568), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5573), 3 },
                    { 17, 20, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5536), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5541), 16 },
                    { 15, 13, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5470), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5476), 14 },
                    { 14, 6, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5438), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5443), 10 },
                    { 13, 18, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5405), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5411), 3 },
                    { 12, 2, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5372), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5378), 1 },
                    { 11, 12, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5340), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5345), 13 },
                    { 19, 16, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5599), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5605), 21 },
                    { 10, 16, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5307), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5313), 1 },
                    { 8, 14, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5242), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5247), 13 },
                    { 7, 6, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5209), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5215), 17 },
                    { 6, 4, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5176), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5182), 7 },
                    { 5, 19, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5131), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5139), 21 },
                    { 4, 13, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4985), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4996), 12 },
                    { 3, 6, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4924), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4934), 6 },
                    { 2, 11, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4838), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4856), 1 },
                    { 9, 13, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5274), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5280), 20 },
                    { 20, 2, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5630), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5635), 17 },
                    { 16, 3, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5502), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5508), 21 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Quo sunt necessitatibus impedit nobis repellendus voluptatem.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(5039), 6, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(6199) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Mollitia et exercitationem aliquam animi iusto est cupiditate.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(6984), 13, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(6997) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Porro nobis error.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7091), 14, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7098) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "A aliquid quia distinctio enim dolorem.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7193), 3, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7200) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Sit ut harum minus atque quidem molestias sint consectetur.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7306), 5, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7314) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Sapiente inventore nostrum.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7383), 5, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7390) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Blanditiis sed ea quia.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7628), 20, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7639) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Et dignissimos ut eaque voluptas labore.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7731), 3, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7739) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Aliquam aut saepe ipsum consequuntur totam quia ipsum autem.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7842), 10, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7850) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Asperiores nostrum doloremque animi et.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7989), 16, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7996) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Esse vero officia.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8068), 7, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8076) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Adipisci ut voluptatum eos accusamus dolor.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8164), 4, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8172) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Sit dolorem aperiam alias maiores voluptates ut hic enim.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8356), 3, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8366) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Voluptatem itaque iusto.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8438), 11, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8445) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Consequuntur ea qui cupiditate quaerat.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8528), 15, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8536) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 7, "Libero eligendi excepturi est veniam.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8622), new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8629) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Pariatur et aperiam eligendi et explicabo.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8713), 19, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8720) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Aut sed nisi ipsa animi.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8855), 13, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8863) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Repudiandae pariatur eum voluptate expedita.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8949), 4, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8957) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Quo et ut odio ut.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(9135), 17, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(9144) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(1845), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1081.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(7597) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8310), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/462.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8321) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8352), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1199.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8357) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8377), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/36.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8382) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8401), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/488.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8406) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8497), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/210.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8504) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8526), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1032.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8531) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8551), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/4.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8556) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8574), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1002.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8579) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8598), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/298.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8603) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8621), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/60.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8626) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8644), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/996.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8650) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8669), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1043.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8674) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8693), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/813.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8698) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8716), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/965.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8721) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8740), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1187.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8745) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8763), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/443.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8768) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8787), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/857.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8792) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8810), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/885.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8815) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8833), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/898.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8838) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(2515), "https://picsum.photos/640/480/?image=34", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(3678) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(3959), "https://picsum.photos/640/480/?image=210", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(3967) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4108), "https://picsum.photos/640/480/?image=938", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4115) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4141), "https://picsum.photos/640/480/?image=582", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4147) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4167), "https://picsum.photos/640/480/?image=200", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4172) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4192), "https://picsum.photos/640/480/?image=556", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4197) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4217), "https://picsum.photos/640/480/?image=895", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4223) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4243), "https://picsum.photos/640/480/?image=501", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4248) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4268), "https://picsum.photos/640/480/?image=779", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4273) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4292), "https://picsum.photos/640/480/?image=971", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4297) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4316), "https://picsum.photos/640/480/?image=677", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4321) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4340), "https://picsum.photos/640/480/?image=987", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4345) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4365), "https://picsum.photos/640/480/?image=842", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4371) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4390), "https://picsum.photos/640/480/?image=127", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4395) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4415), "https://picsum.photos/640/480/?image=312", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4480), "https://picsum.photos/640/480/?image=122", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4485) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4509), "https://picsum.photos/640/480/?image=416", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4514) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4534), "https://picsum.photos/640/480/?image=127", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4539) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4558), "https://picsum.photos/640/480/?image=49", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4563) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4582), "https://picsum.photos/640/480/?image=579", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4587) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9811), false, 8, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9817), 11 },
                    { 1, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(7499), true, 2, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(8513), 14 },
                    { 19, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9779), true, 12, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9784), 7 },
                    { 18, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9744), false, 18, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9750), 2 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9713), true, 18, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9718), 4 },
                    { 16, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9681), false, 19, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9686), 12 },
                    { 14, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9616), true, 17, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9621), 10 },
                    { 13, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9582), false, 8, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9588), 1 },
                    { 12, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9551), true, 8, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9556), 2 },
                    { 11, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9520), true, 17, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9525), 16 },
                    { 15, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9649), false, 9, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9654), 20 },
                    { 9, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9452), false, 8, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9457), 18 },
                    { 8, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9417), false, 4, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9423), 2 },
                    { 7, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9383), true, 10, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9389), 8 },
                    { 6, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9350), true, 19, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9355), 14 },
                    { 10, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9487), true, 13, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9492), 18 },
                    { 5, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9315), false, 5, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9321), 8 },
                    { 4, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9281), true, 4, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9287), 6 },
                    { 3, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9248), true, 4, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9254), 1 },
                    { 2, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9192), true, 17, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9203), 5 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Ipsa laborum accusantium saepe. Sed numquam sit inventore beatae cumque voluptas et dignissimos voluptates. Earum expedita numquam harum quo voluptatem velit.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(1719), 40, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(2872) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "mollitia", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(4623), 30, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(4658) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "voluptatum", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(4872), 31, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(4882) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Ut eaque aut sunt occaecati autem. Rerum qui neque in in. Quia ullam fugiat optio unde ex dolor velit quasi unde. Id id ea reiciendis id sit blanditiis assumenda quisquam. Doloribus et est quia fugit ipsa.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(5624), 38, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(5651) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Adipisci provident cumque repellendus facilis qui quia quos. Ipsa rem corrupti ut corrupti repellendus et qui eaque sapiente. Et soluta dolores magni qui sit in iusto.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(5977), 27, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6025) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Inventore voluptate quia distinctio consectetur. Commodi dolores minima omnis velit occaecati esse esse quidem voluptas. Quos occaecati at deleniti dicta sint aperiam. Reprehenderit laudantium et rerum dolor ab amet.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6354), 25, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6364) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "beatae", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6426), 32, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6432) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "vitae", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6487), 32, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6493) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Dicta est reiciendis voluptas earum odit cumque in suscipit in.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(7857), 38, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(7883) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Et dolores sed. Aspernatur est modi nemo. Labore natus placeat quo quis.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8160), 29, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8234) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Hic a velit optio corporis dolor voluptatum. At illum labore quia sit voluptate soluta ea. Voluptatum voluptatem aliquam dolor eos corporis soluta. Eum ex et aut eveniet qui ut. Veniam modi magni iure.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8574), 36, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8583) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Quasi quis quos illo aperiam autem nisi.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8727), 24, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8735) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Atque et dicta et minus ipsum et. Laborum ut tenetur tempore ut aut. Sit unde perferendis temporibus distinctio at quidem aut quos vel. Quos consequatur aut impedit tempora ab ea vel. At dolores deleniti expedita et saepe ipsa consequatur provident. Illum repellendus veritatis rem neque est.", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(1049), 38, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(1076) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "quod", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(1193), 37, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(1200) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Soluta repudiandae velit.\nIpsam ut enim necessitatibus eius ab quae quasi ex neque.\nConsequatur et in architecto repellat voluptas molestiae provident pariatur.\nEx doloribus et.\nEt non ut quidem iste hic reiciendis et.\nNumquam excepturi quas.", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(2954), 38, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(2984) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Sed sint eveniet.", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3100), 28, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3108) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "maxime", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3161), 31, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3168) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Vel error aut sapiente.", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3249), 39, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3256) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "ipsam", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3302), 31, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3309) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Numquam temporibus dolor non commodi error quos earum officia.", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3511), 23, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3520) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 5, 20, 11, 13, 14, 826, DateTimeKind.Local).AddTicks(488), "Murray_Schuster79@gmail.com", "acAJ53wZsq31VAb6cMXgUfIax6/m7Y0/RHnoJEnMYEg=", "Rcm3LnHPlpiN5J9LLJYTd0PcilyDDnVc6Nswoyo47j0=", new DateTime(2021, 5, 20, 11, 13, 14, 826, DateTimeKind.Local).AddTicks(1583), "Titus_Kuvalis" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 5, 20, 11, 13, 14, 837, DateTimeKind.Local).AddTicks(138), "Dave79@yahoo.com", "d32nF9qsABrvxMVc32IrJVyR99sYzoFWb8KMrKJtcBo=", "tKnkVn1ZFt/SZzKzklyEc3+JSjAszT7v5RMwkfSSA6I=", new DateTime(2021, 5, 20, 11, 13, 14, 837, DateTimeKind.Local).AddTicks(212), "Al_Raynor" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 5, 20, 11, 13, 14, 846, DateTimeKind.Local).AddTicks(7785), "Kendrick_Emmerich13@gmail.com", "hwaghCHm2/1iUok1aCOO6BLTATiQihj5O2AH/ftjbRE=", "+uJsbanlGIkilFjT1yEIO2jLY+GJdMK/HVrxnuwP6i8=", new DateTime(2021, 5, 20, 11, 13, 14, 846, DateTimeKind.Local).AddTicks(7817), "Lenore16" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 5, 20, 11, 13, 14, 856, DateTimeKind.Local).AddTicks(5118), "Lelia_Wintheiser@gmail.com", "NT0Ru66Nr2SQMOYwTg7h8HFDkbhZSGnjrOhfKMCK/VY=", "bRn9pW/8YH842PoDeaPqCJs4ZzFupb6KLJ5cFgWvvXI=", new DateTime(2021, 5, 20, 11, 13, 14, 856, DateTimeKind.Local).AddTicks(5182), "Lionel74" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 5, 20, 11, 13, 14, 866, DateTimeKind.Local).AddTicks(1579), "Shanna.Wolf62@yahoo.com", "xB+JebeSFso9LfB1R0QWq2UD0oOg7UGYMRPoyZ6i77Y=", "mXrPmUfK+EPvjzmLLdmoZE3H6a37OmSXuCK1tH6cFso=", new DateTime(2021, 5, 20, 11, 13, 14, 866, DateTimeKind.Local).AddTicks(1657), "Natasha_Watsica1" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 5, 20, 11, 13, 14, 875, DateTimeKind.Local).AddTicks(6521), "Emil_Moen50@gmail.com", "Wgxx1H7JzjFDpm110weZvqoahZTXgr7Qe3b5WgxraQk=", "gohkEBKxAZWAwYeggEMnkbA6CM7XnSMXVBfwN82C910=", new DateTime(2021, 5, 20, 11, 13, 14, 875, DateTimeKind.Local).AddTicks(6589), "Jennifer45" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 5, 20, 11, 13, 14, 885, DateTimeKind.Local).AddTicks(1450), "Kaleb27@hotmail.com", "J1uf9/PUNeKVrn7jF/Yq7m+0EwZADBlvR+cV+rtcxZQ=", "ySLHbl4+0zW3baq+7fkLbdh9zsJFD7CH2FXuO9SBx58=", new DateTime(2021, 5, 20, 11, 13, 14, 885, DateTimeKind.Local).AddTicks(1515), "Hunter.Kemmer" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 5, 20, 11, 13, 14, 894, DateTimeKind.Local).AddTicks(4593), "Brycen_Bayer@gmail.com", "aaOiiDbTIa2LHpb3qRNhL89CnRUeVJxi+tSdrX6b4OI=", "I4Gidaxl1CccNkwj4MlceIvNOPqMd17wS2LXcrlqh04=", new DateTime(2021, 5, 20, 11, 13, 14, 894, DateTimeKind.Local).AddTicks(4653), "Chelsie24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 5, 20, 11, 13, 14, 904, DateTimeKind.Local).AddTicks(2432), "Giuseppe.Schowalter35@gmail.com", "hZ+2DYV5XXAIOVHgnWBDbfpcg8AQ4zM+9wIJLyAb918=", "hcAUmWfOD0aISKN9HfNk26g/aFy0FNDtKhd/p4b3dO0=", new DateTime(2021, 5, 20, 11, 13, 14, 904, DateTimeKind.Local).AddTicks(2515), "Gino.White66" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 5, 20, 11, 13, 14, 913, DateTimeKind.Local).AddTicks(1972), "Madge.McKenzie27@hotmail.com", "wkwbVcA0aF/VIzL/IDIjM7Azl1hRLCpn60+08IGE6BU=", "0FdI7ApNn6iiqH+JugvWa51yvP0xQNkO1rRxsvQ2Dlo=", new DateTime(2021, 5, 20, 11, 13, 14, 913, DateTimeKind.Local).AddTicks(2019), "Warren_Bernier" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 5, 20, 11, 13, 14, 922, DateTimeKind.Local).AddTicks(3523), "Sandra58@hotmail.com", "k8R4dwnKEzCNF/gUJhFLkEv4xK5nI1GjvYZySMBXGfU=", "usYqevKzZb+qARv2wZCKcPL7vjZGIQf77wZaG9DWDk4=", new DateTime(2021, 5, 20, 11, 13, 14, 922, DateTimeKind.Local).AddTicks(3584), "Angelina.Krajcik76" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 5, 20, 11, 13, 14, 931, DateTimeKind.Local).AddTicks(4810), "Etha.Kuhic@hotmail.com", "tUkmuUC33oqMFIku6hXOpevaYAFHpD2+MLu02CIEfYg=", "xM96YjEgTurGXzj8FfvB3DIj8YsMlioDF0vJrWGj63c=", new DateTime(2021, 5, 20, 11, 13, 14, 931, DateTimeKind.Local).AddTicks(4874), "Antonio.Kub" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 5, 20, 11, 13, 14, 940, DateTimeKind.Local).AddTicks(5517), "Reymundo.Hyatt9@hotmail.com", "0t+8Uu4780g97Q47WrMGjg4ZC2NyaZyjhrVAueDckpw=", "k+nKIvhRcAZoCTlvp4jeLaxOc6Mm7mQvpAZpwIXGiZA=", new DateTime(2021, 5, 20, 11, 13, 14, 940, DateTimeKind.Local).AddTicks(5579), "Buck_Reynolds58" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 5, 20, 11, 13, 14, 949, DateTimeKind.Local).AddTicks(9808), "Ewald_Brown15@gmail.com", "nWDeaY8L/6N+Fe0PIwDMFcbtD8CymNncpqw8sms9i7s=", "ubu59TXbBb0pWuax4nkHvFdu1BLWz6lpSf27whkn0Zw=", new DateTime(2021, 5, 20, 11, 13, 14, 949, DateTimeKind.Local).AddTicks(9852), "Nichole.Kulas" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 5, 20, 11, 13, 14, 960, DateTimeKind.Local).AddTicks(4225), "Webster.Armstrong@yahoo.com", "LbmH1t3nBO0bGpMlURNDdHCbhfm4YFhBp6i8jvYW89g=", "8aLXS7QORAG4uDB+wL5UxChZxhszib/in2oWivPE3oY=", new DateTime(2021, 5, 20, 11, 13, 14, 960, DateTimeKind.Local).AddTicks(4300), "Sigurd_Koch" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 970, DateTimeKind.Local).AddTicks(4430), "Chester.Gibson@yahoo.com", "VxSMljQua5wzDvMTEaHBXvqXg52XaKLJZ9FwWeAr64A=", "99Xcb7TKeygJf4WqTJHCz40J7et0ShfsSKdBaosYcIU=", new DateTime(2021, 5, 20, 11, 13, 14, 970, DateTimeKind.Local).AddTicks(4500), "Zetta_Corwin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 979, DateTimeKind.Local).AddTicks(94), "Cory22@hotmail.com", "cC/WtQdyOIXKFdNESGGD3+4le0trUPJBOjDBYWkwGSs=", "vIQ5cLovukqZYHZ7CGBut3nvr4rD4pqucGuIplghdSc=", new DateTime(2021, 5, 20, 11, 13, 14, 979, DateTimeKind.Local).AddTicks(117), "Harrison_Funk" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 5, 20, 11, 13, 14, 987, DateTimeKind.Local).AddTicks(9474), "Maximus.Schowalter@hotmail.com", "07nUGFmM3kNcnN5BVOql7SjpvK++FzyfLSDhRBW5uhc=", "AlXbT32FGJzaFlNqe7BeNZHBHbEVro2VwUaHkpWOo/k=", new DateTime(2021, 5, 20, 11, 13, 14, 987, DateTimeKind.Local).AddTicks(9547), "Magnolia_Klocko" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 5, 20, 11, 13, 14, 998, DateTimeKind.Local).AddTicks(5070), "Kathryn_Abshire82@yahoo.com", "YPW+idxIGypCR3HAHfvBYxoTJIrKQqywAgAdOTCkUDQ=", "aLSTRGtEVgAXEB7SxTJVGuz2WAyBd8+WJ2DHMPCQAaQ=", new DateTime(2021, 5, 20, 11, 13, 14, 998, DateTimeKind.Local).AddTicks(5100), "Amina.Skiles4" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 5, 20, 11, 13, 15, 9, DateTimeKind.Local).AddTicks(4440), "Javon.Hane5@yahoo.com", "qjQeVb5gyvGZR7JCCa3WcNpCJbbZC1msMQAAm8xzC88=", "sOIYRCoaEv1lNThIyXb7kFDIyRELtsXhnm+bAfZ5fj0=", new DateTime(2021, 5, 20, 11, 13, 15, 9, DateTimeKind.Local).AddTicks(4506), "Precious44" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 15, 20, DateTimeKind.Local).AddTicks(3327), "UGZorU7ixmt7ZHZpwbpmfxeeDK1RdaxwQ8tmD7ojIGU=", "lAdg0m8nOXEvf6zmjZ8bbXKHhE8QfSpTUcV9L09Z+84=", new DateTime(2021, 5, 20, 11, 13, 15, 20, DateTimeKind.Local).AddTicks(3327) });

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Users_AuthorId",
                table: "Comments",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
