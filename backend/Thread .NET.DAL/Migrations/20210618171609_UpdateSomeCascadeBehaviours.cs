﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class UpdateSomeCascadeBehaviours : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CommentReactions_Comments_CommentId",
                table: "CommentReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_CommentReactions_Users_UserId",
                table: "CommentReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Posts_PostId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_PostReactions_Posts_PostId",
                table: "PostReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_PostReactions_Users_UserId",
                table: "PostReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts");

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 14, 12, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4333), true, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4336), 13 },
                    { 18, 3, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4389), false, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4391), 14 },
                    { 17, 10, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4375), true, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4378), 8 },
                    { 16, 6, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4362), true, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4364), 5 },
                    { 15, 6, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4348), false, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4350), 12 },
                    { 13, 10, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4320), false, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4322), 17 },
                    { 12, 17, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4305), false, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4308), 2 },
                    { 11, 8, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4291), false, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4294), 9 },
                    { 10, 6, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4277), true, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4280), 8 },
                    { 20, 13, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4416), true, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4419), 8 },
                    { 9, 7, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4263), false, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4265), 21 },
                    { 7, 12, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4234), false, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4236), 8 },
                    { 6, 10, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4219), true, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4222), 1 },
                    { 5, 14, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4204), true, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4207), 6 },
                    { 4, 10, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4189), false, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4191), 13 },
                    { 3, 4, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4173), true, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4175), 8 },
                    { 2, 3, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4149), true, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4154), 13 },
                    { 1, 7, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(3561), false, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(3898), 18 },
                    { 8, 18, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4249), false, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4251), 19 },
                    { 19, 19, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4403), true, new DateTime(2021, 6, 18, 20, 16, 9, 347, DateTimeKind.Local).AddTicks(4405), 19 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Doloribus voluptates quae aut corporis rerum numquam sapiente tenetur.", new DateTime(2021, 6, 18, 20, 16, 9, 340, DateTimeKind.Local).AddTicks(9308), 10, new DateTime(2021, 6, 18, 20, 16, 9, 340, DateTimeKind.Local).AddTicks(9630) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Voluptatem similique sunt quisquam nesciunt.", new DateTime(2021, 6, 18, 20, 16, 9, 340, DateTimeKind.Local).AddTicks(9933), 4, new DateTime(2021, 6, 18, 20, 16, 9, 340, DateTimeKind.Local).AddTicks(9938) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Quibusdam quis qui inventore eveniet sint perspiciatis rerum fugiat ut.", new DateTime(2021, 6, 18, 20, 16, 9, 340, DateTimeKind.Local).AddTicks(9989), 15, new DateTime(2021, 6, 18, 20, 16, 9, 340, DateTimeKind.Local).AddTicks(9993) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Quo odio sed nihil rem nisi est quos sapiente aliquid.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(34), 5, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(37) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Quaerat quasi corrupti et error.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(132), 13, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(135) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Aut dolorum voluptas doloribus maiores temporibus aspernatur voluptas.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(174), 15, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(177) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Qui quam odio magni eaque.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(206), 14, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(209) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Excepturi molestias fugiat sit quam fugiat unde quaerat.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(242), 8, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(245) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Tempore harum consequuntur ipsum nihil qui.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(274), 15, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(277) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Iste omnis neque fugiat harum eum.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(307), 19, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(310) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Earum ducimus esse officiis sed sunt numquam dolor animi.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(427), 4, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(430) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Provident fuga non minus quae cumque.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(464), 5, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(466) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Nemo eum corporis sed sed sapiente ut alias.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(500), 12, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(503) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Et odio provident hic maiores.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(530), 11, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(533) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "A non eius.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(559), 12, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(562) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Voluptatem molestiae incidunt.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(585), 12, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(588) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Necessitatibus soluta vitae harum.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(645), 3, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(649) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Alias magni et quia numquam enim vel qui.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(683), 2, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(686) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Maxime dolore amet similique nostrum quia sunt cupiditate maxime voluptatem.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(733), 1, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(736) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Ut et totam.", new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(761), 19, new DateTime(2021, 6, 18, 20, 16, 9, 341, DateTimeKind.Local).AddTicks(764) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 196, DateTimeKind.Local).AddTicks(7229), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/408.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 196, DateTimeKind.Local).AddTicks(9629) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 196, DateTimeKind.Local).AddTicks(9943), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/419.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 196, DateTimeKind.Local).AddTicks(9947) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 196, DateTimeKind.Local).AddTicks(9964), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/443.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 196, DateTimeKind.Local).AddTicks(9967) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 196, DateTimeKind.Local).AddTicks(9978), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1077.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 196, DateTimeKind.Local).AddTicks(9981) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 196, DateTimeKind.Local).AddTicks(9991), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/978.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 196, DateTimeKind.Local).AddTicks(9993) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(4), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/11.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(6) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(16), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1112.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(19) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(29), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/745.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(31) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(41), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/760.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(44) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(53), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/580.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(56) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(66), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/608.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(68) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(78), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1066.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(81) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(90), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/646.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(93) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(102), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/234.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(105) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(114), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/852.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(117) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(126), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1105.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(129) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(138), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/993.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(141) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(150), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/252.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(153) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(205), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/250.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(208) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(219), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/553.jpg", new DateTime(2021, 6, 18, 20, 16, 9, 197, DateTimeKind.Local).AddTicks(221) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(2514), "https://picsum.photos/640/480/?image=791", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(2867) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(2995), "https://picsum.photos/640/480/?image=376", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(2998) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3012), "https://picsum.photos/640/480/?image=276", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3015) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3026), "https://picsum.photos/640/480/?image=974", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3028) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3038), "https://picsum.photos/640/480/?image=903", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3041) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3051), "https://picsum.photos/640/480/?image=401", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3053) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3063), "https://picsum.photos/640/480/?image=870", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3065) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3075), "https://picsum.photos/640/480/?image=897", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3077) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3087), "https://picsum.photos/640/480/?image=1033", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3090) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3100), "https://picsum.photos/640/480/?image=403", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3102) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3152), "https://picsum.photos/640/480/?image=631", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3155) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3166), "https://picsum.photos/640/480/?image=309", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3168) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3178), "https://picsum.photos/640/480/?image=819", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3181) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3191), "https://picsum.photos/640/480/?image=36", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3194) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3204), "https://picsum.photos/640/480/?image=510", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3206) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3216), "https://picsum.photos/640/480/?image=743", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3219) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3229), "https://picsum.photos/640/480/?image=496", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3231) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3241), "https://picsum.photos/640/480/?image=68", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3243) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3253), "https://picsum.photos/640/480/?image=182", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3256) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3265), "https://picsum.photos/640/480/?image=743", new DateTime(2021, 6, 18, 20, 16, 9, 200, DateTimeKind.Local).AddTicks(3268) });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5902), new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5905) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5930), true, 13, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5933), 11 },
                    { 1, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5022), true, 7, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5350), 19 },
                    { 17, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5888), true, 11, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5891), 19 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 16, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5875), false, 4, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5877), 20 },
                    { 14, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5846), false, 4, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5849), 14 },
                    { 13, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5832), false, 3, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5834), 19 },
                    { 12, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5817), false, 4, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5820), 16 },
                    { 11, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5803), true, 8, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5806), 18 },
                    { 10, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5789), true, 6, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5791), 16 },
                    { 15, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5860), false, 11, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5863), 2 },
                    { 8, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5759), false, 2, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5762), 12 },
                    { 9, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5774), true, 19, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5776), 11 },
                    { 2, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5657), false, 11, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5662), 3 },
                    { 3, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5682), false, 8, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5684), 2 },
                    { 19, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5916), false, 8, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5919), 11 },
                    { 5, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5713), true, 17, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5715), 20 },
                    { 6, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5728), true, 7, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5730), 8 },
                    { 7, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5743), false, 8, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5745), 7 },
                    { 4, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5697), true, 15, new DateTime(2021, 6, 18, 20, 16, 9, 344, DateTimeKind.Local).AddTicks(5700), 21 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Aspernatur qui et exercitationem sit ex.\nQuidem rerum in autem illo voluptatum dicta est et necessitatibus.", new DateTime(2021, 6, 18, 20, 16, 9, 336, DateTimeKind.Local).AddTicks(9473), 21, new DateTime(2021, 6, 18, 20, 16, 9, 336, DateTimeKind.Local).AddTicks(9871) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "voluptatem", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(400), 35, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(409) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "inventore", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(442), 25, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(446) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Tempore alias fugit dolores unde ex molestiae beatae.\nConsequatur assumenda beatae laborum dolor saepe delectus optio hic consequatur.\nRem consequatur numquam molestiae quis iure id recusandae.\nConsequuntur dolore amet est.\nUt autem itaque reiciendis sed.\nQuas vel doloremque sunt quia.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(1464), 39, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(1473) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Explicabo molestias nesciunt autem.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(1944), 27, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(1953) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Consequuntur neque placeat asperiores. Vero quia qui et sit debitis occaecati accusantium porro. Architecto dolores expedita dolor quia animi. Quis dolorem cumque ratione dolorum sunt. Ullam dicta eaque qui facere at autem ut excepturi. Ipsum ut quod excepturi assumenda natus quo et temporibus maxime.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(2728), 27, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(2739) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Qui optio debitis qui exercitationem eligendi labore aperiam.\nDolores et quo itaque ad et ipsa sed fuga.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(2852), 34, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(2856) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "eos", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(2877), 39, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(2880) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Fugiat ut molestias.\nOmnis tenetur sint.\nNesciunt dolores a eius.\nRecusandae itaque sapiente autem cupiditate.\nOmnis autem et nihil voluptas officia pariatur voluptatum.\nEst omnis commodi reiciendis nemo et ea.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3045), 34, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3049) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "eligendi", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3085), 38, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3088) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "nihil", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3108), 31, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3111) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Tempore numquam eveniet ut asperiores rerum.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3145), 25, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3149) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Et cumque deleniti voluptate dignissimos eum dolores voluptatibus. Nam non magnam eligendi sit fugiat quo omnis est numquam. Consectetur sit ducimus et. Dolorem ut quis sunt aut. Dolores quae provident unde doloribus.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3295), 29, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3299) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Mollitia dolores id ratione delectus quia.\nNesciunt ab dignissimos mollitia.\nAtque omnis ipsam aliquam cum fugit et voluptatem.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3372), 36, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3375) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Quia neque repellendus. Et sequi voluptatem saepe maxime deserunt est impedit expedita. Et voluptatem non ipsa expedita. Et natus voluptatibus incidunt id qui dolores quae.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3490), 24, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3494) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Aut omnis quae molestiae voluptatem et.\nSunt quibusdam id a fuga.\nVoluptatem repellendus alias consequuntur omnis nulla voluptatem.\nIncidunt facilis dolor omnis aut cumque distinctio.\nQuisquam aspernatur aut consequatur enim perspiciatis similique sunt mollitia necessitatibus.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3644), 29, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3648) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "alias", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3668), 37, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3671) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Impedit quae ab explicabo et provident quibusdam debitis.\nEt officiis numquam.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3717), 39, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3720) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Beatae dolorum ut voluptatum eius molestiae non nam.\nRepellendus dignissimos tempora qui nesciunt rerum.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3803), 39, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3807) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Alias sit rem excepturi libero qui nihil est. Molestias id error et sit rerum voluptatibus a. Quos et occaecati et. Placeat aliquid est nostrum nihil molestiae. Molestiae soluta nam voluptate rerum. Voluptatem quisquam consequatur.", new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3950), 36, new DateTime(2021, 6, 18, 20, 16, 9, 337, DateTimeKind.Local).AddTicks(3954) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 18, 20, 16, 9, 223, DateTimeKind.Local).AddTicks(9702), "Marietta.Hermiston@gmail.com", "XGWuykok+Ez3wOGRW/wFWwnWEStr6ogrEdIkdTRIRe0=", "izG6QHe6pHu+VOWkX4VfPTtFCvBjgKBeeABpg8Mh19Q=", new DateTime(2021, 6, 18, 20, 16, 9, 224, DateTimeKind.Local).AddTicks(79), "Delia.Spencer95" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 18, 20, 16, 9, 229, DateTimeKind.Local).AddTicks(3198), "Sibyl_Mertz59@gmail.com", "ta542hNhIL4fxzCmKqrFzWGqvJvY8s94jIsmYVpUbHc=", "mPlCCmjJ5miZ14HKcd1fHDlWBuLndJ85JoHstMsAtcM=", new DateTime(2021, 6, 18, 20, 16, 9, 229, DateTimeKind.Local).AddTicks(3210), "Zetta.Kautzer51" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 18, 20, 16, 9, 234, DateTimeKind.Local).AddTicks(4418), "Patsy.Nienow@yahoo.com", "gYaEw+aYzT/d0DA/PIPdtxAVTDqcI2PdrS9GjSELbYM=", "tcKXUn0RlipkRE2pAW5bJ7GrgUlRd0Ea/9bbiGIxvAU=", new DateTime(2021, 6, 18, 20, 16, 9, 234, DateTimeKind.Local).AddTicks(4426), "Nick63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 18, 20, 16, 9, 239, DateTimeKind.Local).AddTicks(5408), "Duncan69@hotmail.com", "AItRH17oQ0s6u6B8GSeao3NkCRbyzM8KKbCJS3jpE60=", "0W3rgWNjDNuaiX937lPNaOekIO/a7/e9XeP5VgYfsIY=", new DateTime(2021, 6, 18, 20, 16, 9, 239, DateTimeKind.Local).AddTicks(5415), "Rafael.Farrell" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 18, 20, 16, 9, 244, DateTimeKind.Local).AddTicks(7264), "Eva69@hotmail.com", "t4wAkg/yvQSxWEB+vBT922DOZep6BboqSlDuzFuDT1w=", "dQQWdZVTd0dqJH0AOLhKZLHxrDw24TjW3hbYW+MnjRY=", new DateTime(2021, 6, 18, 20, 16, 9, 244, DateTimeKind.Local).AddTicks(7341), "Elody.Kuhn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2021, 6, 18, 20, 16, 9, 250, DateTimeKind.Local).AddTicks(2212), "Tremaine31@gmail.com", "GZnvfZ/1NGcLa/AKJbCIUNNyt5CWd+oviR/ukNJorzU=", "ykPjY12IZORIEdkJ2gv3p4fBAsWqy6kxzTq3XGXdkGE=", new DateTime(2021, 6, 18, 20, 16, 9, 250, DateTimeKind.Local).AddTicks(2219), "Norene.Quitzon" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 18, 20, 16, 9, 255, DateTimeKind.Local).AddTicks(6105), "Raegan32@yahoo.com", "zqtPT0xTrVnhctwDPCASY5quFSgu3ef5XCbazL0SXgc=", "fIx1B4edP3HIGSo2BRj1CaC/CKaJbKv4xiY6TUc0XQo=", new DateTime(2021, 6, 18, 20, 16, 9, 255, DateTimeKind.Local).AddTicks(6111), "Hollie16" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 261, DateTimeKind.Local).AddTicks(1017), "Andy32@gmail.com", "UpBcnFpDfKUS1eB2S3fAegk1Ia5GeLu6Zd9VTzJGsxw=", "BcR+f18DFNkWEg8EdmC37SLYNiPTTvqbCn665k4eouE=", new DateTime(2021, 6, 18, 20, 16, 9, 261, DateTimeKind.Local).AddTicks(1029), "Keeley.Smith" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 18, 20, 16, 9, 266, DateTimeKind.Local).AddTicks(3349), "Donny_Weissnat46@gmail.com", "kuSt16R9VftRDncnOlsimDl4PbVdqEO75JsGASnDDFU=", "0mFlk3B1f0FcxNR2LlxjeZypUX5g1yhzKXBmI9jLPTM=", new DateTime(2021, 6, 18, 20, 16, 9, 266, DateTimeKind.Local).AddTicks(3357), "Eldora.Mohr68" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 6, 18, 20, 16, 9, 272, DateTimeKind.Local).AddTicks(5860), "Aletha.Heaney29@gmail.com", "zDkByhkC44YXH8zLUOrNSNhvbyG74FobmihHZUyQ770=", "iXPkf3Iy6/jYvVf/EvLCFIWQmK8k/rEuJ1LWRDty200=", new DateTime(2021, 6, 18, 20, 16, 9, 272, DateTimeKind.Local).AddTicks(5882), "Alfredo2" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 18, 20, 16, 9, 278, DateTimeKind.Local).AddTicks(1236), "Rylee_Keebler@gmail.com", "onoBik5hTGjzIcH6DAUu+jh4gw2bW7rylG0unXjxJH0=", "JL1tq7DqBK2ZiJzunk+fGFFts/Zkb5QOTRGzP8ZdEsA=", new DateTime(2021, 6, 18, 20, 16, 9, 278, DateTimeKind.Local).AddTicks(1245), "Milo_Torphy71" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 18, 20, 16, 9, 283, DateTimeKind.Local).AddTicks(4096), "Christelle86@gmail.com", "g9R01BLfXLgtUgurux1bx7vd2P4a6DI9tDM2W1AR73k=", "BF994ZeCpejdCudqetk0WzKwIt8y8SZ846wnx+97caU=", new DateTime(2021, 6, 18, 20, 16, 9, 283, DateTimeKind.Local).AddTicks(4103), "Hester.Hane" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 18, 20, 16, 9, 288, DateTimeKind.Local).AddTicks(6906), "Brenna_Lesch86@hotmail.com", "KIQhTgUdXwCshOrLA3TQS9gnBpWiUR5E5PO68P63XVQ=", "0OOvVIlTxGlonyMlAKgzw9H7d6VH2fJpPdVDjw/vdnw=", new DateTime(2021, 6, 18, 20, 16, 9, 288, DateTimeKind.Local).AddTicks(6918), "Modesto_Hodkiewicz54" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 18, 20, 16, 9, 294, DateTimeKind.Local).AddTicks(2030), "Tremaine.Considine43@hotmail.com", "WIc2mBNGxgkdl+rhg0EruqC3pxauQq6ZUE8JcKA/J6U=", "VEtD1XcSI+0hS/9QaKX8K/EpZkFfMb2KAns+cYREviQ=", new DateTime(2021, 6, 18, 20, 16, 9, 294, DateTimeKind.Local).AddTicks(2040), "Beaulah_Bergnaum" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 18, 20, 16, 9, 299, DateTimeKind.Local).AddTicks(3397), "Jacques.Muller47@yahoo.com", "626BvXmcJ4Q93z+TKBPX2nnbDDdyeq8zI2bjg4OzwBc=", "p3svbfjUOFv5Y29F1Vq3Nw8Cgbnx2ETKIPF+6791qWw=", new DateTime(2021, 6, 18, 20, 16, 9, 299, DateTimeKind.Local).AddTicks(3403), "Adeline45" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 18, 20, 16, 9, 304, DateTimeKind.Local).AddTicks(4556), "Lizeth.Nikolaus84@gmail.com", "jED82yiC4qMcOtB+pVvQ+/QbqMYKUo9fjIQ41ltC4k4=", "XJBebIZGxSh1iv5C8jcE4rjI4ismxsqzCxYTanPx4UM=", new DateTime(2021, 6, 18, 20, 16, 9, 304, DateTimeKind.Local).AddTicks(4562), "Wilhelmine_Bechtelar92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 18, 20, 16, 9, 309, DateTimeKind.Local).AddTicks(6840), "Carolina65@gmail.com", "dwTns9ePS8iau4fe/1uc8YK2x8p9gFwBCCHHS67KwgY=", "fGXPRctx1lxhmS8JSNaU7FneyctM32OA3cNPx1hW4P8=", new DateTime(2021, 6, 18, 20, 16, 9, 309, DateTimeKind.Local).AddTicks(6852), "Lincoln0" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 18, 20, 16, 9, 314, DateTimeKind.Local).AddTicks(8180), "Leanna_Maggio@hotmail.com", "NK351gUzkaj48XJ2D2gowIXIehihmRGDsKRE3zlQhGU=", "NbJ+riVQsbZcHPwEOHJwHlt+9f7eGgf8+AWRYv01rwo=", new DateTime(2021, 6, 18, 20, 16, 9, 314, DateTimeKind.Local).AddTicks(8186), "Pearl.Abshire91" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 18, 20, 16, 9, 320, DateTimeKind.Local).AddTicks(150), "Raymond.Christiansen@hotmail.com", "VuuT+Avi8GRIpep/OL8kXfycZR+HmMuJKwIg9aVIXfg=", "WFVqhYrtfu/JwCMVOmF8JhwrS1m0iCKx9aCphYxLfe0=", new DateTime(2021, 6, 18, 20, 16, 9, 320, DateTimeKind.Local).AddTicks(156), "Roma.Kutch" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2021, 6, 18, 20, 16, 9, 325, DateTimeKind.Local).AddTicks(3109), "Ardith76@yahoo.com", "bfW0Lg+yHhrNM/gY3aabVWeJFYEChA4sq7uwBijcfEc=", "tPAmwAcG46Ahn8xJ/ZISYRtjNdnDHRmQGBiL614TwAg=", new DateTime(2021, 6, 18, 20, 16, 9, 325, DateTimeKind.Local).AddTicks(3114), "Chesley.Schimmel57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 18, 20, 16, 9, 330, DateTimeKind.Local).AddTicks(3659), "SS1HEwsUBIhcMMvHK0Exl++J8aMpnpYeghJfoE2vx/0=", "TJ6nGVmNFBuS2TC/Csk6+yAVZkAV1MvkeC1K/vKPKMU=", new DateTime(2021, 6, 18, 20, 16, 9, 330, DateTimeKind.Local).AddTicks(3659) });

            migrationBuilder.AddForeignKey(
                name: "FK_CommentReactions_Comments_CommentId",
                table: "CommentReactions",
                column: "CommentId",
                principalTable: "Comments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CommentReactions_Users_UserId",
                table: "CommentReactions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Posts_PostId",
                table: "Comments",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PostReactions_Posts_PostId",
                table: "PostReactions",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PostReactions_Users_UserId",
                table: "PostReactions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CommentReactions_Comments_CommentId",
                table: "CommentReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_CommentReactions_Users_UserId",
                table: "CommentReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Posts_PostId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_PostReactions_Posts_PostId",
                table: "PostReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_PostReactions_Users_UserId",
                table: "PostReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts");

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 14, 2, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6409), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6411), 8 },
                    { 18, 12, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6544), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6547), 17 },
                    { 17, 1, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6530), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6532), 21 },
                    { 16, 16, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6505), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6507), 9 },
                    { 15, 13, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6489), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6492), 3 },
                    { 13, 13, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6394), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6397), 4 },
                    { 12, 14, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6381), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6383), 6 },
                    { 11, 9, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6364), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6368), 7 },
                    { 10, 3, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6316), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6319), 19 },
                    { 20, 5, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6571), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6574), 1 },
                    { 9, 10, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6302), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6304), 7 },
                    { 7, 9, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6274), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6277), 16 },
                    { 6, 12, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6260), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6262), 7 },
                    { 5, 6, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6246), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6249), 17 },
                    { 4, 8, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6232), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6235), 12 },
                    { 3, 5, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6217), false, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6220), 13 },
                    { 2, 16, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6195), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6200), 12 },
                    { 1, 5, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(5572), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(5916), 2 },
                    { 8, 15, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6289), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6291), 11 },
                    { 19, 11, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6557), true, new DateTime(2021, 6, 17, 23, 16, 54, 898, DateTimeKind.Local).AddTicks(6560), 2 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Amet maiores corrupti perspiciatis aut cupiditate eveniet ut.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(3667), 20, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4005) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Doloremque unde et consectetur modi rerum.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4391), 11, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4396) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Sint rerum nihil tempora beatae nihil voluptates totam nostrum ut.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4449), 19, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4452) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Est at ut velit.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4493), 7, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4496) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Quos doloribus rerum quis blanditiis voluptates occaecati ipsa maxime sit.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4543), 6, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4546) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Omnis omnis unde minima quaerat laudantium dolor.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4577), 9, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4580) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Aliquam aut sunt necessitatibus eveniet et modi.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4666), 13, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4670) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Laborum praesentium non facere suscipit.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4706), 4, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4709) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Et est odit eum.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4738), 9, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4740) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Officiis corporis eligendi voluptatem error.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4769), 7, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4771) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Vel et commodi.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4801), 1, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4804) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Labore cupiditate quis et hic ut voluptatem.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4841), 16, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4844) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Quos ullam asperiores placeat nihil et officiis laboriosam dolorem cupiditate.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4884), 10, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4886) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Distinctio quae eius nemo qui unde quia.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4951), 18, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4954) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Natus et velit id.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4982), 15, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(4985) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Quae accusantium molestias placeat distinctio ut magni.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5017), 8, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5019) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Possimus accusantium porro dolores animi quod asperiores impedit eos minus.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5059), 19, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5062) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Tempore aut est sequi iusto.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5090), 11, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5093) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Qui et libero deleniti repellat doloribus dolor alias dolor.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5157), 16, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5160) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Libero consequatur rerum.", new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5186), 13, new DateTime(2021, 6, 17, 23, 16, 54, 891, DateTimeKind.Local).AddTicks(5188) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(4857), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/875.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(8271) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(8807), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/559.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(8811) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(8993), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/327.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(8996) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9007), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/984.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9009) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9065), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1220.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9068) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9080), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1109.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9083) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9093), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/629.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9096) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9106), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/733.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9109) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9119), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/129.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9121) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9131), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/128.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9134) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9144), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/951.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9147) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9157), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/913.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9159) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9170), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1222.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9172) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9182), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/278.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9185) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9325), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/647.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9328) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9338), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1032.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9340) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9350), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/882.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9352) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9363), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/726.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9365) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9375), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1237.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9378) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9388), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/259.jpg", new DateTime(2021, 6, 17, 23, 16, 54, 746, DateTimeKind.Local).AddTicks(9390) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5332), "https://picsum.photos/640/480/?image=622", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5701) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5875), "https://picsum.photos/640/480/?image=247", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5880) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5895), "https://picsum.photos/640/480/?image=723", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5897) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5908), "https://picsum.photos/640/480/?image=920", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5911) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5921), "https://picsum.photos/640/480/?image=937", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5924) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5934), "https://picsum.photos/640/480/?image=327", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5936) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5946), "https://picsum.photos/640/480/?image=514", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5949) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5959), "https://picsum.photos/640/480/?image=460", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5962) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5972), "https://picsum.photos/640/480/?image=72", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5975) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5985), "https://picsum.photos/640/480/?image=342", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5988) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(5997), "https://picsum.photos/640/480/?image=514", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6000) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6010), "https://picsum.photos/640/480/?image=490", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6013) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6023), "https://picsum.photos/640/480/?image=123", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6026) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6036), "https://picsum.photos/640/480/?image=479", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6038) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6102), "https://picsum.photos/640/480/?image=627", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6105) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6117), "https://picsum.photos/640/480/?image=449", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6119) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6129), "https://picsum.photos/640/480/?image=268", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6132) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6141), "https://picsum.photos/640/480/?image=513", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6144) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6154), "https://picsum.photos/640/480/?image=34", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6157) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6167), "https://picsum.photos/640/480/?image=457", new DateTime(2021, 6, 17, 23, 16, 54, 750, DateTimeKind.Local).AddTicks(6169) });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4395), new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4397) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4422), false, 11, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4425), 7 },
                    { 1, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(3473), false, 5, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(3814), 19 },
                    { 17, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4381), true, 12, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4383), 4 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 16, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4367), false, 8, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4370), 14 },
                    { 14, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4340), false, 8, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4342), 16 },
                    { 13, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4326), true, 16, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4329), 16 },
                    { 12, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4313), false, 5, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4315), 11 },
                    { 11, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4299), true, 9, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4301), 1 },
                    { 10, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4283), false, 13, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4286), 19 },
                    { 15, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4353), true, 1, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4356), 4 },
                    { 8, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4256), false, 5, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4258), 16 },
                    { 9, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4270), false, 13, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4272), 2 },
                    { 2, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4160), true, 20, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4164), 13 },
                    { 3, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4182), false, 15, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4185), 3 },
                    { 19, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4409), false, 3, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4411), 8 },
                    { 5, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4211), true, 9, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4214), 15 },
                    { 6, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4226), false, 8, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4228), 3 },
                    { 7, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4242), true, 1, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4244), 2 },
                    { 4, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4197), false, 2, new DateTime(2021, 6, 17, 23, 16, 54, 895, DateTimeKind.Local).AddTicks(4199), 17 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Rerum ea omnis qui. Ullam ipsam corrupti eveniet vitae et non ut. Quod consectetur aut. Laudantium aut debitis mollitia et qui saepe eos.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(2267), 29, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(2633) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Sunt est sed deleniti mollitia voluptatibus. Officia aspernatur nihil beatae dicta laboriosam aut. Aut et sit dolor sit.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(3215), 37, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(3223) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "sit", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(3516), 21, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(3526) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Dolorem necessitatibus amet expedita distinctio tempore suscipit odio fugit.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(4110), 32, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(4120) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "natus", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(4156), 23, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(4159) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Et nihil dolorum at quos aut quae.\nIn eaque et temporibus iusto deleniti animi quo omnis non.\nQuam molestiae libero rerum.\nSaepe ea officiis sunt est eligendi dolorem voluptatum non.\nNon sapiente non aut qui architecto est.\nAspernatur exercitationem qui voluptas consequuntur libero est nulla vel.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5556), 24, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5568) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Labore dolor temporibus.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5664), 40, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5668) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "voluptatum", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5689), 23, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5692) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Eveniet ut quidem quas temporibus. Neque consequatur tempore similique ut perferendis error incidunt sed. Molestiae ipsam aut aperiam ratione qui aut dolor nihil aut.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5791), 26, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5794) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "dolorum", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5813), 25, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5816) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Ea ut nisi deserunt.\nMinima sunt velit libero iusto.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5898), 36, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5902) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Sed similique eos aut fugit dignissimos nostrum. Beatae omnis unde sit cum ab. Molestiae suscipit et alias et.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5974), 40, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(5978) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "In ipsa nemo quod est et consequatur quibusdam fuga.\nFacilis natus similique tenetur repellendus aut.\nVoluptatem vel quibusdam hic saepe ea enim.\nDignissimos saepe eveniet ut sint dolorum nulla.\nEx sed ea maiores.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6159), 35, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6163) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "dolor", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6185), 31, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6187) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Voluptas unde eius eligendi atque.\nVoluptates officiis et soluta soluta.\nIncidunt sed quisquam totam natus odit quibusdam et consequatur non.\nVel quia autem eum et et.\nNulla est id ratione cum ut.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6326), 34, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6330) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "ullam", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6349), 39, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6352) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Asperiores corrupti tempora iusto quo aut rerum quo.\nRepellat iste magni voluptas sunt eligendi quos aliquam quaerat eum.\nTenetur reprehenderit vitae non autem temporibus perferendis architecto et.\nQuae expedita natus et consequuntur corporis animi numquam ut.\nEveniet rerum optio ratione quia numquam.\nAut nisi aut cum veritatis ipsum sit est id.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6543), 29, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6548) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "et", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6567), 40, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6570) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Tenetur eveniet sunt.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6595), 24, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6598) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Eaque ex earum itaque et optio itaque voluptate neque hic.", new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6671), 21, new DateTime(2021, 6, 17, 23, 16, 54, 887, DateTimeKind.Local).AddTicks(6674) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 17, 23, 16, 54, 775, DateTimeKind.Local).AddTicks(8776), "Rusty_McClure44@yahoo.com", "n10moKj3hYIfd4Db+Jmpj9mLKXxQBW68Upwnybn9pw4=", "vSLPY7B6VrHA3CqfNyWqQ97gEtVZql+RnBanoXHhkrM=", new DateTime(2021, 6, 17, 23, 16, 54, 775, DateTimeKind.Local).AddTicks(9221), "Mariela52" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 17, 23, 16, 54, 781, DateTimeKind.Local).AddTicks(5957), "June.Harris24@yahoo.com", "hn2zPFyh2mGotkfw5zWhIADolgElEYaYKBa3XL+/omo=", "n4WYX6Jd6/etbU4eYjHr+mfh3wASZe6XECpFqZxc/bM=", new DateTime(2021, 6, 17, 23, 16, 54, 781, DateTimeKind.Local).AddTicks(5974), "Beatrice54" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 17, 23, 16, 54, 786, DateTimeKind.Local).AddTicks(6899), "Dimitri.Lueilwitz87@gmail.com", "DJ+W3CoNJTCa/yu9Z83ocm88v2fnCobgDB2DqWwH828=", "C81dUSt8Tf5BScxQG1vuTtDbKGk6+ssQy1JnlFnTTd4=", new DateTime(2021, 6, 17, 23, 16, 54, 786, DateTimeKind.Local).AddTicks(6907), "Iva24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 6, 17, 23, 16, 54, 791, DateTimeKind.Local).AddTicks(8017), "Lenore94@hotmail.com", "J/xPkzzhbHgllcxpdMzIaCxMajR+rXmKjbdPUXPzjAI=", "5jGqR7YOhU+dEGTqUv+XRbx4K2TWVrPLRLQmWJjI53I=", new DateTime(2021, 6, 17, 23, 16, 54, 791, DateTimeKind.Local).AddTicks(8024), "Nathanael56" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 17, 23, 16, 54, 797, DateTimeKind.Local).AddTicks(1701), "Daniella54@yahoo.com", "S7aEE05FBTMRCr0drXkEsTLvxxsQNlrLkK+IvdQcda0=", "hui2SluzHppwLv8yyQjT3+Tp7yvh3OjCPLnc2CZsOz8=", new DateTime(2021, 6, 17, 23, 16, 54, 797, DateTimeKind.Local).AddTicks(1709), "Stan94" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 17, 23, 16, 54, 802, DateTimeKind.Local).AddTicks(2449), "Jazmyn52@yahoo.com", "YhGxdXlD8TU1ZTF1kyHu8e2dq/5gNMwQM2DoyNdMxQk=", "GAXTlRLfK3+JdFRBbuY0bE2SQGNpmLykZnrcZ/h0uQk=", new DateTime(2021, 6, 17, 23, 16, 54, 802, DateTimeKind.Local).AddTicks(2456), "Pearline62" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 17, 23, 16, 54, 807, DateTimeKind.Local).AddTicks(3166), "Jayce_Wolff8@yahoo.com", "56gERSIqsC5dodxax94YC4BLtUQlmn3eEiFAaaw0/Ks=", "BHbQK5Ix9IUdCRcKZmgRyPTosRIPSrAPkVdNIuCOM+Q=", new DateTime(2021, 6, 17, 23, 16, 54, 807, DateTimeKind.Local).AddTicks(3173), "Lawson.Hauck69" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 812, DateTimeKind.Local).AddTicks(5543), "Sigrid.Conn65@gmail.com", "v+N71Apqz5TOkEQMa9LYuLZZXLGWkNJUc8O+MOvClDw=", "HmDAeTdpIchBi6xuPLMglVi/c54hhnl0VbQgZlnxlqA=", new DateTime(2021, 6, 17, 23, 16, 54, 812, DateTimeKind.Local).AddTicks(5551), "Adrianna.McKenzie" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 17, 23, 16, 54, 817, DateTimeKind.Local).AddTicks(6312), "Juana_Schowalter@gmail.com", "Ffa8a/4ipETzG15mTW19e2acVX0kIeLbCiqoZfTegRs=", "sWWScfJEYXcGGYYlTUPssCk8MWmsEQ2787qIMiyyk6A=", new DateTime(2021, 6, 17, 23, 16, 54, 817, DateTimeKind.Local).AddTicks(6319), "Cristal.Kerluke33" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 17, 23, 16, 54, 822, DateTimeKind.Local).AddTicks(7757), "Hazel49@yahoo.com", "ERg1P2fhiDpm8I5d6YVbAL/4n/MTJRkwlzx/hOppguk=", "SQ9O6bH9AXNHunpaptYdkrAxMRtSfBzgAL2lFx73yVU=", new DateTime(2021, 6, 17, 23, 16, 54, 822, DateTimeKind.Local).AddTicks(7762), "Lois27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 17, 23, 16, 54, 828, DateTimeKind.Local).AddTicks(3397), "Haskell_Robel@gmail.com", "+ZbL0WOrt7pG65y8f5tWm92zRMys8xVtC/fK0lvZSdY=", "nWsUa3cG2xtO0q0T0z/eiMnASM2NjJwmBEQNL4f9R48=", new DateTime(2021, 6, 17, 23, 16, 54, 828, DateTimeKind.Local).AddTicks(3405), "Rosalinda82" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 17, 23, 16, 54, 833, DateTimeKind.Local).AddTicks(6848), "Monroe_Kulas34@yahoo.com", "ou5XPQhdZVJErDTQc6azUfKD+G6L6FWMDI4aEcBCcT8=", "h285kvwToulClj30ZvEU6+0TNVDtW4GzefTPuePJUso=", new DateTime(2021, 6, 17, 23, 16, 54, 833, DateTimeKind.Local).AddTicks(6855), "Holly_Hettinger" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 17, 23, 16, 54, 838, DateTimeKind.Local).AddTicks(7562), "Berta47@yahoo.com", "9Kzw02mzyrealbeWni/FWEgVYu1mQkiTqAoEEdL04mk=", "tb9iHciezZKfMqP2DrhBZnT9TAdmWyysVVt7bCYSctw=", new DateTime(2021, 6, 17, 23, 16, 54, 838, DateTimeKind.Local).AddTicks(7568), "Adrianna.Goyette23" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 17, 23, 16, 54, 843, DateTimeKind.Local).AddTicks(9449), "Angus.Wiza@yahoo.com", "nTGnJt7nIHwvtEsuuSsIeFiVodhRlrro5pT4Zv/lHAQ=", "F9dASjZDfhFgkLyMOplzI89UNVWcuALVrUJyb4s6aps=", new DateTime(2021, 6, 17, 23, 16, 54, 843, DateTimeKind.Local).AddTicks(9457), "Gilbert25" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 17, 23, 16, 54, 849, DateTimeKind.Local).AddTicks(527), "Yesenia12@yahoo.com", "+cNZqtDO5DKz/LNKtDaPopx4ZYhmTsfn3KO3pWtoqiY=", "+fY4ddOcZeY2RFLRzSIuEh5MSbnEj/kC9iqtbx3Z71Y=", new DateTime(2021, 6, 17, 23, 16, 54, 849, DateTimeKind.Local).AddTicks(532), "Raheem_Sipes97" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 17, 23, 16, 54, 854, DateTimeKind.Local).AddTicks(1287), "Shaina14@gmail.com", "kqOBP3UxOEjzuymiN+Y+0205roO5gd+4uQczR7/LE4U=", "DTpDzR7ff2TVfZAI5P5inT8jvO0Pdu1YQ7ltnescrDk=", new DateTime(2021, 6, 17, 23, 16, 54, 854, DateTimeKind.Local).AddTicks(1294), "Clement77" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 17, 23, 16, 54, 859, DateTimeKind.Local).AddTicks(3922), "Brooke_Parisian@yahoo.com", "q9kPcdrpTcuUYum0MgCR4wZxx2VCAsI6SZUNLNOGPTs=", "lGZ98Jr42mJjHjNC2bOVLn6IhuOeDmmTClq+xFiSLzU=", new DateTime(2021, 6, 17, 23, 16, 54, 859, DateTimeKind.Local).AddTicks(3930), "Arlene34" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 17, 23, 16, 54, 864, DateTimeKind.Local).AddTicks(5311), "Eino62@hotmail.com", "YeTBzpSzYSS4l2atojMMCuz5YYIY9lwrwLEDw9G1l8o=", "3oYWn7i7ol6A5noZDEpq1Ci9RpAozx8BCjCRhLEdpLo=", new DateTime(2021, 6, 17, 23, 16, 54, 864, DateTimeKind.Local).AddTicks(5318), "Marilie.Ruecker93" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 17, 23, 16, 54, 869, DateTimeKind.Local).AddTicks(6020), "Rhianna.Marquardt65@hotmail.com", "O4OAEvOdUwfqp7S5MTI7ZmM5RYKeJAX/+JPgEL08uV4=", "9Lpt32LHIyOpNqOJWyKcVzSlb2iGjzO+mxOO6ozXxjY=", new DateTime(2021, 6, 17, 23, 16, 54, 869, DateTimeKind.Local).AddTicks(6026), "Felipe.Reilly46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 17, 23, 16, 54, 875, DateTimeKind.Local).AddTicks(984), "Garry_Trantow97@gmail.com", "m0CYgwFtC4SohPIgAiqNvZvbu75Ocg8DGvgDgoiMd80=", "AjSeDTZ+pZueOWpTQdQ0kcJFhrUaO6mL6bfPSn/s2sI=", new DateTime(2021, 6, 17, 23, 16, 54, 875, DateTimeKind.Local).AddTicks(991), "Kira38" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 17, 23, 16, 54, 880, DateTimeKind.Local).AddTicks(1923), "DTJWj+MiJodeSN2K9dV8DH0/miL7xYR/RETnlqznzaQ=", "7JfqcPG422zBYcuDpzEgtFkyVi7ZGmPORr9FMW4ORC8=", new DateTime(2021, 6, 17, 23, 16, 54, 880, DateTimeKind.Local).AddTicks(1923) });

            migrationBuilder.AddForeignKey(
                name: "FK_CommentReactions_Comments_CommentId",
                table: "CommentReactions",
                column: "CommentId",
                principalTable: "Comments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CommentReactions_Users_UserId",
                table: "CommentReactions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Posts_PostId",
                table: "Comments",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PostReactions_Posts_PostId",
                table: "PostReactions",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PostReactions_Users_UserId",
                table: "PostReactions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
