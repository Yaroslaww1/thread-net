﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.DAL.Context.EntityConfigurations
{
    public class CommentReactionConfig : IEntityTypeConfiguration<CommentReaction>
    {
        public void Configure(EntityTypeBuilder<CommentReaction> entity)
        {
            entity
                .HasAlternateKey(cr => new { cr.CommentId, cr.UserId, cr.IsLike });

            entity
                .HasOne(r => r.User)
                .WithMany()
                .OnDelete(DeleteBehavior.ClientCascade);
        }
    }
}
