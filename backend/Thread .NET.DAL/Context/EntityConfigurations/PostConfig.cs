﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.DAL.Context.EntityConfigurations
{
    public class PostConfig : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> entity)
        {
            entity
                .HasOne(p => p.Preview)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            entity
                .HasMany(p => p.Comments)
                .WithOne(c => c.Post)
                .OnDelete(DeleteBehavior.Cascade);

            entity
                .HasMany(p => p.Reactions)
                .WithOne(r => r.Post)
                .HasForeignKey(r => r.PostId)
                .OnDelete(DeleteBehavior.Cascade);

            entity
                .HasOne(p => p.Author)
                .WithMany()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
