﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.DAL.Context.EntityConfigurations
{
    public class PostReactionConfig : IEntityTypeConfiguration<PostReaction>
    {
        public void Configure(EntityTypeBuilder<PostReaction> entity)
        {
            entity
                .HasAlternateKey(pr => new { pr.PostId, pr.UserId, pr.IsLike });

            entity
                .HasOne(r => r.User)
                .WithMany()
                .OnDelete(DeleteBehavior.ClientCascade);
        }
    }
}
