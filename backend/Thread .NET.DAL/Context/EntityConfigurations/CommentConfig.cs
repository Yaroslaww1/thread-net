﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.DAL.Context.EntityConfigurations
{
    public class CommentConfig : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> entity)
        {
            entity
                .HasMany(с => с.Reactions)
                .WithOne(r => r.Comment)
                .HasForeignKey(r => r.CommentId)
                .OnDelete(DeleteBehavior.Cascade);

            entity
                .HasOne(p => p.Author)
                .WithMany()
                .OnDelete(DeleteBehavior.ClientCascade);
        }
    }
}
