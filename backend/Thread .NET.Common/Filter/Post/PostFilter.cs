namespace Thread_.NET.Common.Filter.Post
{
    public sealed class PostFilter
    {
        public int? AuthorId { get; set; }
        public int? LikedByUserId { get; set; }
    }
}