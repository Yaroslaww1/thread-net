﻿namespace Thread_.NET.Common.Options
{
    public sealed class ClientOptions
    {
        public const string Location = "ClientOptions";
        public string Url { get; set; }
    }
}
