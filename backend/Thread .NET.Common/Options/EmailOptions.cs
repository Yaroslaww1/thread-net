﻿namespace Thread_.NET.Common.Options
{
    public sealed class EmailOptions
    {
        public const string Location = "EmailOptions";
        public string SendgridApiKey { get; set; }
        public string SendgridEmail { get; set; }
        public string SendgridName { get; set; }
    }
}
