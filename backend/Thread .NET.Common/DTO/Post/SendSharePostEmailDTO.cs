﻿using Newtonsoft.Json;

namespace Thread_.NET.Common.DTO.Post
{
    public sealed class SendSharePostEmailDTO
    {
        public int PostId { get; set; }

        public string ReceiverEmail { get; set; }
    }
}
