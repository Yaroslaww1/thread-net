﻿namespace Thread_.NET.Common.DTO
{
    public sealed class EmailModel
    {
        public string Subject { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
