﻿namespace Thread_.NET.Common.DTO.User
{
    public class ResetPasswordDTO
    {
        public string Token { get; set; }
        public string Password { get; set; }
    }
}
