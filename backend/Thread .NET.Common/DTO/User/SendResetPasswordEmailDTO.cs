﻿namespace Thread_.NET.Common.DTO.User
{
    public class SendResetPasswordEmailDTO
    {
        public string Email { get; set; }
    }
}
