﻿namespace Thread_.NET.Common.DTO.Comment
{
    public sealed class CommentUpdateDTO
    {
        public string Body { get; set; }
    }
}
